<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;

class HandleAdminAuthentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $scope = $request->route('scope');
        if(!Auth::guard('admin')->check()){
            return redirect()->route('admin.login', $scope);
        }
        return $next($request);
    }
}
