<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\StudentClassRoom;
use App\Models\TypePayment;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function profile($scope)
    {
        $id = auth()->guard('admin')->user()->owner_id;
        $student = Student::with(['student_payment', 'classrooms_sections'])->find($id);
        $class_room_price = StudentClassRoom::where('student_id', $id)->with('class_room')->first();
        $types = TypePayment::all();
//
//        return view('Admin.students.show', compact('scope', 'student', 'class_room_price', 'types'));

        return view('Admin.profile.index', compact('scope', 'student', 'class_room_price', 'types'));
    }
}
