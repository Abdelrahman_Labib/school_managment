<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Agreement;
use App\Models\School;
use App\Models\SchoolExpense;
use App\Models\Student;
use App\Models\StudentPayment;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:show report payments')->only('payments');
        $this->middleware('permission:view report payments receipt')->only('payments_receipt');
        $this->middleware('permission:show report expenses')->only('expenses');
        $this->middleware('permission:view report expenses receipt')->only('expenses_receipt');
    }

    public function payments(Request $request, $scope)
    {
        $payments = StudentPayment::with(['agreement', 'type', 'student']);
        if($request->from && $request->to)
        {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
            $payments = $payments->where('paid_at', '>=', $from)->where('paid_at', '<=', $to);
        }

        $payments = $payments->get();

        return view('Admin.reports.payments.index', compact('payments', 'scope'));
    }

    protected function payments_receipt($scope, $id)
    {
        $receipt = StudentPayment::find($id);
        $school = School::select('header_invoice_image', 'stamp')->first();

        return view('Admin.reports.payments.receipt', compact('scope', 'receipt', 'school'));
    }

    public function expenses(Request $request, $scope)
    {
        $expenses = SchoolExpense::with('category');
        if($request->from && $request->to)
        {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
            $expenses = $expenses->where('date', '>=', $from)->where('date', '<=', $to);
        }

        $expenses = $expenses->get();

        return view('Admin.reports.expenses.index', compact('expenses', 'scope'));
    }

    protected function expenses_receipt($scope, $id)
    {
        $receipt = SchoolExpense::find($id);
        $school = School::select('header_invoice_image', 'stamp')->first();

        return view('Admin.reports.expenses.receipt', compact('scope', 'receipt', 'school'));
    }

    public function agreements(Request $request, $scope)
    {
        if($request->from && $request->to)
        {
            $agreements = Agreement::where('start_date', '>=', $request->from)->where('end_date', '<=', $request->to)->get();
        }else{
            $agreements = Agreement::where('start_date', '>=', now()->format('Y-m-d'))->get();
        }

        return view('Admin.reports.agreements.index', compact('agreements','scope'));
    }
}
