<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CategoryIncentive;
use App\Models\Employee;
use App\Models\EmployeeIncentive;
use App\Models\School;
use Illuminate\Http\Request;

class EmployeeIncentiveController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:show employee incentives')->only('index');
        $this->middleware('permission:create employee incentive')->only('store');
        $this->middleware('permission:edit employee incentive')->only('update');
        $this->middleware('permission:delete employee incentive')->only('destroy');
    }

    public function index($scope)
    {
        $employeeIncentives = EmployeeIncentive::paginate(30);
        $employees = Employee::select('id', 'name')->get();
        $categories = CategoryIncentive::select('id', 'name')->get();

        return view('Admin.employees.incentives.index', compact('scope', 'employeeIncentives', 'employees', 'categories'));
    }

    public function store(Request $request, $scope)
    {
        $validate = $this->checkValidation($request);

        EmployeeIncentive::create($validate);

        return back()->with('success', __('admin.storeSuccessMessage'));
    }

    public function show($scope, $id)
    {
        $receipt = EmployeeIncentive::with('employee')->find($id);
        $receipt->name = $receipt->employee->name;
        $school = School::select('header_invoice_image', 'stamp')->first();

        return view('Admin.layouts.receipt_exchange', compact('receipt', 'school'));
    }

    public function update(Request $request, $scope, $id)
    {
        $validate = $this->checkValidation($request);

        EmployeeIncentive::where('id', $id)->update($validate);

        return back()->with('success', __('admin.updateSuccessMessage'));
    }

    public function destroy($id)
    {
        EmployeeIncentive::where('id', $id)->delete();
        return back()->with('success', __('admin.deleteSuccessMessage'));
    }

    public function checkValidation($input)
    {
        return $input->validate([
            'category_incentive_id' => 'required|exists:category_incentives,id',
            'employee_id'           => 'required|exists:employees,id',
            'cost'                  => 'required|numeric',
            'date'                  => 'required|date',
            'notes'                 => 'sometimes|max:400',
        ]);
    }
}
