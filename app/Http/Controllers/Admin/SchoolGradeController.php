<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AcademicYear;
use App\Models\SchoolGrade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SchoolGradeController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:show school grades')->only('index');
        $this->middleware('permission:create school grades')->only('store');
        $this->middleware('permission:edit school grades')->only('update');
        $this->middleware('permission:delete school grades')->only('destroy');
    }

    public function index($scope)
    {
        $school_grades =
        DB::table('school_grades')
        ->select([
            'school_grades.id',
            'school_grades.name',
            'school_grades.academic_year_id',
            'academic_years.year as academic_year',
            DB::raw('COUNT(student_class_rooms.student_id) as student_count')
        ])
        ->join('academic_years', 'school_grades.academic_year_id', '=', 'academic_years.id')
        ->leftJoin('class_rooms', 'school_grades.id', '=', 'class_rooms.school_grade_id')
        ->leftJoin('student_class_rooms', 'class_rooms.id', '=','student_class_rooms.class_room_id')
        ->groupBy('school_grades.id')
        ->where('school_grades.deleted_at', Null)
        ->orderBy('school_grades.id', 'desc')
        ->paginate(20);

        $academic_years = AcademicYear::select('id', 'year')->get();

        return view('Admin.school_grades.index', compact('school_grades', 'scope', 'academic_years'));
    }

    public function store(Request $request, $scope)
    {
        $validate = $this->checkValidation($request);

        SchoolGrade::create($validate);

        return back()->with('success', __('admin.storeSuccessMessage'));
    }

    public function update(Request $request, $scope, $id)
    {
        $validate = $this->checkValidation($request, $id);

        SchoolGrade::where('id', $id)->update($validate);

        return back()->with('success', __('admin.updateSuccessMessage'));
    }

    public function destroy($scope, $id)
    {
        SchoolGrade::where('id', $id)->delete();
        return back()->with('success', __('admin.deleteSuccessMessage'));
    }

    public function checkValidation($input, $id = null)
    {
        return $input->validate([
            'academic_year_id'  => 'required|exists:academic_years,id',
            'name'              => 'required|max:190|unique:school_grades,name,'.$id.',id,academic_year_id,'.$input->academic_year_id.',deleted_at,NULL',
        ]);
    }
}
