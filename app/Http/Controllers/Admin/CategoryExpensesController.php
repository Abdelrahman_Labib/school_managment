<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryExpensesController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:show expense category')->only('index');
        $this->middleware('permission:create expense category')->only('store');
        $this->middleware('permission:edit expense category')->only('update');
        $this->middleware('permission:delete expense category')->only('destroy');
    }

    public function index($scope)
    {
        $categories = Category::paginate(20);
        return view('Admin.categories.index', compact('categories', 'scope'));
    }

    public function create($scope)
    {
        //
    }

    public function store(Request $request, $scope)
    {
        $validate = $this->checkValidation($request);

        Category::create($validate);

        return back()->with('success', __('admin.storeSuccessMessage'));
    }

    public function show($id)
    {
        //
    }

    public function edit($scope, $id)
    {
        //
    }

    public function update(Request $request, $scope, $id)
    {
        $validate = $this->checkValidation($request);

        Category::where('id', $id)->update($validate);

        return back()->with('success', __('admin.updateSuccessMessage'));
    }

    public function destroy($id)
    {
        //
    }

    public function checkValidation($input)
    {
        return $input->validate([
            'name'      => 'required|unique:categories,name',
        ]);
    }
}
