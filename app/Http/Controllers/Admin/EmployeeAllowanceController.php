<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CategoryAllowance;
use App\Models\Employee;
use App\Models\EmployeeAllowance;
use App\Models\School;
use Illuminate\Http\Request;

class EmployeeAllowanceController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:show employee allowances')->only('index');
        $this->middleware('permission:create employee allowance')->only('store');
        $this->middleware('permission:edit employee allowance')->only('update');
        $this->middleware('permission:delete employee allowance')->only('destroy');
    }

    public function index($scope)
    {
        $employeeAllowances = EmployeeAllowance::paginate(30);
        $employees = Employee::select('id', 'name')->get();
        $categories = CategoryAllowance::select('id', 'name')->get();

        return view('Admin.employees.allowances.index', compact('scope', 'employeeAllowances','employees','categories'));
    }

    public function store(Request $request, $scope)
    {
        $validate = $this->checkValidation($request);

        EmployeeAllowance::create($validate);

        return back()->with('success', __('admin.storeSuccessMessage'));
    }

    public function show($scope, $id)
    {
        $receipt = EmployeeAllowance::with('employee')->find($id);
        $receipt->name = $receipt->employee->name;
        $school = School::select('header_invoice_image', 'stamp')->first();

        return view('Admin.layouts.receipt_exchange', compact('receipt', 'school'));
    }

    public function update(Request $request, $scope, $id)
    {
        $validate = $this->checkValidation($request);

        EmployeeAllowance::where('id', $id)->update($validate);

        return back()->with('success', __('admin.updateSuccessMessage'));
    }

    public function destroy($id)
    {
        EmployeeAllowance::where('id', $id)->delete();
        return back()->with('success', __('admin.deleteSuccessMessage'));
    }

    public function checkValidation($input)
    {
        return $input->validate([
            'category_allowance_id' => 'required|exists:category_allowances,id',
            'employee_id'           => 'required|exists:employees,id',
            'cost'                  => 'required|numeric',
            'date'                  => 'required|date',
            'notes'                 => 'sometimes|max:400',
        ]);
    }
}
