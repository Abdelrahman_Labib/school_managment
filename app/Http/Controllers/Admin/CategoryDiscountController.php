<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CategoryDiscount;
use Illuminate\Http\Request;

class CategoryDiscountController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:show category discounts')->only('index');
        $this->middleware('permission:create category discount')->only('store');
        $this->middleware('permission:edit category discount')->only('update');
        $this->middleware('permission:delete category discount')->only('destroy');
    }

    public function index($scope)
    {
        $categoryDiscounts = CategoryDiscount::paginate(30);

        return view('Admin.employees.categories.discounts.index', compact('scope', 'categoryDiscounts'));
    }

    public function store(Request $request, $scope)
    {
        $validate = $this->checkValidation($request);

        CategoryDiscount::create($validate);

        return back()->with('success', __('admin.storeSuccessMessage'));
    }

    public function update(Request $request, $scope, $id)
    {
        $validate = $this->checkValidation($request, $id);

        CategoryDiscount::where('id', $id)->update($validate);

        return back()->with('success', __('admin.updateSuccessMessage'));
    }

    public function destroy($id)
    {
        CategoryDiscount::where('id', $id)->delete();
        return back()->with('success', __('admin.deleteSuccessMessage'));
    }

    public function checkValidation($input, $id = null)
    {
        return $input->validate([
            'name' => 'required|max:190|unique:category_discounts,name,'.$id.',id,deleted_at,NULL',
        ]);
    }
}
