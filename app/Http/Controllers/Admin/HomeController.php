<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\SchoolExpense;
use App\Models\Student;
use App\Models\StudentPayment;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index($scope)
    {
        $students = Student::count();
        $payments_received = StudentPayment::sum('paid');
        $payments_remain = StudentPayment::sum('remain');
        $expenses = SchoolExpense::sum('expense');

        return view('Admin.dashboard', compact('scope', 'students', 'payments_received', 'payments_remain', 'expenses'));
    }
}
