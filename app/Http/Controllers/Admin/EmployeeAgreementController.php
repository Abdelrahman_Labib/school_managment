<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\EmployeeAgreement;
use Illuminate\Http\Request;

class EmployeeAgreementController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:show employee agreements')->only('index');
        $this->middleware('permission:create employee agreement')->only('store');
        $this->middleware('permission:edit employee agreement')->only('update');
        $this->middleware('permission:delete employee agreement')->only('destroy');
    }

    public function index($scope)
    {
        $employeeAgreements = EmployeeAgreement::paginate(30);
        $employees = Employee::select('id', 'name')->get();

        return view('Admin.employees.agreements.index', compact('scope', 'employeeAgreements', 'employees'));
    }

    public function store(Request $request, $scope)
    {
        $validate = $this->checkValidation($request);

        EmployeeAgreement::create($validate);

        return back()->with('success', __('admin.storeSuccessMessage'));
    }

    public function update(Request $request, $scope, $id)
    {
        $validate = $this->checkValidation($request);

        EmployeeAgreement::where('id', $id)->update($validate);

        return back()->with('success', __('admin.updateSuccessMessage'));
    }

    public function destroy($id)
    {
        EmployeeAgreement::where('id', $id)->delete();
        return back()->with('success', __('admin.deleteSuccessMessage'));
    }

    public function checkValidation($input)
    {
        return $input->validate([
            'employee_id' => 'required|exists:employees,id',
            'payment'     => 'required|numeric',
            'start_date'  => 'required|date',
            'end_date'    => 'required|date',
        ]);
    }
}
