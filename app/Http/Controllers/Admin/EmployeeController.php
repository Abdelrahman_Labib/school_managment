<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AcademicYear;
use App\Models\Employee;
use App\Models\EmployeeJob;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
//    public function __construct()
//    {
//        $this->middleware('permission:show employees')->only('index');
//        $this->middleware('permission:create employee')->only('store');
//        $this->middleware('permission:view employee')->only('show');
//        $this->middleware('permission:edit employee')->only('update');
//        $this->middleware('permission:delete employee')->only('destroy');
//    }

    public function index($scope)
    {
        $employees = Employee::paginate(20);
        $employeeJobs = EmployeeJob::all();
        $academicYears = AcademicYear::select('id', 'year')->get();

        return view('Admin.employees.index', compact('scope', 'employees', 'employeeJobs', 'academicYears'));
    }

    public function store(Request $request, $scope)
    {
        $validate = $this->checkValidation($request);

        Employee::create($validate);

        return back()->with('success', __('admin.storeSuccessMessage'));
    }

    public function show(Request $request, $scope, $id)
    {
       $employee = Employee::find($id);

        return view('Admin.employees.show', compact('scope', 'employee'));
    }

    public function update(Request $request, $scope, $id)
    {
        $validate = $this->checkValidation($request, $id);

        $employee = Employee::where('id', $id)->first();
        $employee->update($validate);
        if($request->image){
            $employee->image = $request->image;
            $employee->save();
        }

        return back()->with('success', __('admin.updateSuccessMessage'));
    }

    public function destroy($id)
    {
        Employee::where('id', $id)->delete();
        return back()->with('success', __('admin.deleteSuccessMessage'));
    }

    public function checkValidation($input, $id = null)
    {
        return $input->validate([
            'academic_year_id' => 'required|exists:academic_years,id',
            'employee_job_id'  => 'required|exists:employee_jobs,id',
            'name'             => 'required|max:190|unique:employees,name,'.$id.',id,employee_job_id,'.$input->employee_job_id.',deleted_at,NULL',
            'phone'            => 'required|max:190|unique:employees,phone,'.$id.',id,employee_job_id,'.$input->employee_job_id.',deleted_at,NULL',
            'email'            => 'required|max:190|unique:employees,email,'.$id.',id,employee_job_id,'.$input->employee_job_id.',deleted_at,NULL',
            'address'          => 'required|max:190',
            'id_number'        => 'required|max:190',
            'expiry_id_number' => 'required|date',
            'nationality'      => 'required|max:190',
            'passport'         => 'required|max:190',
            'expiry_passport'  => 'required|date',
            'image'            => 'sometimes|image',
        ]);
    }
}
