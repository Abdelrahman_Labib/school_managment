<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CategoryDiscount;
use App\Models\Employee;
use App\Models\EmployeeDiscount;
use App\Models\School;
use Illuminate\Http\Request;

class EmployeeDiscountController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:show employee discounts')->only('index');
        $this->middleware('permission:create employee discount')->only('store');
        $this->middleware('permission:edit employee discount')->only('update');
        $this->middleware('permission:delete employee discount')->only('destroy');
    }

    public function index($scope)
    {
        $employeeDiscounts = EmployeeDiscount::paginate(30);
        $employees = Employee::select('id', 'name')->get();
        $categories = CategoryDiscount::select('id', 'name')->get();

        return view('Admin.employees.discounts.index', compact('scope', 'employeeDiscounts', 'employees', 'categories'));
    }

    public function store(Request $request, $scope)
    {
        $validate = $this->checkValidation($request);

        EmployeeDiscount::create($validate);

        return back()->with('success', __('admin.storeSuccessMessage'));
    }

    public function show($scope, $id)
    {
        $receipt = EmployeeDiscount::with('employee')->find($id);
        $receipt->name = $receipt->employee->name;
        $school = School::select('header_invoice_image', 'stamp')->first();

        return view('Admin.layouts.receipt_voucher', compact('receipt', 'school'));
    }

    public function update(Request $request, $scope, $id)
    {
        $validate = $this->checkValidation($request);

        EmployeeDiscount::where('id', $id)->update($validate);

        return back()->with('success', __('admin.updateSuccessMessage'));
    }

    public function destroy($id)
    {
        EmployeeDiscount::where('id', $id)->delete();
        return back()->with('success', __('admin.deleteSuccessMessage'));
    }

    public function checkValidation($input)
    {
        return $input->validate([
            'category_discount_id'  => 'required|exists:category_discounts,id',
            'employee_id'           => 'required|exists:employees,id',
            'cost'                  => 'required|numeric',
            'date'                  => 'required|date',
            'notes'                 => 'sometimes|max:400',
        ]);
    }
}
