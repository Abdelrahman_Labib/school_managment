<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Agreement;
use App\Models\ClassRoom;
use App\Models\School;
use App\Models\Student;
use App\Models\StudentClassRoom;
use App\Models\StudentPayment;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AgreementController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:show agreements')->only('index');
        $this->middleware('permission:view agreements')->only('show');
        $this->middleware('permission:create agreements')->only('store');
        $this->middleware('permission:edit agreements')->only('update');
        $this->middleware('permission:delete agreements')->only('destroy');
    }

    public function index(Request $request, $scope)
    {
        $agreements = Agreement::with(['school', 'student'])->orderBy('id', 'desc')->get();
        $school = School::select('id', 'name')->first();
        $students = Student::select('id', 'name')->get();

        return view('Admin.agreements.index', compact('agreements', 'school', 'students', 'scope'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $validate = $this->checkValidation($request);

        $agreement = Agreement::create($validate);

        StudentPayment::create([
            'student_id'    => $request->student_id,
            'agreement_id'  => $agreement->id,
            'type_id'       => 1,
            'paid'          => $request->paid,
            'remain'        => $request->remain,
            'paid_at'       => now(),
            'description'   => 'paid'.$request->paid,
        ]);

        return back()->with(['success' => __('admin.storeSuccessMessage'), 'agreement_id'=> $agreement->id]);
    }

    public function show($scope, $id)
    {
        $agreement = Agreement::select('id', 'student_id', 'start_date')->find($id);
        $student = Student::select('id', 'name', 'parent_name', 'school_number_id', 'address')->find($agreement->student_id);
        $school = School::select('id', 'name', 'header_invoice_image')->first();

        return view('Admin.agreements.show', compact( 'scope', 'agreement', 'student', 'school'));
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $scope, $id)
    {
        $validate = $this->checkValidation($request);

        Agreement::where('id', $id)->update($validate);

        return back()->with('success', __('admin.updateSuccessMessage'));
    }

    public function destroy($id)
    {
        //
    }

    public function checkValidation($input)
    {
        return $input->validate([
            'school_id'      => 'required|exists:schools,id',
            'student_id'     => 'required|exists:students,id',
            'start_date'     => 'required|date',
            'end_date'       => 'required|date',
            'payment'        => 'required|numeric',
            'discount'       => 'required|numeric|lt:'.$input->payment,
            'after_discount' => 'required|numeric|gt:0',
        ]);
    }

    public function getClassRoomPrice($scope, $student_id)
    {
        $studentClassRoom = StudentClassRoom::where('student_id', $student_id)->select('class_room_id')->get()->last();
        return ClassRoom::select('price')->find($studentClassRoom->class_room_id)->price;
    }
}
