<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ClassRoom;
use App\Models\Section;
use Illuminate\Http\Request;

class SectionController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:show sections')->only('show');
        $this->middleware('permission:create sections')->only('store');
        $this->middleware('permission:edit sections')->only('update');
        $this->middleware('permission:delete sections')->only('destroy');
    }

    public function index()
    {
        //
    }

    public function create(Request $request, $scope)
    {
        //
    }

    public function store(Request $request, $scope)
    {
        $validate = $this->checkValidation($request);

        Section::create($validate);

        return redirect()->route('admin.sections.show', [$scope, $request->class_room_id])->with('success', __('admin.storeSuccessMessage'));
    }

    public function show(Request $request, $scope, $id)
    {
        $request->merge(['id' => $id]);
        $request->validate([
            'id' => 'required|exists:class_rooms,id'
        ]);

        $sections = Section::where('class_room_id', $id)->paginate(30);
        return view('Admin.class_rooms.sections.index', compact('sections', 'scope', 'id'));
    }

    public function edit(Request $request, $scope, $id)
    {
        //
    }

    public function update(Request $request, $scope, $id)
    {
        $validate = $this->checkValidation($request);

        Section::where('id', $id)->update($validate);

        return redirect()->route('admin.sections.show', [$scope, $request->class_room_id])->with('success', __('admin.updateSuccessMessage'));
    }

    public function destroy($id)
    {
        //
    }

    public function checkValidation($input)
    {
        return $input->validate([
            'class_room_id' => 'required|exists:class_rooms,id',
            'name'          => 'required',
        ]);
    }

    public function ajaxSelection($scope, $id)
    {
        $sections = Section::where('class_room_id', $id)->get();
        return response()->json($sections);
    }
}
