<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Agreement;
use App\Models\ClassRoom;
use App\Models\School;
use App\Models\Student;
use App\Models\StudentAttachment;
use App\Models\StudentClassRoom;
use App\Models\StudentPayment;
use App\Models\TypePayment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class StudentController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:show students')->only('index');
        $this->middleware('permission:create student')->only('store');
        $this->middleware('permission:view student')->only('show');
        $this->middleware('permission:add student payment')->only('payment');
        $this->middleware('permission:add student attachment')->only('attachment');
        $this->middleware('permission:transfer student')->only('transfer');
        $this->middleware('permission:edit student')->only('update');
        $this->middleware('permission:delete student')->only('destroy');
    }

    public function index(Request $request, $scope)
    {
        $server = url('');
        $students =
        DB::table('students')
        ->select([
            'students.id',
            'students.name',
            'students.parent_name',
            'students.parent_phone',
            'students.gender',
            'students.school_number_id',
            'students.address',
            'students.nationality',
            'students.birthday',
            DB::raw("concat('$server/storage/', students.image) as image"),
            'class_rooms.id as class_room_id',
            'class_rooms.name as class_room_name',
            'class_rooms.price as class_room_price',
            'sections.name as section_name',
            DB::raw("COALESCE(SUM(student_payments.paid), 0) AS student_payment_paid"),
            'student_payments.remain as student_payment_remain',
        ])
        ->leftJoin('student_class_rooms', 'students.id', '=', 'student_class_rooms.student_id')
        ->leftJoin('class_rooms', 'student_class_rooms.class_room_id', '=', 'class_rooms.id')
        ->leftJoin('sections', 'student_class_rooms.section_id', '=', 'sections.id')
        ->leftJoin('school_grades', 'class_rooms.school_grade_id', '=', 'school_grades.id')
        ->leftJoin('student_payments', 'students.id', '=', 'student_payments.student_id')
        ->groupBy('students.id');

        if ($request->academic_year){
            $students = $students->where('school_grades.academic_year_id', $request->academic_year);
        }elseif($request->school_grades){
            $students = $students->where('school_grades.id', $request->school_grades);
        }elseif($request->class_room){
            $students = $students->where('class_rooms.id', $request->class_room);
        }

        $students = $students->paginate(1);
        $class_rooms = ClassRoom::get();
        return view('Admin.students.index', compact('students', 'class_rooms', 'scope'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request, $scope)
    {
        $validate = $this->checkValidation($request);

        unset($validate['class_room_id'], $validate['section_id']);
        $student = Student::create($validate);

        StudentClassRoom::create([
            'student_id'    => $student->id,
            'class_room_id' => $request->class_room_id,
            'section_id'    => $request->section_id,
        ]);

        Admin::create([
            'active' => 1,
            'owner_type' => 'student',
            'owner_id' => $student->id,
            'unique_id' => $student->school_number_id,
            'password' => Hash::make('123456'),
        ]);

        return back()->with('success', __('admin.storeSuccessMessage'));
    }

    public function show(Request $request, $scope, $id)
    {
        $request->merge(['id' => $id]);
        $request->validate([
            'id' => 'required|exists:students,id',
        ]);

        $student = Student::with(['student_payment', 'classrooms_sections'])->find($id);
        $class_room_price = StudentClassRoom::where('student_id', $id)->with('class_room')->first();
        $types = TypePayment::all();

        return view('Admin.students.show', compact('scope', 'student', 'class_room_price', 'types'));
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $scope, $id)
    {
        $validate = $this->checkValidation($request, $id);

        unset($validate['class_room_id'], $validate['section_id'], $validate['image']);

        $student = Student::where('id', $id)->first();
        $student->update($validate);
        if($request->image){
            $student->image = $request->image;
            $student->save();
        }

        StudentClassRoom::where('student_id', $id)->update([
            'class_room_id' => $request->class_room_id,
            'section_id'    => $request->section_id,
        ]);

        return back()->with('success', __('admin.updateSuccessMessage'));
    }


    public function destroy($id)
    {
        //
    }

    public function checkValidation($input, $id=null)
    {
        return $input->validate([
            'name'              => 'required|unique:students,name,'. $id,
            'parent_name'       => 'required',
            'parent_phone'      => 'required',
            'gender'            => 'required|in:male,female',
            'school_number_id'  => 'required|unique:students,school_number_id,'. $id,
            'address'           => 'required',
            'nationality'       => 'required',
            'birthday'          => 'required',
            'class_room_id'     => 'required',
            'section_id'        => 'required',
            'image'             => 'sometimes|image',
        ]);
    }

    public function payment(Request $request, $scope)
    {

        $class_room_price = StudentClassRoom::where('student_id', $request->student_id)->get()->last()->class_room->price;

        $request->validate([
            'student_id'        => 'required|exists:students,id',
            'agreement_id'      => 'required|exists:agreements,id',
            'paid'              => 'required|numeric|gt:0|lte:'. $class_room_price,
            'paid_at'           => 'required|date',
            'type_id'           => 'required|exists:type_payments,id',
            'name_payment_type' => 'sometimes',
        ]);

        $studentPaidSum = array_sum(StudentPayment::where('student_id', $request->student_id)->pluck('paid')->toArray());
        $agreement = Agreement::select('after_discount')->find($request->agreement_id);

        if($agreement->after_discount == $studentPaidSum){
            return back()->with('error', __('admin.wrongValue'));
        }
        $total = $request->paid + $studentPaidSum;

        $request->merge([
            'remain' => $agreement->after_discount - $total
        ]);

        StudentPayment::create($request->all());

        return back()->with('success', __('admin.storeSuccessMessage'));
    }

    public function payment_receipt($scope, $id)
    {
        $receipt = StudentPayment::with('student', 'type')->find($id);
        $receipt->name = $receipt->student->name;
        $receipt->cost = $receipt->paid;
        $receipt->notes = $receipt->description;
        $receipt->date = $receipt->paid_at;
        $school = School::select('header_invoice_image', 'stamp')->first();

        return view('Admin.layouts.receipt_voucher', compact('receipt', 'school'));
    }

    public function attachment(Request $request)
    {
        $validate = $request->validate([
            'name'       => 'required',
            'attachment' => 'required',
            'student_id' => 'required|exists:students,id',
        ]);

        StudentAttachment::create($validate);

        return back()->with('success', __('admin.storeSuccessMessage'));
    }

    public function transfer(Request $request)
    {
        $request->validate([
            'student_id'             => 'required|exists:students,id',
            'transfer_class_room_id' => 'required|exists:class_rooms,id|unique:student_class_rooms,class_room_id,student_id',
            'transfer_section_id'    => 'required|exists:sections,id|unique:student_class_rooms,section_id,student_id',
        ]);

        StudentClassRoom::create([
            'student_id'    => $request->student_id,
            'class_room_id' => $request->class_room_id,
            'section_id'    => $request->section_id,
        ]);

        return back()->with('success', __('admin.storeSuccessMessage'));
    }
}
