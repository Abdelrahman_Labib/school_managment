<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CategoryAllowance;
use Illuminate\Http\Request;

class CategoryAllowanceController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:show category allowances')->only('index');
        $this->middleware('permission:create category allowance')->only('store');
        $this->middleware('permission:edit category allowance')->only('update');
        $this->middleware('permission:delete category allowance')->only('destroy');
    }

    public function index($scope)
    {
        $categoryAllowances = CategoryAllowance::paginate(30);

        return view('Admin.employees.categories.allowances.index', compact('scope', 'categoryAllowances'));
    }

    public function store(Request $request, $scope)
    {
        $validate = $this->checkValidation($request);

        CategoryAllowance::create($validate);

        return back()->with('success', __('admin.storeSuccessMessage'));
    }

    public function update(Request $request, $scope, $id)
    {
        $validate = $this->checkValidation($request, $id);

        CategoryAllowance::where('id', $id)->update($validate);

        return back()->with('success', __('admin.updateSuccessMessage'));
    }

    public function destroy($id)
    {
        CategoryAllowance::where('id', $id)->delete();
        return back()->with('success', __('admin.deleteSuccessMessage'));
    }

    public function checkValidation($input, $id = null)
    {
        return $input->validate([
            'name' => 'required|max:190|unique:category_allowances,name,'.$id.',id,deleted_at,NULL',
        ]);
    }
}
