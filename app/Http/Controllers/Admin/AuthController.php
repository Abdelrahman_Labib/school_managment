<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function viewLoginPage(Request $request, $scope)
    {
        if(Auth::guard('admin')->user())
        {
            return redirect('/school/admin/dashboard');
        }

        return view('Admin.login.view', compact('scope'));
    }

    public function authenticate(Request $request, $scope)
    {
        if($scope == 'student'){

            $request->validate([
                'school_id' => 'required',
                'password'  => 'required|min:6'
            ]);

            if(Auth::guard('admin')->attempt(['unique_id' => $request->school_id, 'password' => $request->password, 'active' => 1], false))
            {
                return redirect()->route('admin.profile', $scope);
            }else{
                return back()->with('error', 'Invalid Credentials');
            }

        }elseif($scope == 'admin'){

            $request->validate([
                'phone'     => 'required',
                'password'  => 'required|min:6'
            ]);

            if(Auth::guard('admin')->attempt(['phone' => $request->phone, 'password' => $request->password, 'active' => 1], false))
            {
                return redirect()->route('admin.dashboard', $scope);
            }else{
                return back()->with('error', 'Invalid Credentials');
            }
        }
    }

    public function logout($scope)
    {
        Auth::guard('admin')->logout();
        return redirect()->route('admin.login', $scope);
    }
}
