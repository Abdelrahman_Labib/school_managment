<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:show admins')->only('index');
        $this->middleware('permission:create admin')->only('store');
        $this->middleware('permission:edit admin')->only('update');
    }

    public function index($scope)
    {
        $admins = Admin::where('id', '!=', 1)->where('owner_type', 'admin')->get();
        $permissions = Permission::where('guard_name','admin')->get();
        $data = [
            'Dashboard'         => [$permissions[0]],
            'School'            => [$permissions[1],$permissions[2],$permissions[3]],
            'Academic year'     => [$permissions[4],$permissions[5],$permissions[6],$permissions[7]],
            'School grades'     => [$permissions[8],$permissions[9],$permissions[10],$permissions[11]],
            'Classrooms'        => [$permissions[12],$permissions[13],$permissions[14],$permissions[15]],
            'Sections'          => [$permissions[16],$permissions[17],$permissions[18],$permissions[19]],
            'Admins'            => [$permissions[20],$permissions[21],$permissions[22]],
            'Students'          => [$permissions[23],$permissions[24],$permissions[25],$permissions[26],$permissions[27],
                                 $permissions[28],$permissions[29],$permissions[30],$permissions[31]],
            'Expense category'  => [$permissions[32],$permissions[33],$permissions[34],$permissions[35]],
            'Expenses'          => [$permissions[36],$permissions[37],$permissions[38],$permissions[39]],
            'Agreements'        => [$permissions[40],$permissions[41],$permissions[42],$permissions[43],$permissions[44]],
            'Report payments'   => [$permissions[45],$permissions[46]],
            'Report expenses'   => [$permissions[47],$permissions[48]],
        ];

        return view('Admin.admins.index', compact('scope', 'admins', 'data'));
    }

    public function create($scope)
    {
        //
    }

    public function store(Request $request, $scope)
    {
        $request->validate([
            'name'      => 'required',
            'email'     => 'required|unique:admins,email',
            'phone'     => 'required|unique:admins,phone',
            'password'  => 'required|min:6',
        ]);

        $admin = Admin::create([
            'active'     => 1,
            'owner_type' => 'admin',
            'name'       => $request->name,
            'email'      => $request->email,
            'phone'      => $request->phone,
            'password'   => Hash::make($request->password),
        ]);

        $admin->syncPermissions($request->check_list);

        return back()->with('success', __('admin.storeSuccessMessage'));
    }

    public function show($id)
    {
        //
    }

    public function edit($scope, $id)
    {
        //
    }

    public function update(Request $request, $scope, $id)
    {
        $request->validate([
            'name'  => 'required',
            'email' => 'required|unique:admins,email,'.$id,
            'phone' => 'required|unique:admins,phone,'.$id,
        ]);

        $admin = Admin::whereId($id)->first();

        $admin->active  = $request->active;
        $admin->name    = $request->name;
        $admin->email   = $request->email;
        $admin->phone   = $request->phone;

        if($request->password)
            $admin->password = Hash::make($request->password);

        $admin->save();

        return back()->with('success', __('admin.updateSuccessMessage'));
    }

    public function destroy($id)
    {
        //
    }
}
