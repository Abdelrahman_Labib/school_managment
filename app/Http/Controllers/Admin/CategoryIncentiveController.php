<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CategoryIncentive;
use Illuminate\Http\Request;

class CategoryIncentiveController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:show category incentives')->only('index');
        $this->middleware('permission:create category incentive')->only('store');
        $this->middleware('permission:edit category incentive')->only('update');
        $this->middleware('permission:delete category incentive')->only('destroy');
    }

    public function index($scope)
    {
        $categoryIncentives = CategoryIncentive::paginate(30);

        return view('Admin.employees.categories.incentives.index', compact('scope', 'categoryIncentives'));
    }

    public function store(Request $request, $scope)
    {
        $validate = $this->checkValidation($request);

        CategoryIncentive::create($validate);

        return back()->with('success', __('admin.storeSuccessMessage'));
    }

    public function update(Request $request, $scope, $id)
    {
        $validate = $this->checkValidation($request, $id);

        CategoryIncentive::where('id', $id)->update($validate);

        return back()->with('success', __('admin.updateSuccessMessage'));
    }

    public function destroy($id)
    {
        CategoryIncentive::where('id', $id)->delete();
        return back()->with('success', __('admin.deleteSuccessMessage'));
    }

    public function checkValidation($input, $id = null)
    {
        return $input->validate([
            'name' => 'required|max:190|unique:category_incentives,name,'.$id.',id,deleted_at,NULL',
        ]);
    }
}
