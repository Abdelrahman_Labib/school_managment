<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AcademicYear;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AcademicYearController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:show academic year')->only('index');
        $this->middleware('permission:create academic year')->only('store');
        $this->middleware('permission:edit academic year')->only('update');
        $this->middleware('permission:delete academic year')->only('destroy');
    }

    public function index($scope)
    {
        $academics =
        DB::table('academic_years')
        ->select([
            'academic_years.id',
            'academic_years.year',
            'academic_years.title',
            DB::raw('COUNT(student_class_rooms.student_id) as student_count')
        ])
        ->leftJoin('school_grades', 'academic_years.id', '=', 'school_grades.academic_year_id')
        ->leftJoin('class_rooms', 'school_grades.id', '=', 'class_rooms.school_grade_id')
        ->leftJoin('student_class_rooms', 'class_rooms.id', '=','student_class_rooms.class_room_id')
        ->groupBy('academic_years.id')
        ->where('academic_years.deleted_at', Null)
        ->orderBy('academic_years.id', 'desc')
        ->paginate(20);

        return view('Admin.academic_years.index', compact('academics', 'scope'));
    }

    public function store(Request $request, $scope)
    {
        $validate = $this->checkValidation($request);

        AcademicYear::create($validate);

        return back()->with('success', __('admin.storeSuccessMessage'));
    }

    public function update(Request $request, $scope, $id)
    {
        $validate = $this->checkValidation($request, $id);

        AcademicYear::where('id', $id)->update($validate);

        return back()->with('success', __('admin.updateSuccessMessage'));
    }

    public function destroy($scope, $id)
    {
        AcademicYear::where('id', $id)->delete();
        return back()->with('success', __('admin.deleteSuccessMessage'));
    }

    public function checkValidation($input,$id = null)
    {
        return $input->validate([
            'year'   => 'required|unique:academic_years,year,'.$id.',id,deleted_at,NULL',
            'title'  => 'sometimes|unique:academic_years,title,'.$id.',id,deleted_at,NULL',
        ]);
    }
}
