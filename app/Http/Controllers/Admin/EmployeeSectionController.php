<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\EmployeeSection;
use Illuminate\Http\Request;

class EmployeeSectionController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:show employee sections')->only('index');
        $this->middleware('permission:create employee section')->only('store');
        $this->middleware('permission:edit employee section')->only('update');
        $this->middleware('permission:delete employee section')->only('destroy');
    }

    public function index($scope)
    {
        $employeeSections = EmployeeSection::paginate(20);

        return view('Admin.employees.sections.index', compact('scope', 'employeeSections'));
    }

    public function store(Request $request, $scope)
    {
        $validate = $this->checkValidation($request);

        EmployeeSection::create($validate);

        return back()->with('success', __('admin.storeSuccessMessage'));
    }

    public function update(Request $request, $scope, $id)
    {
        $validate = $this->checkValidation($request, $id);

        EmployeeSection::where('id', $id)->update($validate);

        return back()->with('success', __('admin.updateSuccessMessage'));
    }

    public function destroy($id)
    {
        EmployeeSection::where('id', $id)->delete();
        return back()->with('success', __('admin.deleteSuccessMessage'));
    }

    public function checkValidation($input, $id = null)
    {
        return $input->validate([
            'name' => 'required|max:190|unique:employee_sections,name,'.$id.',id,deleted_at,NULL',
        ]);
    }
}
