<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\School;
use Illuminate\Http\Request;

class SchoolController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:show school')->only('index');
        $this->middleware('permission:create school')->only(['create', 'store']);
        $this->middleware('permission:edit school')->only(['edit', 'update']);
    }

    public function index($scope)
    {
        $school = School::first();
        return view('Admin.school.index', compact('school', 'scope'));
    }

    public function create($scope)
    {
        return view('Admin.school.create', compact('scope'));
    }

    public function store(Request $request, $scope)
    {
        $validate = $this->checkValidation($request);

        School::create($validate);

        return redirect()->route('admin.school_info.index', $scope)->with('success', __('admin.storeSuccessMessage'));
    }

    public function show($id)
    {
        //
    }

    public function edit($scope, $id)
    {
        $school = School::find($id);
        return view('Admin.school.edit', compact('school', 'scope'));
    }

    public function update(Request $request, $id, $scope)
    {
        $validate = $this->checkValidation($request);

        School::first()->update($validate);

        return redirect('/school/admin/school_info')->with('success', __('admin.updateSuccessMessage'));
    }

    public function destroy($id)
    {
        //
    }

    public function checkValidation($input)
    {
        return $input->validate([
            'name'                 => 'required',
            'email'                => 'required',
            'phone'                => 'required',
            'address'              => 'required',
            'logo'                 => 'sometimes|image',
            'header_invoice_image' => 'sometimes|image',
            'stamp'                => 'sometimes|image',
        ]);
    }
}
