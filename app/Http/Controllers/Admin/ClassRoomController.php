<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ClassRoom;
use App\Models\SchoolGrade;
use Illuminate\Http\Request;

class ClassRoomController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:show classrooms')->only('index');
        $this->middleware('permission:create classrooms')->only('store');
        $this->middleware('permission:edit classrooms')->only('update');
        $this->middleware('permission:delete classrooms')->only('destroy');
    }

    public function index($scope)
    {
        $class_rooms = ClassRoom::with('school_grade')->paginate(20);
        $school_grades = SchoolGrade::get();

        return view('Admin.class_rooms.index', compact('class_rooms', 'scope', 'school_grades'));
    }

    public function create($scope)
    {
        //
    }

    public function store(Request $request, $scope)
    {
        $validate = $this->checkValidation($request);

        ClassRoom::create($validate);

        return back()->with('success', __('admin.storeSuccessMessage'));
    }

    public function show($id)
    {
        //
    }

    public function edit($scope, $id)
    {
        //
    }

    public function update(Request $request, $scope, $id)
    {
        $validate = $this->checkValidation($request);

        ClassRoom::where('id', $id)->update($validate);

        return redirect('/school/admin/class_rooms')->with('success', __('admin.updateSuccessMessage'));
    }

    public function destroy($id)
    {
        //
    }

    public function checkValidation($input)
    {
        return $input->validate([
            'school_grade_id'   => 'required|exists:school_grades,id',
            'name'              => 'required|max:180',
            'price'             => 'required|numeric',
        ]);
    }
}
