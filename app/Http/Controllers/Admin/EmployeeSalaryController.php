<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\EmployeeSalary;
use App\Models\School;
use Illuminate\Http\Request;

class EmployeeSalaryController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:show employee salaries')->only('index');
        $this->middleware('permission:create employee salary')->only('store');
        $this->middleware('permission:edit employee salary')->only('update');
        $this->middleware('permission:delete employee salary')->only('destroy');
    }

    public function index($scope)
    {
        $employeeSalaries = EmployeeSalary::paginate(30);
        $employees = Employee::select('id', 'name')->get();

        return view('Admin.employees.salaries.index', compact('scope', 'employeeSalaries', 'employees'));
    }

    public function store(Request $request, $scope)
    {
        $validate = $this->checkValidation($request);

        EmployeeSalary::create($validate);

        return back()->with('success', __('admin.storeSuccessMessage'));
    }

    public function show($scope, $id)
    {
        $receipt = EmployeeSalary::with('employee')->find($id);
        $receipt->name = $receipt->employee->name;
        $receipt->cost = $receipt->salary;
        $school = School::select('header_invoice_image', 'stamp')->first();

        return view('Admin.layouts.receipt_exchange', compact('receipt', 'school'));
    }

    public function update(Request $request, $scope, $id)
    {
        $validate = $this->checkValidation($request);

        EmployeeSalary::where('id', $id)->update($validate);

        return back()->with('success', __('admin.updateSuccessMessage'));
    }

    public function destroy($id)
    {
        EmployeeSalary::where('id', $id)->delete();
        return back()->with('success', __('admin.deleteSuccessMessage'));
    }

    public function checkValidation($input)
    {
        return $input->validate([
            'employee_id' => 'required|exists:employees,id',
            'salary'      => 'required|numeric',
            'date'        => 'required|date',
            'notes'       => 'sometimes|max:400',
        ]);
    }
}
