<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\School;
use App\Models\SchoolExpense;
use Illuminate\Http\Request;

class ExpenseController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:show expenses')->only('index');
        $this->middleware('permission:create expenses')->only('store');
        $this->middleware('permission:edit expenses')->only('update');
        $this->middleware('permission:delete expenses')->only('destroy');
    }

    public function index($scope)
    {
        $expenses = SchoolExpense::with('category')->get();
        $categories = Category::all();
        return view('Admin.expenses.index', compact('expenses', 'scope', 'categories'));
    }

    public function create(Request $request, $scope)
    {
        //
    }

    public function store(Request $request, $scope)
    {
        $validate = $this->checkValidation($request);

        SchoolExpense::create($validate);

        return back()->with('success', __('admin.storeSuccessMessage'));
    }

    public function show($scope, $id)
    {
        $receipt = SchoolExpense::with('category')->find($id);
        $receipt->name = $receipt->category->name;
        $receipt->cost = $receipt->expense;
        $school = School::select('header_invoice_image', 'stamp')->first();

        return view('Admin.layouts.receipt_exchange', compact('receipt', 'school'));
    }

    public function edit(Request $request, $scope, $id)
    {
        //
    }

    public function update(Request $request, $scope, $id)
    {
        $request->merge(['id' => $id]);
        $request->validate([
            'id' => 'required|exists:school_expenses,id',
        ]);

        $validate = $this->checkValidation($request);

        SchoolExpense::where('id', $id)->update($validate);

        return back()->with('success', __('admin.updateSuccessMessage'));
    }

    public function destroy($id)
    {
        //
    }

    public function checkValidation($input)
    {
        return $input->validate([
            'category_id' => 'required|exists:categories,id',
            'name'        => 'required',
            'expense'     => 'required|numeric',
            'date'        => 'required|date',
            'notes'       => 'required',
        ]);
    }
}
