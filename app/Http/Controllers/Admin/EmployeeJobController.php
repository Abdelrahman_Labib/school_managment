<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\EmployeeJob;
use App\Models\EmployeeSection;
use Illuminate\Http\Request;

class EmployeeJobController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:show employee jobs')->only('index');
        $this->middleware('permission:create employee job')->only('store');
        $this->middleware('permission:edit employee job')->only('update');
        $this->middleware('permission:delete employee job')->only('destroy');
    }

    public function index($scope)
    {
        $employeeJobs = EmployeeJob::paginate(20);
        $employeeSections = EmployeeSection::all();

        return view('Admin.employees.jobs.index', compact('scope', 'employeeJobs', 'employeeSections'));
    }

    public function store(Request $request, $scope)
    {
        $validate = $this->checkValidation($request);

        EmployeeJob::create($validate);

        return back()->with('success', __('admin.storeSuccessMessage'));
    }

    public function update(Request $request, $scope, $id)
    {
        $validate = $this->checkValidation($request, $id);

        EmployeeJob::where('id', $id)->update($validate);

        return back()->with('success', __('admin.updateSuccessMessage'));
    }

    public function destroy($id)
    {
        EmployeeJob::where('id', $id)->delete();
        return back()->with('success', __('admin.deleteSuccessMessage'));
    }

    public function checkValidation($input, $id = null)
    {
        return $input->validate([
            'employee_section_id' => 'required|exists:employee_sections,id',
            'name'                => 'required|max:190|unique:employee_jobs,name,'.$id.',id,employee_section_id,'.$input->employee_section_id.',deleted_at,NULL',
        ]);
    }
}
