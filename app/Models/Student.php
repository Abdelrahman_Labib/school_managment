<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $guarded = [];

    public function setImageAttribute($value)
    {
        if($value){
            $name = $value->store('students', 'public');
            $this->attributes['image'] = $name;
        }
    }

    public function getImageAttribute($value)
    {
        if($value){
            return asset('storage/' . $value);
        }
    }

    public function classrooms_sections()
    {
        return $this->hasMany(StudentClassRoom::class);
    }

    public function student_payment()
    {
        return $this->hasMany(StudentPayment::class);
    }

    public function studentPaymentAgreement($agreement_id)
    {
        return StudentPayment::where('student_id', $this->id)->where('agreement_id', $agreement_id)->get();
    }

    public function attachments()
    {
        return $this->hasMany(StudentAttachment::class);
    }

    public function agreements()
    {
        return $this->hasMany(Agreement::class);
    }
}
