<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudentPayment extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $guarded = [];

    public function agreement()
    {
        return $this->belongsTo(Agreement::class);
    }

    public function type()
    {
        return $this->belongsTo(TypePayment::class);
    }

    public function student()
    {
        return $this->belongsTo(Student::class);
    }
}
