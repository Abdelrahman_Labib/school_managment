<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SchoolGrade extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $guarded = [];

    public function academic_year()
    {
        return $this->belongsTo(AcademicYear::class);
    }

    public function class_room()
    {
        return $this->hasMany(ClassRoom::class);
    }
}
