<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function setLogoAttribute($value)
    {
        if($value){
            $name = $value->store('school', 'public');
            $this->attributes['logo'] = $name;
        }
    }

    public function getLogoAttribute($value)
    {
        if($value){
            return asset('storage/' . $value);
        }
    }

    public function setHeaderInvoiceImageAttribute($value)
    {
        if($value){
            $name = $value->store('header_invoice_image', 'public');
            $this->attributes['header_invoice_image'] = $name;
        }
    }

    public function getHeaderInvoiceImageAttribute($value)
    {
        if($value){
            return asset('storage/' . $value);
        }
    }

    public function setStampAttribute($value)
    {
        if($value){
            $name = $value->store('stamp', 'public');
            $this->attributes['stamp'] = $name;
        }
    }

    public function getStampAttribute($value)
    {
        if($value){
            return asset('storage/' . $value);
        }
    }
}
