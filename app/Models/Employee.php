<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $guarded = [];

    public function job()
    {
        return $this->belongsTo(EmployeeJob::class, 'employee_job_id');
    }

    public function setImageAttribute($value)
    {
        if($value){
            $name = $value->store('employees', 'public');
            $this->attributes['image'] = $name;
        }
    }

    public function getImageAttribute($value)
    {
        if($value){
            return asset('storage/' . $value);
        }
    }
}
