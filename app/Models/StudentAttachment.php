<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentAttachment extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function setAttachmentAttribute($value)
    {
        if($value){
            $name = $value->store('attachments', 'public');
            $this->attributes['attachment'] = $name;
        }
    }

    public function getAttachmentAttribute($value)
    {
        if($value){
            return asset('storage/' . $value);
        }
    }
}
