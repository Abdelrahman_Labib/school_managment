<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClassRoom extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $guarded = [];

    public function school_grade()
    {
        return $this->belongsTo(SchoolGrade::class);
    }

    public function student_class_rooms()
    {
        return $this->hasMany(StudentClassRoom::class);
    }
}
