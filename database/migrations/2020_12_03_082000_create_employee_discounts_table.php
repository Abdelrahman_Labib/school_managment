<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_discounts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('category_discount_id');
            $table->unsignedBigInteger('employee_id');
            $table->float('cost');
            $table->date('date');
            $table->text('notes');
            $table->timestamps();

            $table->foreign('category_discount_id')->references('id')->on('category_discounts')->onDelete('cascade');
            $table->foreign('employee_id')->references('id')->on('employees')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_discounts');
    }
}
