<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $isPermissionSeededBefore =
            DB::table('permissions')->where('guard_name', 'admin')->exists();

        if($isPermissionSeededBefore) {
            return 0;
        }

        DB::table('permissions')->insert([
            'name'       => 'show dashboard',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'show school',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'create school',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'edit school',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'show academic year',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'create academic year',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'edit academic year',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'delete academic year',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'show school grades',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'create school grades',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'edit school grades',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'delete school grades',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'show classrooms',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'create classrooms',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'edit classrooms',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'delete classrooms',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'show sections',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'create sections',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'edit sections',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'delete sections',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'show admins',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'create admin',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'edit admin',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'show students',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'create student',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'view student',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'view student information',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'view student agreements',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'view student attachments',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'add student payment',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'add student attachment',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'edit student',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'transfer student',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'delete student',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'show expense category',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'create expense category',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'edit expense category',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'delete expense category',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'show expenses',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'create expenses',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'edit expenses',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'delete expenses',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'show agreements',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'create agreements',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'view agreements',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'edit agreements',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'delete agreements',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'show report payments',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'view report payments receipt',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'show report expenses',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'view report expenses receipt',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'show report agreements',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'view report agreements',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'show employee sections',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'create employee section',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'edit employee section',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'delete employee section',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'show employee jobs',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'create employee job',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'edit employee job',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'delete employee job',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'show employees',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'create employee',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'view employee',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'edit employee',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'delete employee',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'show employee agreements',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'create employee agreement',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'edit employee agreement',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'delete employee agreement',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'show employee salaries',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'create employee salary',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'edit employee salary',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'delete employee salary',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'show category allowances',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'create category allowance',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'edit category allowance',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'delete category allowance',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'show employee allowances',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'create employee allowance',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'edit employee allowance',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'delete employee allowance',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'show category incentives',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'create category incentive',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'edit category incentive',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'delete category incentive',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'show employee incentives',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'create employee incentive',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'edit employee incentive',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'delete employee incentive',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'show category discounts',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'create category discount',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'edit category discount',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'delete category discount',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'show employee discounts',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'create employee discount',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'edit employee discount',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('permissions')->insert([
            'name'       => 'delete employee discount',
            'guard_name' => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
