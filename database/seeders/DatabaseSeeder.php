<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PermissionSeeder::class);
        $this->call(AdminPermissionSeeder::class);

        DB::table('type_payments')->insert([
            'name_ar'    => 'كاش',
            'name_en'    => 'Cash',
            'created_at' => now(),
        ]);
        DB::table('type_payments')->insert([
            'name_ar'    => 'بنك',
            'name_en'    => 'Bank',
            'created_at' => now(),
        ]);
        DB::table('type_payments')->insert([
            'name_ar'    => 'شيك',
            'name_en'    => 'Check',
            'created_at' => now(),
        ]);
    }
}
