<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;

class AdminPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = DB::table('admins')->where('id', 1)->exists();
        if(!$admin){
            DB::table('admins')->insert([
                'active'    => 1,
                'name'      => 'admin',
                'email'     => 'admin@admin.com',
                'phone'     => '123456',
                'password'  => Hash::make('123123'),
            ]);
        }
        $isPermissionSeededBefore = DB::table('model_has_permissions')->where('model_id', 1)->exists();
        if($isPermissionSeededBefore){
            return 0;
        }

        $permissions = Permission::where('guard_name','admin')->get();
        foreach($permissions as $permission){
            DB::table('model_has_permissions')->insert([
                'permission_id'  => $permission->id,
                'model_type'     => 'App\Models\Admin',
                'model_id'       => 1,
            ]);
        }
    }
}
