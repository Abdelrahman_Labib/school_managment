<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', 'AuthController@viewLoginPage')->name('login');
Route::post('login', 'AuthController@authenticate')->name('authenticate');
Route::get('logout', 'AuthController@logout')->name('logout');

Route::middleware(['admin'])->group(function () {
    Route::get('profile', 'ProfileController@profile')->name('profile');
    Route::get('dashboard', 'HomeController@index')->name('dashboard');
    Route::resource('school_info', 'SchoolController');
    Route::resource('admins', 'AdminController');
    Route::resource('academic_years', 'AcademicYearController');
    Route::resource('school_grades', 'SchoolGradeController');
    Route::resource('class_rooms', 'ClassRoomController');
    Route::resource('sections', 'SectionController');
    Route::get('section/{id}', 'SectionController@ajaxSelection');
    Route::resource('students', 'StudentController');
    Route::post('students/payment', 'StudentController@payment')->name('students.payment');
    Route::get('students/payment/receipt/{id}', 'StudentController@payment_receipt')->name('students.payment_receipt');
    Route::post('students/attachment', 'StudentController@attachment')->name('students.attachment');
    Route::post('students/transfer', 'StudentController@transfer')->name('students.transfer');
    Route::resource('category_expenses', 'CategoryExpensesController');
    Route::resource('expenses', 'ExpenseController');
    Route::resource('agreements', 'AgreementController');
    Route::get('agreements/getClassRoomPrice/{id}', 'AgreementController@getClassRoomPrice');

    Route::prefix('reports')->name('reports.')->group(function (){
        Route::get('payments', 'ReportController@payments')->name('payments');
        Route::get('payments/receipt/{id}', 'ReportController@payments_receipt')->name('payments.receipt');
        Route::get('expenses', 'ReportController@expenses')->name('expenses');
        Route::get('expenses/receipt/{id}', 'ReportController@expenses_receipt')->name('expenses.receipt');
        Route::get('agreements', 'ReportController@agreements')->name('agreements');
    });

    Route::prefix('employees')->name('employees.')->group(function(){
        Route::resource('sections', 'EmployeeSectionController');
        Route::resource('jobs', 'EmployeeJobController');
        Route::resource('salaries', 'EmployeeSalaryController');
        Route::resource('agreements', 'EmployeeAgreementController');
        Route::resource('allowances', 'EmployeeAllowanceController');
        Route::resource('incentives', 'EmployeeIncentiveController');
        Route::resource('discounts', 'EmployeeDiscountController');
    });
    Route::resource('employees', 'EmployeeController');

    Route::prefix('categories')->name('categories.')->group(function(){
        Route::resource('allowances', 'CategoryAllowanceController');
        Route::resource('incentives', 'CategoryIncentiveController');
        Route::resource('discounts', 'CategoryDiscountController');
    });

    Route::get('set-locale/{locale}', function ($scope, $locale) {
        session()->put('locale', $locale);
        return redirect()->back();
    })->name('locale.setting');
});
