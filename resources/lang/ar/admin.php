<?php

return [
    'dashboardSideBar'      => 'الرئيسية',
    'schoolSideBar'         => 'بيانات المدرسة',
    'adminSideBar'          => 'المشرفين',
    'academicYearSideBar'   => 'السنوات الدراسية',
    'schoolGradesSideBar'   => 'المراحل الدراسية',
    'classRoomSideBar'      => 'الصفوف',
    'sectionSideBar'        => 'الشعب',
    'studentSideBar'        => 'الطلاب',
    'categoriesSideBar'     => 'التصنيفات',
    'categoryEmploySideBar' => 'تصنيفات الموظفين',
    'expensesSideBar'       => 'المصاريف',
    'agreementsSideBar'     => 'عقود الطلاب',
    'reportsSideBar'        => 'التقارير',
    'paymentsSideBar'       => 'المدفوعات',
    'informationSideBar'    => 'معلومات',
    'employeeSectSideBar'   => 'أقسام الموظفين',
    'employeeJobSideBar'    => 'وظائف الموظفين',
    'employeesSideBar'      => 'الموظفين',
    'employeeSalarySideBar' => 'رواتب الموظفين',
    'employeeAgreeSideBar'  => 'اتفاقيات الموظفين',
    'employeeAllowSideBar'  => 'بدلات الموظفين',
    'employeeIncenSideBar'  => 'حوافز الموظفين',
    'employeeDiscoSideBar'  => 'خصومات الموظفين',
    'categoryAllowSideBar'  => 'تصنيف البدلات',
    'categoryIncenSideBar'  => 'تصنيف الحوافز',
    'categoryDiscoSideBar'  => 'تصنيف الخصومات',
    'categoryAttribute'     => 'تصنيفات',
    'nameAttribute'         => 'الإسم',
    'emailAttribute'        => 'البريد الإلكتروني',
    'phoneAttribute'        => 'الهاتف',
    'passwordAttribute'     => 'كلمة السر',
    'addressAttribute'      => 'العنوان',
    'logoAttribute'         => 'الشعار',
    'headInvoiceAttribute'  => 'شعار الفاتورة',
    'stampAttribute'        => 'ختم',
    'attachmentAttribute'   => 'المرفق',
    'priceAttribute'        => 'السعر',
    'paymentAttribute'      => 'دفع',
    'paidAttribute'         => 'المدفوع',
    'remainingAttribute'    => 'المتبقي',
    'discountAttribute'     => 'خصم',
    'afrDiscountAttribute'  => 'بعد الخصم',
    'expenseAttribute'      => 'المبلغ',
    'dateAttribute'         => 'تاريخ',
    'startDateAttribute'    => 'تاريخ البداية',
    'endDateAttribute'      => 'تاريخ النهاية',
    'notesAttribute'        => 'ملاحظات',
    'parentNameAttribute'   => 'إسم الوالد',
    'parentPhoneAttribute'  => 'رقم هاتف الوالد',
    'schoolNumberAttribute' => 'رقم الهويه الشخصية بالمدرسة',
    'IdNumberAttribute'     => 'رقم الهويه الشخصية',
    'expiryIdNumAttribute'  => 'تاريخ انتهاء الهويه الشخصية',
    'passportAttribute'     => 'جواز سفر',
    'expiryPassAttribute'   => 'تاريخ انتهاء جواز سفر',
    'imageAttribute'        => 'صورة',
    'amountAttribute'       => 'الكمية',
    'paidDateAttribute'     => 'تاريخ الدفع',
    'paidTypeAttribute'     => 'نوع الدفع',
    'paidTypeNameAttribute' => 'اسم نوع الدفع',
    'agreementAttribute'    => 'رقم العقد',
    'monthAttribute'        => 'شهر',
    'yearAttribute'         => 'السنة',
    'titleAttribute'        => 'العنوان',
    'descriptionAttribute'  => 'الوصف',
    'fromAttribute'         => 'من',
    'toAttribute'           => 'إلي',
    'totalAttribute'        => 'المجموع',
    'genderAttribute'       => 'الجنس',
    'nationalityAttribute'  => 'الجنسية',
    'birthdayAttribute'     => 'تاريخ الميلاد',
    'activeAttribute'       => 'مفعل',
    'notActiveAttribute'    => 'غير مفعل',
    'adminStatusAttribute'  => 'حالة المشرف',
    'salaryAttribute'       => 'الراتب',
    'salaryAgreedAttribute' => 'الراتب المتفق عليه',
    'receiptVoucherAttr'    => 'سند قبض',
    'receiptExchangeAttr'   => 'سند صرف',
    'OperationAttribute'    => 'العمليات',
    'addAdmin'              => 'إضافه مشرف',
    'editAdmin'             => 'تعديل مشرف',
    'addSchoolInfo'         => 'إضافه تفاصيل المدرسة',
    'editSchoolInfo'        => 'تعديل تفاصيل المدرسة',
    'addAcademicYear'       => 'إضافه عام دراسي',
    'editAcademicYear'      => 'تعديل عام دراسي',
    'deleteAcademicYear'    => 'حذف عام دراسي',
    'addSchoolGrade'        => 'إضافة مرحلة دراسي',
    'editSchoolGrade'       => 'تعديل مرحلة دراسي',
    'deleteSchoolGrade'     => 'حذف مرحلة دراسي',
    'addClassRoom'          => 'إضافة صف دراسي',
    'editClassRoom'         => 'تعديل صف دراسي',
    'showSection'           => 'إظهار الشعب',
    'addSection'            => 'إضافة شعبة',
    'editSection'           => 'تعديل شعبة',
    'addStudent'            => 'إضافة طالب',
    'editStudent'           => 'تعديل طالب',
    'showStudent'           => 'إظهار الطالب',
    'showEmployee'          => 'إظهار موظف',
    'addCategory'           => 'إضافة تصنيف',
    'editCategory'          => 'تعديل تصنيف',
    'addExpense'            => 'إضافة مصاريف',
    'editExpense'           => 'تعديل مصاريف',
    'showExpenses'          => 'إظهار المصاريف',
    'expenseCategory'       => 'قسم المصاريف',
    'addPayment'            => 'إضافة دفعة',
    'addAgreement'          => 'إضافة اتفاقية',
    'editAgreement'         => 'تعديل اتفاقية',
    'addAttachment'         => 'إضافة مرفق',
    'addEmployee'           => 'إضافة موظف',
    'editEmployee'          => 'تعديل موظف',
    'addEmployeeSection'    => 'إضافة أقسام الموظفين',
    'editEmployeeSection'   => 'تعديل أقسام الموظفين',
    'addEmployeeJob'        => 'إضافة وظائف الموظفين',
    'editEmployeeJob'       => 'تعديل وظائف الموظفين',
    'addEmployeeSalary'     => 'إضافة رواتب الموظفين',
    'editEmployeeSalary'    => 'تعديل رواتب الموظفين',
    'addEmployeeAgreement'  => 'إضافة اتفاقيات الموظفين',
    'editEmployeeAgreement' => 'تعديل اتفاقيات الموظفين',
    'addEmployeeAllowance'  => 'إضافة بدلات الموظفين',
    'editEmployeeAllowance' => 'تعديل بدلات الموظفين',
    'addEmployeeIncentive'  => 'إضافة حوافز الموظفين',
    'editEmployeeIncentive' => 'تعديل حوافز الموظفين',
    'addEmployeeDiscount'   => 'إضافة خصومات الموظفين',
    'editEmployeeDiscount'  => 'تعديل خصومات الموظفين',
    'addCategoryAllowance'  => 'إضافة تصنيف البدلات',
    'editCategoryAllowance' => 'تعديل تصنيف البدلات',
    'addCategoryIncentive'  => 'إضافة تصنيف الحوافز',
    'editCategoryIncentive' => 'تعديل تصنيف الحوافز',
    'addCategoryDiscount'   => 'إضافة تصنيف الخصومات',
    'editCategoryDiscount'  => 'تعديل تصنيف الخصومات',
    'storeSuccessMessage'   => 'تم الإضافة بنجاح',
    'updateSuccessMessage'  => 'تم التعديل بنجاح',
    'deleteSuccessMessage'  => 'تم الحذف بنجاح',
    'saveButton'            => 'حفظ',
    'closeButton'           => 'إغلاق',
    'searchButton'          => 'بحث',
    'selectClassRoomMsg'    => 'قم باختيار الصف اولا!',
    'selectClassRoom'       => 'قم باختيار الصف',
    'selectAcademicYear'    => 'قم باختيار السنة الدراسية',
    'selectSchoolGrade'     => 'قم باختيار المرحلة الدراسية',
    'selectSection'         => 'قم باختيار الشعبة',
    'selectCategory'        => 'قم باختيار تصنيف',
    'selectStudent'         => 'قم باختيار طالب',
    'selectAdminStatus'     => 'قم بتحديد حالة المشرف',
    'selectEmployeeSect'    => 'قم بتحديد أقسام الموظفين',
    'selectEmployeeJob'     => 'قم بتحديد وظائف الموظفين',
    'selectEmployee'        => 'قم بتحديد موظف',
    'contactInformation'    => 'معلومات التواصل',
    'wrongValue'            => 'قيمة خاطئه',
    'classRoomSingleWord'   => 'صف',
    'sectionSingleWord'     => 'شعبة',
    'countStudents'         => 'عدد الطلاب',
    'transferStudent'       => 'نقل الطالب إلى الصف الجديد',
    'totalFees'             => 'إجمالي الرسوم',
    'deleteMessage'         => 'هل انت متأكد من حذف, ',
];
