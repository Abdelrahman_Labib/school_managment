@extends('Admin.layouts.app')
@section('title') {{__('admin.paymentsSideBar')}} @endsection
@section('css')
    <link href="/admin/lib/datatables.net-dt/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="/admin/lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/admin/css/datepicker.css">
@endsection
@section('content')

    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <span class="breadcrumb-item">{{__('admin.reportsSideBar')}}</span>
            <span class="breadcrumb-item active">{{__('admin.paymentsSideBar')}}</span>
        </nav>
    </div><!-- br-pageheader -->
    @include('Admin.layouts.search', ['route' => route('admin.reports.payments', $scope)])
    <div class="br-pagetitle">

    </div><!-- d-flex -->
    <!-- Button trigger modal -->
    <div class="br-section-wrapper">
    @include('Admin.layouts.message')
        <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
                <thead>
                <tr>
                    <th class="wd-5p">#</th>
                    <th class="wd-5p">{{__('admin.agreementAttribute')}}</th>
                    <th class="wd-5p">{{__('admin.nameAttribute')}}</th>
                    <th class="wd-5p">{{__('admin.parentPhoneAttribute')}}</th>
                    <th class="wd-5p">{{__('admin.dateAttribute')}}</th>
                    <th class="wd-5p">{{__('admin.monthAttribute')}}</th>
                    <th class="wd-5p">{{__('admin.paidTypeAttribute')}}</th>
                    <th class="wd-5p">{{__('admin.notesAttribute')}}</th>
                    <th class="wd-5p">{{__('admin.paidAttribute')}}</th>
                    @if(auth()->guard('admin')->user()->hasPermissionTo('view report payments receipt'))
                    <th class="wd-10p">{{__('admin.OperationAttribute')}}</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @foreach($payments as $payment)
                    <tr>
                        <td>{{$payment->id}}</td>
                        <td>{{'AGR-'.$payment->agreement_id}}</td>
                        <td>{{$payment->student->name}}</td>
                        <td>{{$payment->student->parent_phone}}</td>
                        <td>{{$payment->paid_at}}</td>
                        <td>{{\Carbon\Carbon::parse($payment->paid_at)->format('F Y')}} </td>
                        <td>{{$payment->type->name_en}}</td>
                        <td>{{$payment->description}}</td>
                        <td>{{$payment->paid}}</td>
                        @if(auth()->guard('admin')->user()->hasPermissionTo('view report payments receipt'))
                        <td>
                            <a target="_blank" href="{{route('admin.reports.payments.receipt', [$scope, $payment->id])}}" class="btn btn-dark btn-sm">
                                <i class="fa fa-print"></i>
                            </a>

                        </td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
                <tr style="background-color: #1a202c">
                    <td colspan="8" style="text-align: center; color: white">{{__('admin.totalAttribute')}}</td>
                    <td style="color: white">{{$payments->sum('paid')}}</td>
                    <td></td>
                </tr>
            </table>
        </div><!-- table-wrapper -->
    </div>

@endsection

@section('script')
    <script src="/admin/lib/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/admin/lib/datatables.net-dt/js/dataTables.dataTables.min.js"></script>
    <script src="/admin/lib/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="/admin/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js"></script>
    <script src="/admin/js/datepicker.js"></script>
    <script>
        // Datepicker
        $('.fc-datepicker').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true
        });

        $(function(){
            'use strict';

            $('#datatable1').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });

            // Select2
            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

            $('[data-toggle="datepicker"]').datepicker({
                format: 'yyyy-mm-dd',
            });
        });

    </script>
@endsection
