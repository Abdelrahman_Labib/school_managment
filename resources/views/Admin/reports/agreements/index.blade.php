@extends('Admin.layouts.app')
@section('title') {{__('admin.agreementsSideBar')}} @endsection
@section('css')
    <link href="/admin/lib/datatables.net-dt/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="/admin/lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/admin/css/datepicker.css">
@endsection
@section('content')

    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <span class="breadcrumb-item active">{{__('admin.agreementsSideBar')}}</span>
        </nav>
    </div><!-- br-pageheader -->
    @include('Admin.layouts.search', ['route' => route('admin.reports.agreements', $scope)])
    <div class="br-pagetitle">

    </div><!-- d-flex -->
    <!-- Button trigger modal -->
    <div class="br-section-wrapper">
    @include('Admin.layouts.message')
        <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th class="wd-5p">#</th>
                    <th class="wd-10p">{{__('admin.schoolSideBar')}}</th>
                    <th class="wd-10p">{{__('admin.studentSideBar')}}</th>
                    <th class="wd-10p">{{__('admin.startDateAttribute')}}</th>
                    <th class="wd-10p">{{__('admin.endDateAttribute')}}</th>
                    <th class="wd-10p">{{__('admin.paymentAttribute')}}</th>
                    <th class="wd-10p">{{__('admin.discountAttribute')}}</th>
                    <th class="wd-20p">{{__('admin.afrDiscountAttribute')}}</th>
                    <th class="wd-20p">{{__('admin.paidAttribute')}}</th>
                    <th class="wd-20p">{{__('admin.remainingAttribute')}}</th>
                    @if(auth()->guard('admin')->user()->hasPermissionTo('view report agreements'))
                    <th class="wd-25p">{{__('admin.OperationAttribute')}}</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @foreach($agreements as $agreement)
                    <tr>
                        <td>{{$agreement->id}}</td>
                        <td>{{$agreement->school->name}}</td>
                        <td>
                            <a target="_blank" href="{{route('admin.students.show', [$scope, $agreement->student->id])}}">
                            {{$agreement->student->name}}
                            </a>
                        </td>
                        <td>{{$agreement->start_date}}</td>
                        <td>{{$agreement->end_date}} </td>
                        <td>{{$agreement->payment}}</td>
                        <td>{{$agreement->discount}}</td>
                        <td>{{$agreement->after_discount}}</td>
                        <td>{{$agreement->student->student_payment->sum('paid')}}</td>
                        @if($agreement->student->studentPaymentAgreement($agreement->id)->last())
                            <td>
                                {{$agreement->student->studentPaymentAgreement($agreement->id)->last()['remain']}}
                            </td>
                        @else
                            <td>
                                {{$agreement->after_discount}}
                            </td>
                        @endif
                        @if(auth()->guard('admin')->user()->hasPermissionTo('view report agreements'))
                        <td>
                            <a target="_blank" href="{{route('admin.agreements.show',[$scope, $agreement->id])}}" class="btn btn-dark btn-sm">
                                <i class="fa fa-print"></i>
                            </a>
                        </td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
                <tr style="background-color: #1a202c">
                    <td colspan="5" style="text-align: center; color: white">{{__('admin.totalAttribute')}}</td>
                    <td style="color: white">{{$agreements->sum('payment')}}</td>
                    <td style="color: white">{{$agreements->sum('discount')}}</td>
                    <td style="color: white">{{$agreements->sum('after_discount')}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div><!-- table-wrapper -->
    </div>

@endsection

@section('script')
    <script src="/admin/lib/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/admin/lib/datatables.net-dt/js/dataTables.dataTables.min.js"></script>
    <script src="/admin/lib/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="/admin/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js"></script>
    <script src="/admin/js/datepicker.js"></script>
    <script>

        $(function(){
            'use strict';

            $('#datatable1').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });

            // Select2
            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

            $('[data-toggle="datepicker"]').datepicker({
                format: 'yyyy-mm-dd',
            });
        });


    </script>

@endsection
