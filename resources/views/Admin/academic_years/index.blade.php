@extends('Admin.layouts.app')
@section('title') {{__('admin.academicYearSideBar')}} @endsection
@section('content')
    <link rel="stylesheet" href="/admin/css/yearpicker.css" />

    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <span class="breadcrumb-item active">{{__('admin.academicYearSideBar')}}</span>
        </nav>
    </div><!-- br-pageheader -->
    <div class="br-pagetitle">

    </div><!-- d-flex -->
    <!-- Button trigger modal -->
    @if(auth()->guard('admin')->user()->hasPermissionTo('create academic year'))
    <div style="padding:15px;float: right">
        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#addAcademicYear">
            <i class="fa fa-plus"></i>
            {{__('admin.addAcademicYear')}}
        </button>
    </div>
    @endif
{{--    <input type="text" class="yearpicker form-control" value="" />--}}
    <div class="br-section-wrapper">
        @include('Admin.layouts.message')
        <div class="bd bd-gray-300 rounded table-responsive">
            <table class="table table-striped mg-b-0">
                <thead>
                <tr>
                    <th>{{__('admin.yearAttribute')}}</th>
                    <th>{{__('admin.titleAttribute')}}</th>
                    <th>{{__('admin.countStudents')}}</th>
                    @if(auth()->guard('admin')->user()->hasPermissionTo('edit academic year') || auth()->guard('admin')->user()->hasPermissionTo('delete academic year'))
                    <th>{{__('admin.OperationAttribute')}}</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @foreach($academics as $academic)
                    <tr>
                        <td>{{$academic->year}}</td>
                        <td>{{$academic->title}}</td>
                        <td>
                            <a href="{{route('admin.students.index', [$scope, 'academic_year' => $academic->id])}}">
                                {{$academic->student_count}}
                            </a>
                        </td>
                        @if(auth()->guard('admin')->user()->hasPermissionTo('edit academic year') || auth()->guard('admin')->user()->hasPermissionTo('delete academic year'))
                        <td>
                            @if(auth()->guard('admin')->user()->hasPermissionTo('edit academic year'))
                            <button type="button" class="btn btn-warning btn-sm" onclick="openModalEdit({{json_encode($academic)}})" title="{{__('admin.editAcademicYear')}}">
                                <i class="fa fa-edit"></i>
                            </button>
                            @endif

                            @if(auth()->guard('admin')->user()->hasPermissionTo('delete academic year'))
                            <button type="button" class="btn btn-danger btn-sm" onclick="openModalDelete({{json_encode($academic)}})" title="{{__('admin.deleteAcademicYear')}}">
                                <i class="fa fa-trash"></i>
                            </button>
                            @endif
                        </td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div><!-- bd -->
        {{$academics->links('Admin.layouts.pagination')}}
    </div>

    @if(auth()->guard('admin')->user()->hasPermissionTo('create academic year'))
    <!-- Modal Add -->
    <div class="modal fade" id="addAcademicYear" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form class="modal-content" method="post" action="{{route('admin.academic_years.store', $scope)}}">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('admin.addAcademicYear')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <label class="col-sm-4 form-control-label">{{__('admin.yearAttribute')}} <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" name="year" class="yearpicker form-control" value="" required/>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'year'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.titleAttribute')}} <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" name="title" class="form-control" placeholder="{{__('admin.titleAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'title'])
                    </div><!-- row -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                    <button type="submit" class="btn btn-info btn-sm">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                </div>
            </form>
        </div>
    </div>
    @endif

    @if(auth()->guard('admin')->user()->hasPermissionTo('edit academic year'))
    <!-- Modal Edit -->
    <div class="modal fade" id="editAcademicYear" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form class="modal-content" method="post" id="edit_form">
                @csrf
                @method('put')
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('admin.editAcademicYear')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <label class="col-sm-4 form-control-label">{{__('admin.yearAttribute')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" name="year" class="yearpicker form-control" value="" required/>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'year'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.titleAttribute')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" name="title" id="edit_title" class="form-control" placeholder="{{__('admin.titleAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'title'])
                    </div><!-- row -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                    <button type="submit" class="btn btn-info btn-sm">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                </div>
            </form>
        </div>
    </div>
    @endif

    @if(auth()->guard('admin')->user()->hasPermissionTo('delete academic year'))
    <!-- Modal Delete -->
    <div class="modal fade" id="deleteAcademicYear" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form class="modal-content" method="post" id="delete_form">
                @csrf
                @method('delete')
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('admin.deleteAcademicYear')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <label class="col-sm-12 form-control-label" style="font-weight: bold">
                            {{__('admin.deleteMessage')}} <label style="color: #bd2130" id="delete_title"></label>
                        </label>
                    </div><!-- row -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                    <button type="submit" class="btn btn-info btn-sm">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                </div>
            </form>
        </div>
    </div>
    @endif
@endsection

@section('script')
    <script src="/admin/js/yearpicker.js"></script>
    <script>
        function openModalEdit(academic_year) {
            const date = new Date();
            $('#edit_year').val(academic_year.year + '-' + (date.getMonth()+1));
            $('#edit_title').val(academic_year.title);
            $('#edit_form').attr('action', '/school/admin/academic_years/' + academic_year.id);
            $('#editAcademicYear').modal('show');
        }

        function openModalDelete(academic_year) {
            $('#delete_title').text(academic_year.title);
            $('#delete_form').attr('action', '/school/admin/academic_years/' + academic_year.id);
            $('#deleteAcademicYear').modal('show');
        }

        $(".yearpicker").yearpicker({
            year: 2021,
            startYear: 2020,
            endYear: 2050,
        });
    </script>
    <style>
        .yearpicker-container {
            z-index: 100000;
        }
    </style>
@endsection
