@extends('Admin.layouts.app')
@section('title') {{__('admin.agreementsSideBar')}} @endsection
@section('css')
    <link href="/admin/lib/datatables.net-dt/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="/admin/lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/admin/css/datepicker.css">
@endsection
@section('content')

    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <span class="breadcrumb-item active">{{__('admin.agreementsSideBar')}}</span>
        </nav>
    </div><!-- br-pageheader -->

    <div class="br-pagetitle">

    </div><!-- d-flex -->
    <!-- Button trigger modal -->
    @if(auth()->guard('admin')->user()->hasPermissionTo('create agreements'))
    <div style="padding:15px;float: right">
        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#addAgreement">
            <i class="fa fa-plus"></i>
            {{__('admin.addAgreement')}}
        </button>
    </div>
    @endif
    <div class="br-section-wrapper">
    @include('Admin.layouts.message')
        <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th class="wd-5p">#</th>
                    <th class="wd-10p">{{__('admin.schoolSideBar')}}</th>
                    <th class="wd-10p">{{__('admin.studentSideBar')}}</th>
                    <th class="wd-10p">{{__('admin.startDateAttribute')}}</th>
                    <th class="wd-10p">{{__('admin.endDateAttribute')}}</th>
                    <th class="wd-10p">{{__('admin.totalFees')}}</th>
                    <th class="wd-10p">{{__('admin.discountAttribute')}}</th>
                    <th class="wd-20p">{{__('admin.afrDiscountAttribute')}}</th>
                    <th class="wd-20p">{{__('admin.paidAttribute')}}</th>
                    <th class="wd-20p">{{__('admin.remainingAttribute')}}</th>
                    @if(auth()->guard('admin')->user()->hasPermissionTo('view agreements') ||
                        auth()->guard('admin')->user()->hasPermissionTo('edit agreements'))
                    <th class="wd-25p">{{__('admin.OperationAttribute')}}</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @foreach($agreements as $agreement)
                    <tr>
                        <td>{{$agreement->id}}</td>
                        <td>{{$agreement->school->name}}</td>
                        <td>
                            <a target="_blank" href="{{route('admin.students.show', [$scope, $agreement->student->id])}}">
                            {{$agreement->student->name}}
                            </a>
                        </td>
                        <td>{{$agreement->start_date}}</td>
                        <td>{{$agreement->end_date}} </td>
                        <td>{{$agreement->payment}}</td>
                        <td>{{$agreement->discount}}</td>
                        <td>{{$agreement->after_discount}}</td>
                        <td>{{$agreement->student->student_payment->sum('paid')}}</td>
                        @if($agreement->student->studentPaymentAgreement($agreement->id)->last())
                            <td>
                                {{$agreement->student->studentPaymentAgreement($agreement->id)->last()['remain']}}
                            </td>
                        @else
                            <td>
                                {{$agreement->after_discount}}
                            </td>
                        @endif
                        @if(auth()->guard('admin')->user()->hasPermissionTo('view agreements') ||
                            auth()->guard('admin')->user()->hasPermissionTo('edit agreements'))
                        <td>
                            @if(auth()->guard('admin')->user()->hasPermissionTo('view agreements'))
                            <a target="_blank" href="{{route('admin.agreements.show',[$scope, $agreement->id])}}" class="btn btn-dark btn-sm">
                                <i class="fa fa-print"></i>
                            </a>
                            @endif

                            @if(auth()->guard('admin')->user()->hasPermissionTo('edit agreements'))
                            <button title="{{__('admin.editAgreement')}}" type="button" class="btn btn-warning btn-sm" onclick="openModalEditEvent({{$agreement}})">
                                <i class="fa fa-edit"></i>
                            </button>
                            @endif

                        </td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
                <tr style="background-color: #1a202c">
                    <td colspan="5" style="text-align: center; color: white">{{__('admin.totalAttribute')}}</td>
                    <td style="color: white">{{$agreements->sum('payment')}}</td>
                    <td style="color: white">{{$agreements->sum('discount')}}</td>
                    <td style="color: white">{{$agreements->sum('after_discount')}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div><!-- table-wrapper -->
    </div>

    @if(auth()->guard('admin')->user()->hasPermissionTo('create agreements'))
    <!-- Modal -->
    <div class="modal fade" id="addAgreement" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form class="modal-content" method="post" action="{{route('admin.agreements.store', $scope)}}">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('admin.addAgreement')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <label class="col-sm-4 form-control-label">{{__('admin.schoolSideBar')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text"  class="form-control" placeholder="{{__('admin.schoolSideBar')}}" value="{{$school->name}}" disabled required>
                            <input type="hidden" name="school_id" value="{{$school->id}}">
                        </div>
                        @include('Admin.layouts.error', ['input' => 'name'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.studentSideBar')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select id="addSelect" class="form-control" name="student_id" style="width: 100%" required>
                                <option selected disabled> {{__('admin.selectStudent')}} </option>
                                @foreach($students as $student)
                                    <option value="{{$student->id}}">
                                        {{$student->name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'student_id'])
                    </div>
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.startDateAttribute')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" name="start_date" class="form-control" placeholder="{{__('admin.startDateAttribute')}}" autocomplete="off" data-toggle="datepicker" value="{{now()->format('yy-m-d')}}">
                        </div>
                        @include('Admin.layouts.error', ['input' => 'start_date'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.endDateAttribute')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" name="end_date" class="form-control" placeholder="{{__('admin.endDateAttribute')}}" autocomplete="off" data-toggle="datepicker">
                        </div>
                        @include('Admin.layouts.error', ['input' => 'end_date'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.totalFees')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="number" name="payment" id="payment" class="form-control" placeholder="{{__('admin.paymentAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'payment'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.discountAttribute')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="number" name="discount" id="discount" class="form-control" placeholder="{{__('admin.discountAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'discount'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.afrDiscountAttribute')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="number" name="after_discount" class="form-control after_discount" placeholder="{{__('admin.afrDiscountAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'after_discount'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.paidAttribute')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="number" id="paid" name="paid" class="form-control" placeholder="{{__('admin.paidAttribute')}}" value="0" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'paid'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.remainingAttribute')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="number" id="remain" name="remain" class="form-control" placeholder="{{__('admin.remainingAttribute')}}" value="0" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'remain'])
                    </div><!-- row -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                    <button type="submit" class="btn btn-info btn-sm">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                </div>
            </form>
        </div>
    </div>
    @endif

    @if(auth()->guard('admin')->user()->hasPermissionTo('edit agreements'))
    <!-- Modal -->
    <div class="modal fade" id="editAgreement" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form class="modal-content" method="post" id="edit_form">
                @csrf
                @method('put')
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('admin.editAgreement')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <label class="col-sm-4 form-control-label">{{__('admin.schoolSideBar')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text"  class="form-control" placeholder="{{__('admin.schoolSideBar')}}" value="{{$school->name}}" disabled required>
                            <input type="hidden" name="school_id" value="{{$school->id}}">
                        </div>
                        @include('Admin.layouts.error', ['input' => 'school_id'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.studentSideBar')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select class="form-control" name="student_id" id="edit_select" style="width: 100%" required>
                                @foreach($students as $student)
                                    <option value="{{$student->id}}">
                                        {{$student->name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'student_id'])
                    </div>
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.startDateAttribute')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="date" name="start_date" class="form-control" placeholder="{{__('admin.startDateAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'start_date'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.endDateAttribute')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="date" name="end_date" class="form-control" placeholder="{{__('admin.endDateAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'end_date'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.totalFees')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="number" name="payment" class="form-control" placeholder="{{__('admin.paymentAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'payment'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.discountAttribute')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="number" name="discount" class="form-control" placeholder="{{__('admin.discountAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'discount'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.afrDiscountAttribute')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="number" name="after_discount" class="form-control after_discount" placeholder="{{__('admin.afrDiscountAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'after_discount'])
                    </div><!-- row -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                    <button type="submit" class="btn btn-info btn-sm">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                </div>
            </form>
        </div>
    </div>
    @endif
@endsection

@section('script')
    <script src="/admin/lib/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/admin/lib/datatables.net-dt/js/dataTables.dataTables.min.js"></script>
    <script src="/admin/lib/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="/admin/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js"></script>
    <script src="/admin/js/datepicker.js"></script>
    <script>

        function openModalEditEvent(agreement) {
            // $("#edit_student_id option").each(function()
            // {
            //     if(agreement.student_id == $(this).val() ){
            //         $("#edit_student_id").val(agreement.student_id);
            //     }
            // });
            $("#edit_select").val(agreement.student_id);
            $("[name='start_date']").val(agreement.start_date);
            $("[name='end_date']").val(agreement.end_date);
            $("[name='payment']").val(agreement.payment);
            $("[name='discount']").val(agreement.discount);
            $("[name='after_discount']").val(agreement.after_discount);
            $('#edit_form').attr('action', '/school/admin/agreements/' + agreement.id);
            $('#editAgreement').modal('show');
        }

        $(function(){
            'use strict';

            $('#datatable1').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });

            // Select2
            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

            $("#addSelect").select2({
                dropdownParent: $("#addAgreement")
            });

            $("#edit_select").select2({
                dropdownParent: $("#editAgreement")
            });

            $('[data-toggle="datepicker"]').datepicker({
                format: 'yyyy-mm-dd',
                autoHide: true,
                zIndex: 2048,
            });

            if({{Session::has('success')}})
            {
                var url = '/school/admin/agreements/'+ {{Session::get('agreement_id')}};
                window.open(url, '_blank')
            }
        });

        $('#addSelect').on('change', function () {
            var parent_id = $('#addSelect').val();
            if (parent_id) {
                $.get( "agreements/getClassRoomPrice/" + parent_id, function( data ) {
                    $('#payment').val(data);
                });
            }
        });

        $('#paid').on('change', function () {
            var remain = 0;
            if($('.after_discount').val() == '' || $('#payment').val() == ''){
                remain;
            }else{
                remain = $('.after_discount').val() -  $('#paid').val() ;
            }

            $('#remain').val(remain);
        });

        $('#discount').on('change', function () {
            var after_discount = $('#payment').val() -  $('#discount').val() ;
            $('.after_discount').val(after_discount);
        });

    </script>

@endsection
