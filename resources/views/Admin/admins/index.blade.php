@extends('Admin.layouts.app')
@section('title') {{__('admin.adminSideBar')}} @endsection
@section('content')

    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <span class="breadcrumb-item active">{{__('admin.adminSideBar')}}</span>
        </nav>
    </div><!-- br-pageheader -->
    <div class="br-pagetitle">

    </div><!-- d-flex -->
    <!-- Button trigger modal -->
    @if(auth()->guard('admin')->user()->hasPermissionTo('create admin'))
    <div style="padding:15px;float: right">
        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#addAdmin">
            <i class="fa fa-plus"></i>
            {{__('admin.addAdmin')}}
        </button>
    </div>
    @endif
    <div class="br-section-wrapper">
        @include('Admin.layouts.message')
        <div class="bd bd-gray-300 rounded table-responsive">
            <table class="table table-striped mg-b-0">
                <thead>
                <tr>
                    <th>{{__('admin.nameAttribute')}}</th>
                    <th>{{__('admin.emailAttribute')}}</th>
                    <th>{{__('admin.phoneAttribute')}}</th>
                    @if(auth()->guard('admin')->user()->hasPermissionTo('edit admin'))
                        <th>{{__('admin.OperationAttribute')}}</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @foreach($admins as $admin)
                    <tr>
                        <td>{{$admin->name}}</td>
                        <td>{{$admin->email}}</td>
                        <td>{{$admin->phone}}</td>
                        @if(auth()->guard('admin')->user()->hasPermissionTo('edit admin'))
                        <td>
                            <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#editAdmin{{$admin->id}}">
                                <i class="fa fa-edit"></i>
                                {{__('admin.editAdmin')}}
                            </button>

                            <!-- Modal -->
                            <div class="modal fade" id="editAdmin{{$admin->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <form class="modal-content" method="post" action="{{route('admin.admins.update', [$scope, $admin->id])}}">
                                        @csrf
                                        @method('put')
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">{{__('admin.editSchoolGrade')}}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row mg-t-20">
                                                <label class="col-sm-4 form-control-label">{{__('admin.nameAttribute')}}</label>
                                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                                    <input type="text" name="name" class="form-control" placeholder="{{__('admin.nameAttribute')}}" value="{{$admin->name}}" required>
                                                </div>
                                                @include('Admin.layouts.error', ['input' => 'name'])
                                            </div><!-- row -->
                                            <div class="row mg-t-20">
                                                <label class="col-sm-4 form-control-label">{{__('admin.emailAttribute')}}</label>
                                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                                    <input type="email" name="email" class="form-control" placeholder="{{__('admin.emailAttribute')}}" value="{{$admin->email}}" required>
                                                </div>
                                                @include('Admin.layouts.error', ['input' => 'email'])
                                            </div><!-- row -->
                                            <div class="row mg-t-20">
                                                <label class="col-sm-4 form-control-label">{{__('admin.phoneAttribute')}}</label>
                                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                                    <input type="text" name="phone" class="form-control" placeholder="{{__('admin.phoneAttribute')}}" value="{{$admin->phone}}" required>
                                                </div>
                                                @include('Admin.layouts.error', ['input' => 'phone'])
                                            </div><!-- row -->
                                            <div class="row mg-t-20">
                                                <label class="col-sm-4 form-control-label">{{__('admin.adminStatusAttribute')}} <span class="tx-danger">*</span></label>
                                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                                    <select class="form-control class_room_id_1" name="active">
                                                        <option selected disabled> {{__('admin.selectAdminStatus')}} </option>
                                                        <option value="1"> {{__('admin.activeAttribute')}} </option>
                                                        <option value="0"> {{__('admin.notActiveAttribute')}} </option>
                                                    </select>
                                                </div>
                                                @include('Admin.layouts.error', ['input' => 'active'])
                                            </div>
                                            <div class="row mg-t-20">
                                                <label class="col-sm-4 form-control-label">{{__('admin.passwordAttribute')}}</label>
                                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                                    <input type="password" name="password" class="form-control" placeholder="{{__('admin.passwordAttribute')}}">
                                                </div>
                                                @include('Admin.layouts.error', ['input' => 'password'])
                                            </div><!-- row -->
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                                            <button type="submit" class="btn btn-info btn-sm">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div><!-- bd -->
    </div>

    @if(auth()->guard('admin')->user()->hasPermissionTo('create admin'))
    <!-- Modal -->
    <div class="modal fade" id="addAdmin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form class="modal-content" method="post" action="{{route('admin.admins.store', $scope)}}">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('admin.addAdmin')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.nameAttribute')}} <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" name="name" class="form-control" placeholder="{{__('admin.nameAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'name'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.emailAttribute')}} <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="email" name="email" class="form-control" placeholder="{{__('admin.emailAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'email'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.phoneAttribute')}} <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" name="phone" class="form-control" placeholder="{{__('admin.phoneAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'phone'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.passwordAttribute')}} <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="password" name="password" class="form-control" placeholder="{{__('admin.passwordAttribute')}}">
                        </div>
                        @include('Admin.layouts.error', ['input' => 'password'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.passwordAttribute')}} <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            @foreach($data as $key => $permission)
                                <h6 style="color: #1d75b3">{{$key}}</h6>
                                        @foreach($permission as $single_permission)
                                            <div class="col-md-12">
                                                <input type="checkbox" id="check{{$single_permission->id}}" name="check_list[]" value="{{ $single_permission->name }}">
                                                <label class="control-label" style="padding: 4px" for="check{{$single_permission->id}}">{{$single_permission->name}}</label>
                                            </div>
                                        @endforeach
                            @endforeach
                        </div>
                    </div><!-- row -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                    <button type="submit" class="btn btn-info">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                </div>
            </form>
        </div>
    </div>
    @endif
@endsection
