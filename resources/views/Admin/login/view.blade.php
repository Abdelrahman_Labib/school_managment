<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>Sign In Page</title>

    <!-- vendor css -->
    <link href="/admin/lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="/admin/lib/ionicons/css/ionicons.min.css" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="/admin/css/bracket.css">
</head>

<body>

<div class="d-flex align-items-center justify-content-center bg-br-primary ht-100v">

    <form method="post" action="{{route('admin.authenticate', $scope)}}" class="login-wrapper wd-300 wd-xs-350 pd-25 pd-xs-40 bg-white rounded shadow-base">
        @csrf
        <div class="tx-center tx-28 tx-bold tx-inverse"><i class="fa fa-user-circle"></i></div>
        <div class="tx-center mg-b-60">Login to your account</div>
        @if($scope == 'student')
            @include('Admin.layouts.message')
            <div class="form-group">
                <input type="text" name="school_id" class="form-control" placeholder="Enter your school id" required>
                @include('Admin.layouts.error', ['input' => 'school_id'])
            </div><!-- form-group -->
            <div class="form-group">
                <input type="password" name="password" class="form-control" placeholder="Enter your password" required>
                @include('Admin.layouts.error', ['input' => 'password'])
            </div><!-- form-group -->
        @else
            @include('Admin.layouts.message')
            <div class="form-group">
                <input type="text" name="phone" class="form-control" placeholder="Enter your phone" required>
                @include('Admin.layouts.error', ['input' => 'phone'])
            </div><!-- form-group -->
            <div class="form-group">
                <input type="password" name="password" class="form-control" placeholder="Enter your password" required>
                @include('Admin.layouts.error', ['input' => 'password'])
            </div><!-- form-group -->
        @endif
        <button type="submit" class="btn btn-info btn-block">Sign In</button>

    </form><!-- login-wrapper -->
</div><!-- d-flex -->

<script src="/admin/lib/jquery/jquery.min.js"></script>
<script src="/admin/lib/jquery-ui/ui/widgets/datepicker.js"></script>
<script src="/admin/lib/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>
</html>
