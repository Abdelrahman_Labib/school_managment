@extends('Admin.layouts.app')
@section('title') {{__('admin.studentSideBar')}} @endsection
@section('css')
    <link href="/admin/lib/datatables.net-dt/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="/admin/lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/admin/css/datepicker.css">
@endsection
@section('content')

    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <span class="breadcrumb-item active">{{__('admin.studentSideBar')}}</span>
        </nav>
    </div><!-- br-pageheader -->
    <div class="br-pagetitle">

    </div><!-- d-flex -->

    <!-- Button trigger modal -->
    @if(auth()->guard('admin')->user()->hasPermissionTo('create student'))
    <div style="padding:15px;float: right">
        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#addStudent">
            <i class="fa fa-plus"></i>
            {{__('admin.addStudent')}}
        </button>
    </div>
    @endif
    <div class="br-section-wrapper">
        @include('Admin.layouts.message')
        <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>{{__('admin.nameAttribute')}}</th>
                    <th>{{__('admin.parentNameAttribute')}}</th>
                    <th>{{__('admin.parentPhoneAttribute')}}</th>
                    <th>{{__('admin.genderAttribute')}}</th>
                    <th>{{__('admin.IdNumberAttribute')}}</th>
                    <th>{{__('admin.addressAttribute')}}</th>
                    <th>{{__('admin.classRoomSingleWord')}}</th>
                    <th>{{__('admin.sectionSingleWord')}}</th>
                    <th>{{__('admin.totalFees')}}</th>
                    <th>{{__('admin.paidAttribute')}}</th>
                    <th>{{__('admin.remainingAttribute')}}</th>
                    <th>{{__('admin.imageAttribute')}}</th>
                    @if(auth()->guard('admin')->user()->hasPermissionTo('view student') ||
                        auth()->guard('admin')->user()->hasPermissionTo('edit student') ||
                        auth()->guard('admin')->user()->hasPermissionTo('transfer student'))
                    <th>{{__('admin.OperationAttribute')}}</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @foreach($students as $student)
                    <tr>
                        <td>{{$student->name}}</td>
                        <td>{{$student->parent_name}}</td>
                        <td>{{$student->parent_phone}}</td>
                        <td>{{$student->gender}}</td>
                        <td>{{$student->school_number_id}}</td>
                        <td>{{$student->address}}</td>
                        <td>{{$student->class_room_name}}</td>
                        <td>{{$student->section_name}}</td>
                        <td>{{$student->class_room_price}}</td>
                        <td>{{$student->student_payment_paid}}</td>
                        <td>{{$student->student_payment_remain}}</td>
                        <td><img src="{{$student->image}}" style="width: 40px;"></td>
                        @if(auth()->guard('admin')->user()->hasPermissionTo('view student') ||
                            auth()->guard('admin')->user()->hasPermissionTo('edit student') ||
                            auth()->guard('admin')->user()->hasPermissionTo('transfer student'))
                        <td>
                            @if(auth()->guard('admin')->user()->hasPermissionTo('view student'))
                            <a title="{{__('admin.showStudent')}}" href="{{route('admin.students.show', [$scope, $student->id])}}" class="btn btn-info btn-sm pull-right">
                                <i class="fa fa-eye"></i>
                            </a>
                            @endif

                            @if(auth()->guard('admin')->user()->hasPermissionTo('edit student'))
                            <button title="{{__('admin.editStudent')}}" class="btn btn-warning btn-sm btn-condensed" onclick="openModalEditEvent({{json_encode($student)}})">
                                <i class="fa fa-edit"></i>
                            </button>
                            @endif

                            @if(auth()->guard('admin')->user()->hasPermissionTo('transfer student'))
                            <button title="{{__('admin.transferStudent')}}" class="btn btn-primary btn-sm btn-condensed" onclick="openModalTransferStudent({{$student->id}})">
                                <i class="fa fa-exchange-alt"></i>
                            </button>
                            @endif
                        </td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div><!-- bd -->
    </div>

    @if(auth()->guard('admin')->user()->hasPermissionTo('create student'))
    <!-- Modal Add Student-->
    <div class="modal fade" id="addStudent" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form class="modal-content" method="post" action="{{route('admin.students.store', $scope)}}" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('admin.addStudent')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <label class="col-sm-4 form-control-label">{{__('admin.nameAttribute')}} <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" name="name" class="form-control" placeholder="{{__('admin.nameAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'name'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.parentNameAttribute')}} <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" name="parent_name" class="form-control" placeholder="{{__('admin.parentNameAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'parent_name'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.parentPhoneAttribute')}} <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="number" name="parent_phone" class="form-control" placeholder="{{__('admin.parentPhoneAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'parent_phone'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.genderAttribute')}} <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select class="form-control" name="gender">
                                <option value="male"> Male </option>
                                <option value="female"> Female </option>
                            </select>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'gender'])
                    </div>
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.schoolNumberAttribute')}} <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" name="school_number_id" class="form-control" placeholder="{{__('admin.schoolNumberAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'school_number_id'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.addressAttribute')}} <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" name="address" class="form-control" placeholder="{{__('admin.addressAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'address'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.nationalityAttribute')}} <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" name="nationality" class="form-control" placeholder="{{__('admin.nationalityAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'nationality'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.birthdayAttribute')}} <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" name="birthday" data-toggle="datepicker" autocomplete="off" class="form-control" placeholder="{{__('admin.birthdayAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'birthday'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.classRoomSideBar')}} <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select class="form-control class_room_id_1" name="class_room_id" onchange="selectClassRoom(1)">
                                <option selected disabled> {{__('admin.selectClassRoom')}} </option>
                                @foreach($class_rooms as $class_room)
                                    <option value="{{$class_room->id}}">{{$class_room->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'class_room_id'])
                    </div>
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.sectionSideBar')}} <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select class="form-control section_id_1" name="section_id">
                                <option disabled selected>{{__('admin.selectClassRoomMsg')}}</option>
                            </select>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'class_room_id'])
                    </div>
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.imageAttribute')}} <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <div class="d-flex">
                                <input type="file" name="image" id="file-2" class="inputfile">
                                <label for="file-2" class="tx-white bg-info">
                                    <i class="icon ion-ios-upload-outline tx-24"></i>
                                    <span>Choose a file...</span>
                                </label>
                            </div><!-- ht-200 -->
                            @include('Admin.layouts.error', ['input' => 'image'])
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                    <button type="submit" class="btn btn-info btn-sm">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                </div>
            </form>
        </div>
    </div>
    @endif

    @if(auth()->guard('admin')->user()->hasPermissionTo('edit student'))
    <!-- Modal Edit Student -->
    <div class="modal fade" id="editStudent" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form class="modal-content" method="post" id="edit_form" enctype="multipart/form-data">
                @csrf
                @method('patch')
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('admin.addStudent')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <label class="col-sm-4 form-control-label">{{__('admin.nameAttribute')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" id="name" name="name" class="form-control" placeholder="{{__('admin.nameAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'name'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.parentNameAttribute')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" id="parent_name" name="parent_name" class="form-control" placeholder="{{__('admin.parentNameAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'parent_name'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.parentPhoneAttribute')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="number" id="parent_phone" name="parent_phone" class="form-control" placeholder="{{__('admin.parentPhoneAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'parent_phone'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.genderAttribute')}} <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select class="form-control" name="gender">
                                <option value="male"> Male </option>
                                <option value="female"> Female </option>
                            </select>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'gender'])
                    </div>
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.schoolNumberAttribute')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" id="school_number_id" name="school_number_id" class="form-control" placeholder="{{__('admin.schoolNumberAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'school_number_id'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.addressAttribute')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" id="address" name="address" class="form-control" placeholder="{{__('admin.addressAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'address'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.nationalityAttribute')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" id="nationality" name="nationality" class="form-control" placeholder="{{__('admin.nationalityAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'nationality'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.birthdayAttribute')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" id="birthday" name="birthday" data-toggle="datepicker" autocomplete="off" class="form-control" placeholder="{{__('admin.birthdayAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'birthday'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.classRoomSideBar')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select class="form-control class_room_id_2" name="class_room_id" id="class_room" onchange="selectClassRoom(2)">
                                <option selected disabled> {{__('admin.selectClassRoom')}} </option>
                                @foreach($class_rooms as $class_room)
                                    <option value="{{$class_room->id}}">
                                        {{$class_room->name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'class_room_id'])
                    </div>
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.sectionSideBar')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select class="form-control section_id_2" name="section_id" id="section">
                                <option disabled selected>{{__('admin.selectClassRoomMsg')}}</option>
                            </select>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'class_room_id'])
                    </div>
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.imageAttribute')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <div class="d-flex">
                                <input type="file" name="image" id="file-1" class="inputfile">
                                <label for="file-1" class="tx-white bg-info">
                                    <i class="icon ion-ios-upload-outline tx-24"></i>
                                    <span>Choose a file...</span>
                                </label>
                            </div><!-- ht-200 -->
                            @include('Admin.layouts.error', ['input' => 'image'])
                            <div id="student_image"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                    <button type="submit" class="btn btn-info btn-sm">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                </div>
            </form>
        </div>
    </div>
    @endif

    @if(auth()->guard('admin')->user()->hasPermissionTo('transfer student'))
    <!-- Modal Transfer Student -->
    <div class="modal fade" id="transferStudent" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form class="modal-content" method="post" id="transfer_student">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('admin.transferStudent')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.classRoomSideBar')}} <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select class="form-control class_room_id_3" name="transfer_class_room_id" onchange="selectClassRoom(3)">
                                <option selected disabled> {{__('admin.selectClassRoom')}} </option>
                                @foreach($class_rooms as $class_room)
                                    <option value="{{$class_room->id}}">{{$class_room->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'transfer_class_room_id'])
                    </div>
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.sectionSideBar')}} <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select class="form-control section_id_3" name="transfer_section_id">
                                <option disabled selected>{{__('admin.selectClassRoomMsg')}}</option>
                            </select>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'transfer_section_id'])
                    </div>
                    <input type="hidden" name="student_id" id="student_id">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                    <button type="submit" class="btn btn-info btn-sm">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                </div>
            </form>
        </div>
    </div>
    @endif

{{--    {{ $students->links() }}--}}
@endsection

@section('script')
    <script src="/admin/lib/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/admin/lib/datatables.net-dt/js/dataTables.dataTables.min.js"></script>
    <script src="/admin/lib/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="/admin/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js"></script>
    <script src="/admin/js/datepicker.js"></script>
    <script>

        function openModalEditEvent(student) {

            $('#name').val(student.name);
            $('#parent_name').val(student.parent_name);
            $('#parent_phone').val(student.parent_phone);
            $("#school_number_id").val(student.school_number_id)
            $('#address').val(student.address);
            $('#nationality').val(student.nationality);
            $('#birthday').val(student.birthday);
            $('#class_room').val(student.class_room_id);

            $.get( "section/" + student.class_room_id, function( data ) {
                $('.section_id').empty();
                $.each(data, function (i, section_id) {
                    $('#section').append('<option value="' + section_id.id + '">' + section_id.name + '</option>');
                    $('#section').val(section_id.id);
                });
            });
            $('#student_image').prepend('<img src="'+student.image+'" style="width: 70px" />')
            $('#edit_form').attr('action', '/school/admin/students/' + student.id);
            $('#editStudent').modal('show');
        }

        function openModalTransferStudent(student_id) {
            $('#student_id').val(student_id);
            $('#transfer_student').attr('action', '/school/admin/students/transfer');
            $('#transferStudent').modal('show');
        }

        $(function(){
            'use strict';

            $('#datatable1').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });

            // Select2
            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

            $('[data-toggle="datepicker"]').datepicker({
                format: 'yyyy-mm-dd',
                autoHide: true,
                zIndex: 2048,
            });

            $( '.inputfile' ).each( function()
            {
                var $input	 = $( this ),
                    $label	 = $input.next( 'label' ),
                    labelVal = $label.html();

                $input.on( 'change', function( e )
                {
                    var fileName = '';

                    if( this.files && this.files.length > 1 )
                        fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
                    else if( e.target.value )
                        fileName = e.target.value.split( '\\' ).pop();

                    if( fileName )
                        $label.find( 'span' ).html( fileName );
                    else
                        $label.html( labelVal );
                });

                // Firefox bug fix
                $input
                    .on( 'focus', function(){ $input.addClass( 'has-focus' ); })
                    .on( 'blur', function(){ $input.removeClass( 'has-focus' ); });
            });

        });

        function selectClassRoom(id) {
            var parent_id = $('.class_room_id_'+id).val();
            if (parent_id) {
                $.get( "section/" + parent_id, function( data ) {
                    $('.section_id_'+id).empty();
                    $('.section_id_'+id).append('<option selected disabled> Select a Section </option>');
                    $.each(data, function (i, section_id) {
                        $('.section_id_'+id).append('<option value="' + section_id.id + '">' + section_id.name + '</option>');
                    });
                });
            }
        }
    </script>
@endsection


