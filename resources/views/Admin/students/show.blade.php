@extends('Admin.layouts.app')
@section('title') {{__('admin.showStudent')}} @endsection
@section('content')
    <div class="br-profile-page">
        @include('Admin.layouts.message')
        <div class="card shadow-base bd-0 rounded-0 widget-4">
            <div class="card-header ht-75">

            </div><!-- card-header -->
            <div class="card-body">
                <div class="card-profile-img">
                    <img src="{{$student->image}}" alt="">
                </div><!-- card-profile-img -->
                <h4 class="tx-normal tx-roboto tx-white">{{$student->name}}</h4>
                <p class="mg-b-25">{{$student->school_number_id}}</p>

            </div><!-- card-body -->
        </div><!-- card -->

        <div class="ht-70 bg-gray-100 pd-x-20 d-flex align-items-center justify-content-center shadow-base">
            <ul class="nav nav-outline active-info align-items-center flex-row" role="tablist">

                @if(auth()->guard('admin')->user()->hasPermissionTo('view student information'))
                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#information" role="tab">{{__('admin.informationSideBar')}}</a></li>
                @endif

                @if(auth()->guard('admin')->user()->hasPermissionTo('view student agreements'))
                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#agreements" role="tab">{{__('admin.agreementsSideBar')}}</a></li>
                @endif

                @if(auth()->guard('admin')->user()->hasPermissionTo('view student attachments'))
                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#attachment" role="tab">{{__('admin.attachmentAttribute')}}</a></li>
                @endif

            </ul>
        </div>

        <div class="tab-content br-profile-body">
            <div class="tab-pane fade active show" id="information">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="media-list bg-white rounded shadow-base">
                            <div class="media pd-20 pd-xs-30">
                                <div class="media-body mg-l-20">
                                    <table class="table table-striped mg-b-0">
                                        <thead>
                                        <tr>
                                            <th>{{__('admin.academicYearSideBar')}}</th>
                                            <th>{{__('admin.schoolGradesSideBar')}}</th>
                                            <th>{{__('admin.classRoomSingleWord')}}</th>
                                            <th>{{__('admin.sectionSingleWord')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($student->classrooms_sections as $classroom)
                                            <tr>
                                                <td>{{$classroom->class_room->school_grade->academic_year->name}}</td>
                                                <td>{{$classroom->class_room->school_grade->name}}</td>
                                                <td>{{$classroom->class_room->name}}</td>
                                                <td>{{$classroom->section->name}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div><!-- media-body -->
                            </div><!-- media -->
                        </div><!-- card -->

                    </div><!-- col-lg-8 -->
                    <div class="col-lg-4 mg-t-30 mg-lg-t-0">
                        <div class="card pd-20 pd-xs-30 shadow-base bd-0">
                            <h6 class="tx-gray-800 tx-uppercase tx-semibold tx-13 mg-b-25">{{__('admin.contactInformation')}}</h6>

                            <label class="tx-10 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">{{__('admin.parentNameAttribute')}}</label>
                            <p class="tx-info mg-b-25">{{$student->parent_name}}</p>

                            <label class="tx-10 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">{{__('admin.parentPhoneAttribute')}}</label>
                            <p class="tx-inverse mg-b-25">{{$student->parent_phone}}</p>

                            <label class="tx-10 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">{{__('admin.schoolNumberAttribute')}}</label>
                            <p class="tx-inverse mg-b-25">{{$student->school_number_id}} </p>

                            <label class="tx-10 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">{{__('admin.addressAttribute')}}</label>
                            <p class="tx-inverse mg-b-25">{{$student->address}} </p>

                            <label class="tx-10 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">{{__('admin.nationalityAttribute')}}</label>
                            <p class="tx-inverse mg-b-25">{{$student->nationality}} </p>

                            <label class="tx-10 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">{{__('admin.birthdayAttribute')}}</label>
                            <p class="tx-inverse mg-b-25">{{$student->birthday}} </p>

                        </div><!-- card -->

                    </div><!-- col-lg-4 -->
                </div><!-- row -->
            </div><!-- tab-pane -->
            <div class="tab-pane fade" id="agreements">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card pd-20 pd-xs-30 shadow-base bd-0 mg-t-30">
                            <div class="row row-xs">
                                <h6 class="tx-gray-800 tx-uppercase tx-semibold tx-14 mg-b-30">{{__('admin.agreementsSideBar')}}</h6>
                            </div><!-- row -->

                            <div class="row row-xs">
                                    <div class="bd bd-gray-300 rounded table-responsive">
                                        <table class="table table-striped mg-b-0">
                                            <thead>
                                            <tr>
                                                <th>{{__('admin.startDateAttribute')}}</th>
                                                <th>{{__('admin.endDateAttribute')}}</th>
                                                <th>{{__('admin.paymentAttribute')}}</th>
                                                <th>{{__('admin.discountAttribute')}}</th>
                                                <th>{{__('admin.afrDiscountAttribute')}}</th>
                                                <th>{{__('admin.paidAttribute')}}</th>
                                                <th>{{__('admin.remainingAttribute')}}</th>
                                                <th>{{__('admin.OperationAttribute')}}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($student->agreements as $agreement)
                                                <tr>
                                                    <td>{{$agreement->start_date}}</td>
                                                    <td>{{$agreement->end_date}}</td>
                                                    <td>{{$agreement->payment}}</td>
                                                    <td>{{$agreement->discount}}</td>
                                                    <td>{{$agreement->after_discount}}</td>
                                                    <td>{{$student->studentPaymentAgreement($agreement->id)->sum('paid')}}</td>
                                                    @if($student->studentPaymentAgreement($agreement->id)->last())
                                                    <td>
                                                        {{$student->studentPaymentAgreement($agreement->id)->last()['remain']}}
                                                    </td>
                                                    @else
                                                        <td>
                                                            {{$agreement->after_discount}}
                                                        </td>
                                                    @endif
                                                    <td>
                                                        <a target="_blank" href="{{route('admin.agreements.show',[$scope, $agreement->id])}}" class="btn btn-info btn-sm">
                                                            <i class="fa fa-eye"></i>
                                                        </a>
                                                        <button type="button" class="btn btn-dark btn-sm" data-toggle="modal" data-target="#addPayment{{$agreement->id}}" title="{{__('admin.addPayment')}}">
                                                            <i class="fa fa-plus"></i>
                                                        </button>

                                                        <div class="modal fade" id="addPayment{{$agreement->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content" >
                                                                    <form method="post" action="{{route('admin.students.payment', $scope)}}">
                                                                    @csrf
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel">{{__('admin.addPayment')}}</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <div class="row mg-t-20">
                                                                            <label class="col-sm-4 form-control-label">{{__('admin.paymentAttribute')}} <span class="tx-danger">*</span></label>
                                                                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                                                                <input type="text" name="paid" class="form-control" placeholder="{{__('admin.paymentAttribute')}}" required>
                                                                            </div>
                                                                            @include('Admin.layouts.error', ['input' => 'paid'])
                                                                        </div><!-- row -->
                                                                        <input type="hidden" name="student_id" value="{{$student->id}}">
                                                                        <input type="hidden" name="agreement_id" value="{{$agreement->id}}">
                                                                        <div class="row mg-t-20">
                                                                            <label class="col-sm-4 form-control-label">{{__('admin.paidDateAttribute')}} <span class="tx-danger">*</span></label>
                                                                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                                                                <input type="date" name="paid_at" class="form-control" placeholder="{{__('admin.paidDateAttribute')}}" required>
                                                                            </div>
                                                                            @include('Admin.layouts.error', ['input' => 'paid_at'])
                                                                        </div><!-- row -->
                                                                        <div class="row mg-t-20">
                                                                            <label class="col-sm-4 form-control-label">{{__('admin.paidTypeAttribute')}} <span class="tx-danger">*</span></label>
                                                                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                                                                <select class="form-control" id="type_id" name="type_id" required>
                                                                                    <option selected disabled> {{__('admin.selectCategory')}} </option>
                                                                                    @foreach($types as $type)
                                                                                        <option value="{{$type->id}}">{{$type->name_en}}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                            @include('Admin.layouts.error', ['input' => 'type_id'])
                                                                        </div><!-- row -->
                                                                        <div class="row mg-t-20" id="name_payment_type" style="display: none">
                                                                            <label class="col-sm-4 form-control-label">{{__('admin.paidTypeNameAttribute')}} <span class="tx-danger">*</span></label>
                                                                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                                                                <input type="text" name="name_payment_type" class="form-control" placeholder="{{__('admin.paidTypeNameAttribute')}}">
                                                                            </div>
                                                                            @include('Admin.layouts.error', ['input' => 'name_payment_type'])
                                                                        </div><!-- row -->
                                                                        <div class="row mg-t-20">
                                                                            <label class="col-sm-4 form-control-label">{{__('admin.descriptionAttribute')}} <span class="tx-danger">*</span></label>
                                                                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                                                                <textarea rows="3" name="description" class="form-control" placeholder="{{__('admin.descriptionAttribute')}}"></textarea>                                                                            </div>
                                                                            @include('Admin.layouts.error', ['input' => 'description'])
                                                                        </div><!-- row -->
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                                                                        <button type="submit" class="btn btn-info btn-sm">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                                                                    </div>
                                                                </form>

                                                                    <div class="media pd-20 pd-xs-30">
                                                                        <table class="table table-striped mg-b-0">
                                                                            <thead>
                                                                            <tr>
                                                                                <th>#</th>
                                                                                <th>{{__('admin.paidAttribute')}}</th>
                                                                                <th>{{__('admin.remainingAttribute')}}</th>
                                                                                <th>{{__('admin.paidDateAttribute')}}</th>
                                                                                <th>{{__('admin.OperationAttribute')}}</th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            @foreach($student->student_payment as $payment)
                                                                                <tr>
                                                                                    <td>{{$payment->id}}</td>
                                                                                    <td>{{$payment->paid}}</td>
                                                                                    <td>{{$payment->remain}}</td>
                                                                                    <td>{{$payment->paid_at}}</td>
                                                                                    <td>
                                                                                        <a target="_blank" class="btn btn-dark btn-sm" href="{{route('admin.students.payment_receipt', [$scope, $payment->id])}}" title="{{__('admin.receiptExchangeAttr')}}">
                                                                                            <i class="fa fa-print"></i>
                                                                                        </a>
                                                                                    </td>
                                                                                </tr>
                                                                            @endforeach
                                                                            </tbody>
                                                                        </table>
                                                                    </div><!-- media-body -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div><!-- bd -->

                            </div><!-- row -->


                        </div><!-- card -->
                    </div><!-- col-lg-12 -->
                </div><!-- row -->
            </div><!-- tab-pane -->
            <div class="tab-pane fade" id="attachment">
                <div class="row">
                    <div class="offset-md-2 col-lg-8">
                        <div class="card pd-20 pd-xs-30 shadow-base bd-0 mg-t-30">
                            <div class="row row-xs">
                                <div class="col-6 col-sm-4 col-md-6">
                                    <h6 class="tx-gray-800 tx-uppercase tx-semibold tx-14 mg-b-30">{{__('admin.attachmentAttribute')}}</h6>
                                </div>
                                <div class="col-6 col-sm-4 offset-md-4 col-md-2">
                                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#addAttachment">
                                        <i class="fa fa-plus"></i>
                                        {{__('admin.addAttachment')}}
                                    </button>
                                </div>
                            </div><!-- row -->

                            <div class="row row-xs">
                                @foreach($student->attachments as $attachment)
                                <div class="col-6 col-sm-4 col-md-6 row">
                                    <div class="col-md-6">
                                        <p class="tx-inverse mg-b-25">{{$attachment->name}} </p>
                                    </div>
                                    <div class="col-md-6">
                                        <a href="{{$attachment->attachment}}">Preview file</a>
                                    </div>
                                </div>
                                @endforeach
                            </div><!-- row -->


                        </div><!-- card -->
                    </div><!-- col-lg-12 -->
                </div><!-- row -->
            </div><!-- tab-pane -->
        </div><!-- br-pagebody -->

    </div><!-- br-mainpanel -->

    <div class="modal fade" id="addAttachment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form class="modal-content" method="post" action="{{route('admin.students.attachment', $scope)}}" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('admin.addAttachment')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <label class="col-sm-4 form-control-label">{{__('admin.nameAttribute')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" id="name" name="name" class="form-control" placeholder="{{__('admin.nameAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'name'])
                    </div><!-- row -->

                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.attachmentAttribute')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <div class="d-flex">
                                <input type="file" name="attachment" id="file-4" class="inputfile" required>
                                <label for="file-4" class="tx-white bg-info">
                                    <i class="icon ion-ios-upload-outline tx-24"></i>
                                    <span>Choose a file...</span>
                                </label>
                            </div><!-- ht-200 -->
                            @include('Admin.layouts.error', ['input' => 'attachment'])
                        </div>
                        <input type="hidden" name="student_id" value="{{$student->id}}">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                    <button type="submit" class="btn btn-info btn-sm">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('script')
    <script>

        $(function(){

            'use strict';

            $( '.inputfile' ).each( function()
            {
                var $input	 = $( this ),
                    $label	 = $input.next( 'label' ),
                    labelVal = $label.html();

                $input.on( 'change', function( e )
                {
                    var fileName = '';

                    if( this.files && this.files.length > 1 )
                        fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
                    else if( e.target.value )
                        fileName = e.target.value.split( '\\' ).pop();

                    if( fileName )
                        $label.find( 'span' ).html( fileName );
                    else
                        $label.html( labelVal );
                });

                // Firefox bug fix
                $input
                    .on( 'focus', function(){ $input.addClass( 'has-focus' ); })
                    .on( 'blur', function(){ $input.removeClass( 'has-focus' ); });
            });

        });

        $('#type_id').on('change', function () {
            var type_id = $('#type_id option:selected').text();

            if(type_id == 'Bank' || type_id == 'بنك' )
            {
                $('#name_payment_type').css("display", "");
            }
            else if(type_id == 'Check' || type_id == 'شيك' )
            {
                $('#name_payment_type').css("display", "");
            }else{
                $('#name_payment_type').css("display", "none");
            }

        })

    </script>
@endsection
