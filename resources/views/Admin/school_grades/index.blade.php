@extends('Admin.layouts.app')
@section('title') {{__('admin.schoolGradesSideBar')}} @endsection
@section('content')

    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <span class="breadcrumb-item active">{{__('admin.schoolGradesSideBar')}}</span>
        </nav>
    </div><!-- br-pageheader -->
    <div class="br-pagetitle">

    </div><!-- d-flex -->
    <!-- Button trigger modal -->
    @if(auth()->guard('admin')->user()->hasPermissionTo('create school grades'))
    <div style="padding:15px;float: right">
        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#addSchoolGrade">
            <i class="fa fa-plus"></i>
            {{__('admin.addSchoolGrade')}}
        </button>
    </div>
    @endif
    <div class="br-section-wrapper">
        @include('Admin.layouts.message')
        <div class="bd bd-gray-300 rounded table-responsive">
            <table class="table table-striped mg-b-0">
                <thead>
                <tr>
                    <th>{{__('admin.academicYearSideBar')}}</th>
                    <th>{{__('admin.nameAttribute')}}</th>
                    <th>{{__('admin.countStudents')}}</th>
                    @if(auth()->guard('admin')->user()->hasPermissionTo('edit school grades') || auth()->guard('admin')->user()->hasPermissionTo('delete school grades'))
                    <th>{{__('admin.OperationAttribute')}}</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @foreach($school_grades as $school_grade)
                    <tr>
                        <td>{{$school_grade->academic_year}}</td>
                        <td>{{$school_grade->name}}</td>
                        <td>
                            <a href="{{route('admin.students.index', [$scope, 'school_grades' => $school_grade->id])}}">
                                {{$school_grade->student_count}}
                            </a>
                        </td>
                        @if(auth()->guard('admin')->user()->hasPermissionTo('edit school grades') || auth()->guard('admin')->user()->hasPermissionTo('delete school grades'))
                            <td>
                                @if(auth()->guard('admin')->user()->hasPermissionTo('edit school grades'))
                                    <button type="button" class="btn btn-warning btn-sm" onclick="openModalEdit({{json_encode($school_grade)}})" title="{{__('admin.editSchoolGrade')}}">
                                        <i class="fa fa-edit"></i>
                                    </button>
                                @endif

                                @if(auth()->guard('admin')->user()->hasPermissionTo('delete school grades'))
                                    <button type="button" class="btn btn-danger btn-sm" onclick="openModalDelete({{json_encode($school_grade)}})" title="{{__('admin.deleteSchoolGrade')}}">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                @endif
                            </td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div><!-- bd -->
        {{$school_grades->links('Admin.layouts.pagination')}}
    </div>

    @if(auth()->guard('admin')->user()->hasPermissionTo('create school grades'))
    <!-- Modal Add -->
    <div class="modal fade" id="addSchoolGrade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form class="modal-content" method="post" action="{{route('admin.school_grades.store', $scope)}}">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('admin.addSchoolGrade')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <label class="col-sm-4 form-control-label">{{__('admin.academicYearSideBar')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select class="form-control" name="academic_year_id" required>
                                <option selected disabled> {{__('admin.selectAcademicYear')}} </option>
                                @foreach($academic_years as $academic_year)
                                    <option value="{{$academic_year->id}}">{{$academic_year->year}}</option>
                                @endforeach
                            </select>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'academic_year_id'])
                    </div>
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.nameAttribute')}} <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" name="name" class="form-control" placeholder="{{__('admin.nameAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'name'])
                    </div><!-- row -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                    <button type="submit" class="btn btn-info">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                </div>
            </form>
        </div>
    </div>
    @endif

    @if(auth()->guard('admin')->user()->hasPermissionTo('edit school grades'))
        <!-- Modal Edit -->
        <div class="modal fade" id="editSchoolGrade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form class="modal-content" method="post" id="edit_form">
                    @csrf
                    @method('put')
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">{{__('admin.editSchoolGrade')}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <label class="col-sm-4 form-control-label">{{__('admin.academicYearSideBar')}}</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select class="form-control" name="academic_year_id" id="edit_academic_year_id" required>
                                    <option selected disabled> {{__('admin.selectAcademicYear')}} </option>
                                    @foreach($academic_years as $academic_year)
                                        <option value="{{$academic_year->id}}">
                                            {{$academic_year->year}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            @include('Admin.layouts.error', ['input' => 'academic_year_id'])
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">{{__('admin.nameAttribute')}}</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" id="edit_name" name="name" class="form-control" placeholder="{{__('admin.nameAttribute')}}" required>
                            </div>
                            @include('Admin.layouts.error', ['input' => 'name'])
                        </div><!-- row -->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                        <button type="submit" class="btn btn-info btn-sm">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                    </div>
                </form>
            </div>
        </div>
    @endif

    @if(auth()->guard('admin')->user()->hasPermissionTo('delete school grades'))
        <!-- Modal Delete -->
        <div class="modal fade" id="deleteSchoolGrade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form class="modal-content" method="post" id="delete_form">
                    @csrf
                    @method('delete')
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">{{__('admin.deleteSchoolGrade')}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <label class="col-sm-12 form-control-label" style="font-weight: bold">
                                {{__('admin.deleteMessage')}} <label style="color: #bd2130" id="delete_name"></label>
                            </label>
                        </div><!-- row -->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                        <button type="submit" class="btn btn-info btn-sm">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                    </div>
                </form>
            </div>
        </div>
    @endif
@endsection

@section('script')
    <script>
        function openModalEdit(school_grade) {
            $('#edit_academic_year_id').val(school_grade.academic_year_id);
            $('#edit_name').val(school_grade.name);
            $('#edit_form').attr('action', '/school/admin/school_grades/' + school_grade.id);
            $('#editSchoolGrade').modal('show');
        }

        function openModalDelete(school_grade) {
            $('#delete_name').text(school_grade.name);
            $('#delete_form').attr('action', '/school/admin/school_grades/' + school_grade.id);
            $('#deleteSchoolGrade').modal('show');
        }
    </script>
@endsection
