@extends('Admin.layouts.app')
@section('title') {{__('admin.employeeSalarySideBar')}} @endsection
@section('css') <link rel="stylesheet" href="/admin/css/datepicker.css"> @endsection
@section('content')

    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <span class="breadcrumb-item active">{{__('admin.employeeSalarySideBar')}}</span>
        </nav>
    </div><!-- br-pageheader -->
    <div class="br-pagetitle">

    </div><!-- d-flex -->
    <!-- Button trigger modal -->
    <div style="padding:15px;float: right">
        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#addEmployeeSalary">
            <i class="fa fa-plus"></i>
            {{__('admin.addEmployeeSalary')}}
        </button>
    </div>
    <div class="br-section-wrapper">
        @include('Admin.layouts.message')
        <div class="bd bd-gray-300 rounded table-responsive">
            <table class="table table-striped mg-b-0">
                <thead>
                <tr>
                    <th>{{__('admin.employeesSideBar')}}</th>
                    <th>{{__('admin.salaryAttribute')}}</th>
                    <th>{{__('admin.dateAttribute')}}</th>
                    <th>{{__('admin.notesAttribute')}}</th>
                    <th>{{__('admin.OperationAttribute')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($employeeSalaries as $employeeSalary)
                    <tr>
                        <td>{{$employeeSalary->employee->name}}</td>
                        <td>{{$employeeSalary->salary}}</td>
                        <td>{{$employeeSalary->date}}</td>
                        <td>{{$employeeSalary->notes}}</td>
                        <td>
                            <button type="button" class="btn btn-warning btn-sm" onclick="openModalEditEvent({{$employeeSalary}})" title="{{__('admin.editEmployeeSalary')}}">
                                <i class="fa fa-edit"></i>
                            </button>
                            <a target="_blank" class="btn btn-dark btn-sm" href="{{route('admin.employees.salaries.show', [$scope, $employeeSalary->id])}}" title="{{__('admin.receiptExchangeAttr')}}">
                                <i class="fa fa-print"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div><!-- bd -->
        {{$employeeSalaries->links('Admin.layouts.pagination')}}
    </div>

    <!-- Add Modal -->
    <div class="modal fade" id="addEmployeeSalary" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form class="modal-content" method="post" action="{{route('admin.employees.salaries.store', $scope)}}">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('admin.addEmployeeSalary')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <label class="col-sm-4 form-control-label">{{__('admin.employeesSideBar')}} <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select id="addSelect" name="employee_id" style="width: 100%">
                                <option selected disabled>{{__('admin.selectEmployee')}}</option>
                                @foreach($employees as $employee)
                                    <option value="{{$employee->id}}">{{$employee->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'employee_id'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.salaryAttribute')}} <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="number" name="salary" class="form-control" placeholder="{{__('admin.salaryAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'salary'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.dateAttribute')}} <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" name="date" data-toggle="datepicker" value="{{request()->from ?: now()->format('yy-m-d')}}" autocomplete="off" class="form-control" placeholder="{{__('admin.dateAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'date'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.notesAttribute')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <textarea rows="3" name="notes" class="form-control" placeholder="{{__('admin.notesAttribute')}}" ></textarea>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'notes'])
                    </div><!-- row -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                    <button type="submit" class="btn btn-info btn-sm">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                </div>
            </form>
        </div>
    </div>

    <!-- Edit Modal -->
    <div class="modal fade" id="editEmployeeSalary" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form class="modal-content" id="edit_form" method="post">
                @csrf
                @method('put')
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('admin.editEmployeeSalary')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <label class="col-sm-4 form-control-label">{{__('admin.employeesSideBar')}} </label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select id="editSelect" name="employee_id" style="width: 100%">
                                @foreach($employees as $employee)
                                    <option value="{{$employee->id}}">{{$employee->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'employee_id'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.salaryAttribute')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="number" id="salary" name="salary" class="form-control" placeholder="{{__('admin.salaryAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'salary'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.dateAttribute')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" id="date" data-toggle="datepicker" autocomplete="off" name="date" class="form-control" placeholder="{{__('admin.dateAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'date'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.notesAttribute')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <textarea rows="3" id="notes" name="notes" class="form-control" placeholder="{{__('admin.notesAttribute')}}" ></textarea>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'notes'])
                    </div><!-- row -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                    <button type="submit" class="btn btn-info btn-sm">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('script')
    <script src="/admin/js/datepicker.js"></script>

    <script>
        $(function () {
            $("#addSelect").select2({
                dropdownParent: $("#addEmployeeSalary")
            });

            $("#editSelect").select2({
                dropdownParent: $("#editEmployeeSalary")
            });

            $('[data-toggle="datepicker"]').datepicker({
                format: 'yyyy-mm-dd',
                autoHide: true,
                zIndex: 2048,
            });
        });

        function openModalEditEvent(employeeSalary) {

            $('#editSelect').val(employeeSalary.employee_id);
            $('#salary').val(employeeSalary.salary);
            $('#date').val(employeeSalary.date);
            $('#notes').val(employeeSalary.notes);

            $('#edit_form').attr('action', '/school/admin/employees/salaries/' + employeeSalary.id);
            $('#editEmployeeSalary').modal('show');
        }
    </script>
@endsection
