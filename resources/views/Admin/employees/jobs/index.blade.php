@extends('Admin.layouts.app')
@section('title') {{__('admin.employeeJobSideBar')}} @endsection
@section('content')

    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <span class="breadcrumb-item active">{{__('admin.employeeJobSideBar')}}</span>
        </nav>
    </div><!-- br-pageheader -->
    <div class="br-pagetitle">

    </div><!-- d-flex -->
    <!-- Button trigger modal -->
    @if(auth()->guard('admin')->user()->hasPermissionTo('create employee job'))
    <div style="padding:15px;float: right">
        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#addEmployeeJob">
            <i class="fa fa-plus"></i>
            {{__('admin.addEmployeeJob')}}
        </button>
    </div>
    @endif
    <div class="br-section-wrapper">
        @include('Admin.layouts.message')
        <div class="bd bd-gray-300 rounded table-responsive">
            <table class="table table-striped mg-b-0">
                <thead>
                <tr>
                    <th>{{__('admin.nameAttribute')}}</th>
                    @if(auth()->guard('admin')->user()->hasPermissionTo('edit employee job') || auth()->guard('admin')->user()->hasPermissionTo('delete employee job'))
                    <th>{{__('admin.OperationAttribute')}}</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @foreach($employeeJobs as $employeeJob)
                    <tr>
                        <td>{{$employeeJob->name}}</td>
                        @if(auth()->guard('admin')->user()->hasPermissionTo('edit employee job'))
                        <td>
                            <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#editEmployeeJob{{$employeeJob->id}}">
                                <i class="fa fa-edit"></i>
                                {{__('admin.editEmployeeJob')}}
                            </button>

                            <!-- Modal -->
                            <div class="modal fade" id="editEmployeeJob{{$employeeJob->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <form class="modal-content" method="post" action="{{route('admin.employees.jobs.update', [$scope, $employeeJob->id])}}">
                                        @csrf
                                        @method('put')
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">{{__('admin.editEmployeeJob')}}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <label class="col-sm-4 form-control-label">{{__('admin.employeeSectSideBar')}}</label>
                                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                                    <select class="form-control" name="employee_section_id" required>
                                                        <option selected disabled> {{__('admin.selectEmployeeSect')}} </option>
                                                        @foreach($employeeSections as $employeeSection)
                                                            <option value="{{$employeeSection->id}}" @if($employeeSection->id == $employeeJob->employee_section_id) selected @endif>
                                                                {{$employeeSection->name}}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                @include('Admin.layouts.error', ['input' => 'employee_section_id'])
                                            </div>
                                            <div class="row mg-t-20">
                                                <label class="col-sm-4 form-control-label">{{__('admin.nameAttribute')}}</label>
                                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                                    <input type="text" name="name" class="form-control" placeholder="{{__('admin.nameAttribute')}}" value="{{$employeeJob->name}}" required>
                                                </div>
                                                @include('Admin.layouts.error', ['input' => 'name'])
                                            </div><!-- row -->
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                                            <button type="submit" class="btn btn-info btn-sm">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div><!-- bd -->
        {{$employeeJobs->links('Admin.layouts.pagination')}}
    </div>

    @if(auth()->guard('admin')->user()->hasPermissionTo('create employee job'))
    <!-- Modal -->
    <div class="modal fade" id="addEmployeeJob" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form class="modal-content" method="post" action="{{route('admin.employees.jobs.store', $scope)}}">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('admin.addEmployeeJob')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <label class="col-sm-4 form-control-label">{{__('admin.employeeSectSideBar')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select class="form-control" name="employee_section_id" required>
                                <option selected disabled> {{__('admin.selectEmployeeSect')}} </option>
                                @foreach($employeeSections as $employeeSection)
                                    <option value="{{$employeeSection->id}}">
                                        {{$employeeSection->name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'employee_section_id'])
                    </div>
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.nameAttribute')}} <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" name="name" class="form-control" placeholder="{{__('admin.nameAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'name'])
                    </div><!-- row -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                    <button type="submit" class="btn btn-info btn-sm">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                </div>
            </form>
        </div>
    </div>
    @endif

@endsection
