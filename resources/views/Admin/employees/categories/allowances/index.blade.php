@extends('Admin.layouts.app')
@section('title') {{__('admin.categoryAllowSideBar')}} @endsection
@section('content')

    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <span class="breadcrumb-item active">{{__('admin.categoryAllowSideBar')}}</span>
        </nav>
    </div><!-- br-pageheader -->
    <div class="br-pagetitle">

    </div><!-- d-flex -->
    <!-- Button trigger modal -->
    @if(auth()->guard('admin')->user()->hasPermissionTo('create category allowance'))
    <div style="padding:15px;float: right">
        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#addCategoryAllowance">
            <i class="fa fa-plus"></i>
            {{__('admin.addCategoryAllowance')}}
        </button>
    </div>
    @endif
    <div class="br-section-wrapper">
        @include('Admin.layouts.message')
        <div class="bd bd-gray-300 rounded table-responsive">
            <table class="table table-striped mg-b-0">
                <thead>
                <tr>
                    <th>{{__('admin.nameAttribute')}}</th>
                    @if(auth()->guard('admin')->user()->hasPermissionTo('edit category allowance') || auth()->guard('admin')->user()->hasPermissionTo('delete category allowance'))
                    <th>{{__('admin.OperationAttribute')}}</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @foreach($categoryAllowances as $categoryAllowance)
                    <tr>
                        <td>{{$categoryAllowance->name}}</td>
                        @if(auth()->guard('admin')->user()->hasPermissionTo('edit category allowance'))
                        <td>
                            <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#editCategoryAllowance{{$categoryAllowance->id}}">
                                <i class="fa fa-edit"></i>
                                {{__('admin.editCategoryAllowance')}}
                            </button>

                            <!-- Modal -->
                            <div class="modal fade" id="editCategoryAllowance{{$categoryAllowance->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <form class="modal-content" method="post" action="{{route('admin.categories.allowances.update', [$scope, $categoryAllowance->id])}}">
                                        @csrf
                                        @method('put')
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">{{__('admin.editCategoryAllowance')}}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <label class="col-sm-4 form-control-label">{{__('admin.nameAttribute')}}</label>
                                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                                    <input type="text" name="name" class="form-control" placeholder="{{__('admin.nameAttribute')}}" value="{{$categoryAllowance->name}}" required>
                                                </div>
                                                @include('Admin.layouts.error', ['input' => 'name'])
                                            </div><!-- row -->
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                                            <button type="submit" class="btn btn-info btn-sm">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div><!-- bd -->
        {{$categoryAllowances->links('Admin.layouts.pagination')}}
    </div>

    @if(auth()->guard('admin')->user()->hasPermissionTo('create category allowance'))
    <!-- Modal -->
    <div class="modal fade" id="addCategoryAllowance" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form class="modal-content" method="post" action="{{route('admin.categories.allowances.store', $scope)}}">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('admin.addCategoryAllowance')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <label class="col-sm-4 form-control-label">{{__('admin.nameAttribute')}} <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" name="name" class="form-control" placeholder="{{__('admin.nameAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'name'])
                    </div><!-- row -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                    <button type="submit" class="btn btn-info btn-sm">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                </div>
            </form>
        </div>
    </div>
    @endif

@endsection
