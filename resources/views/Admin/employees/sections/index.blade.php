@extends('Admin.layouts.app')
@section('title') {{__('admin.employeeSectSideBar')}} @endsection
@section('content')

    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <span class="breadcrumb-item active">{{__('admin.employeeSectSideBar')}}</span>
        </nav>
    </div><!-- br-pageheader -->
    <div class="br-pagetitle">

    </div><!-- d-flex -->
    <!-- Button trigger modal -->
    @if(auth()->guard('admin')->user()->hasPermissionTo('create employee section'))
    <div style="padding:15px;float: right">
        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#addEmployeeSection">
            <i class="fa fa-plus"></i>
            {{__('admin.addEmployeeSection')}}
        </button>
    </div>
    @endif
    <div class="br-section-wrapper">
        @include('Admin.layouts.message')
        <div class="bd bd-gray-300 rounded table-responsive">
            <table class="table table-striped mg-b-0">
                <thead>
                <tr>
                    <th>{{__('admin.nameAttribute')}}</th>
                    @if(auth()->guard('admin')->user()->hasPermissionTo('edit employee section') || auth()->guard('admin')->user()->hasPermissionTo('delete employee section'))
                    <th>{{__('admin.OperationAttribute')}}</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @foreach($employeeSections as $employeeSection)
                    <tr>
                        <td>{{$employeeSection->name}}</td>
                        @if(auth()->guard('admin')->user()->hasPermissionTo('edit employee section'))
                        <td>
                            <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#editEmployeeSection{{$employeeSection->id}}">
                                <i class="fa fa-edit"></i>
                                {{__('admin.editEmployeeSection')}}
                            </button>

                            <!-- Modal -->
                            <div class="modal fade" id="editEmployeeSection{{$employeeSection->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <form class="modal-content" method="post" action="{{route('admin.employees.sections.update', [$scope, $employeeSection->id])}}">
                                        @csrf
                                        @method('put')
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">{{__('admin.editEmployeeSection')}}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <label class="col-sm-4 form-control-label">{{__('admin.nameAttribute')}}</label>
                                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                                    <input type="text" name="name" class="form-control" placeholder="{{__('admin.nameAttribute')}}" value="{{$employeeSection->name}}" required>
                                                </div>
                                                @include('Admin.layouts.error', ['input' => 'name'])
                                            </div><!-- row -->
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                                            <button type="submit" class="btn btn-info btn-sm">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div><!-- bd -->
        {{$employeeSections->links('Admin.layouts.pagination')}}
    </div>

    @if(auth()->guard('admin')->user()->hasPermissionTo('create employee section'))
    <!-- Modal -->
    <div class="modal fade" id="addEmployeeSection" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form class="modal-content" method="post" action="{{route('admin.employees.sections.store', $scope)}}">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('admin.addEmployeeSection')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <label class="col-sm-4 form-control-label">{{__('admin.nameAttribute')}} <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" name="name" class="form-control" placeholder="{{__('admin.nameAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'name'])
                    </div><!-- row -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                    <button type="submit" class="btn btn-info btn-sm">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                </div>
            </form>
        </div>
    </div>
    @endif

@endsection
