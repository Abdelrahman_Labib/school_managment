@extends('Admin.layouts.app')
@section('title') {{__('admin.employeesSideBar')}} @endsection
@section('css')
    <link href="/admin/lib/datatables.net-dt/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="/admin/lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/admin/css/datepicker.css">
@endsection
@section('content')

    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <span class="breadcrumb-item active">{{__('admin.employeesSideBar')}}</span>
        </nav>
    </div><!-- br-pageheader -->
    <div class="br-pagetitle">

    </div><!-- d-flex -->
    <!-- Button trigger modal -->
    @if(auth()->guard('admin')->user()->hasPermissionTo('create employee'))
        <div style="padding:15px;float: right">
            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#addEmployee">
                <i class="fa fa-plus"></i>
                {{__('admin.addEmployee')}}
            </button>
        </div>
    @endif
    <div class="br-section-wrapper">
        @include('Admin.layouts.message')
        <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap" >
                <thead>
                <tr>
                    <th>{{__('admin.employeeJobSideBar')}}</th>
                    <th>{{__('admin.nameAttribute')}}</th>
                    <th>{{__('admin.phoneAttribute')}}</th>
                    <th>{{__('admin.emailAttribute')}}</th>
                    <th>{{__('admin.addressAttribute')}}</th>
                    <th>{{__('admin.IdNumberAttribute')}}</th>
                    <th>{{__('admin.expiryIdNumAttribute')}}</th>
                    <th>{{__('admin.nationalityAttribute')}}</th>
                    <th>{{__('admin.passportAttribute')}}</th>
                    <th>{{__('admin.expiryPassAttribute')}}</th>
                    <th>{{__('admin.imageAttribute')}}</th>
                    @if(auth()->guard('admin')->user()->hasPermissionTo('edit employee') || auth()->guard('admin')->user()->hasPermissionTo('delete employee'))
                        <th>{{__('admin.OperationAttribute')}}</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @foreach($employees as $employee)
                    <tr>
                        <td>{{$employee->job->name}}</td>
                        <td>{{$employee->name}}</td>
                        <td>{{$employee->phone}}</td>
                        <td>{{$employee->email}}</td>
                        <td>{{$employee->address}}</td>
                        <td>{{$employee->id_number}}</td>
                        <td>{{$employee->expiry_id_number}}</td>
                        <td>{{$employee->nationality}}</td>
                        <td>{{$employee->passport}}</td>
                        <td>{{$employee->expiry_passport}}</td>
                        <td><img src="{{$employee->image}}" style="width: 40px;"></td>
                        @if(auth()->guard('admin')->user()->hasPermissionTo('edit employee'))
                            <td>
                                <button type="button" class="btn btn-warning btn-sm" onclick="openModalEditEvent({{$employee}})" title="{{__('admin.editEmployeeSection')}}">
                                    <i class="fa fa-edit"></i>
                                </button>
{{--                                @if(auth()->guard('admin')->user()->hasPermissionTo('view employee'))--}}
{{--                                    <a title="{{__('admin.showEmployee')}}" href="{{route('admin.employees.show', [$scope, $employee->id])}}" class="btn btn-info btn-sm pull-right">--}}
{{--                                        <i class="fa fa-eye"></i>--}}
{{--                                    </a>--}}
{{--                                @endif--}}


                            </td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div><!-- bd -->
        {{$employees->links('Admin.layouts.pagination')}}
    </div>

    @if(auth()->guard('admin')->user()->hasPermissionTo('create employee'))
        <!-- Add Modal -->
        <div class="modal fade" id="addEmployee" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form class="modal-content" method="post" action="{{route('admin.employees.store', $scope)}}" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">{{__('admin.addEmployee')}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <label class="col-sm-4 form-control-label">{{__('admin.academicYearSideBar')}} <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select class="form-control" name="academic_year_id" required>
                                    <option selected disabled> {{__('admin.selectAcademicYear')}} </option>
                                    @foreach($academicYears as $academicYear)
                                        <option value="{{$academicYear->id}}">
                                            {{$academicYear->year}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            @include('Admin.layouts.error', ['input' => 'academic_year_id'])
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">{{__('admin.employeeJobSideBar')}} <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select class="form-control" name="employee_job_id" required>
                                    <option selected disabled> {{__('admin.selectEmployeeJob')}} </option>
                                    @foreach($employeeJobs as $employeeJob)
                                        <option value="{{$employeeJob->id}}">
                                            {{$employeeJob->name}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            @include('Admin.layouts.error', ['input' => 'employee_job_id'])
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">{{__('admin.nameAttribute')}} <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="name" class="form-control" placeholder="{{__('admin.nameAttribute')}}" required>
                            </div>
                            @include('Admin.layouts.error', ['input' => 'name'])
                        </div><!-- row -->
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">{{__('admin.phoneAttribute')}} <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="phone" class="form-control" placeholder="{{__('admin.phoneAttribute')}}" required>
                            </div>
                            @include('Admin.layouts.error', ['input' => 'phone'])
                        </div><!-- row -->
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">{{__('admin.emailAttribute')}} <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="email" name="email" class="form-control" placeholder="{{__('admin.emailAttribute')}}" required>
                            </div>
                            @include('Admin.layouts.error', ['input' => 'email'])
                        </div><!-- row -->
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">{{__('admin.addressAttribute')}} <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="address" class="form-control" placeholder="{{__('admin.addressAttribute')}}" required>
                            </div>
                            @include('Admin.layouts.error', ['input' => 'address'])
                        </div><!-- row -->
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">{{__('admin.IdNumberAttribute')}} <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="number" name="id_number" class="form-control" placeholder="{{__('admin.IdNumberAttribute')}}" required>
                            </div>
                            @include('Admin.layouts.error', ['input' => 'id_number'])
                        </div><!-- row -->
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">{{__('admin.expiryIdNumAttribute')}} <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="expiry_id_number" data-toggle="datepicker" value="{{request()->from ?: now()->format('yy-m-d')}}" autocomplete="off" class="form-control" placeholder="{{__('admin.expiryIdNumAttribute')}}" required>
                            </div>
                            @include('Admin.layouts.error', ['input' => 'expiry_id_number'])
                        </div><!-- row -->
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">{{__('admin.nationalityAttribute')}} <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="nationality" class="form-control" placeholder="{{__('admin.nationalityAttribute')}}" required>
                            </div>
                            @include('Admin.layouts.error', ['input' => 'nationality'])
                        </div><!-- row -->
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">{{__('admin.passportAttribute')}} <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="passport" class="form-control" placeholder="{{__('admin.passportAttribute')}}" required>
                            </div>
                            @include('Admin.layouts.error', ['input' => 'passport'])
                        </div><!-- row -->
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">{{__('admin.expiryPassAttribute')}} <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="expiry_passport" data-toggle="datepicker" value="{{request()->from ?: now()->format('yy-m-d')}}" autocomplete="off" class="form-control" placeholder="{{__('admin.expiryPassAttribute')}}" required>
                            </div>
                            @include('Admin.layouts.error', ['input' => 'expiry_passport'])
                        </div><!-- row -->
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">{{__('admin.imageAttribute')}} <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <div class="d-flex">
                                    <input type="file" name="image" id="file-2" class="inputfile">
                                    <label for="file-2" class="tx-white bg-info">
                                        <i class="icon ion-ios-upload-outline tx-24"></i>
                                        <span>Choose a file...</span>
                                    </label>
                                </div><!-- ht-200 -->
                                @include('Admin.layouts.error', ['input' => 'image'])
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                        <button type="submit" class="btn btn-info btn-sm">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                    </div>
                </form>
            </div>
        </div>
    @endif

    @if(auth()->guard('admin')->user()->hasPermissionTo('edit employee'))
    <!-- Edit Modal -->
    <div class="modal fade" id="editEmployee" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form class="modal-content" method="post" id="edit_form" enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('admin.editEmployee')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <label class="col-sm-4 form-control-label">{{__('admin.academicYearSideBar')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select class="form-control" id="academic_year_id" name="academic_year_id" required>
                                @foreach($academicYears as $academicYear)
                                    <option value="{{$academicYear->id}}">
                                        {{$academicYear->year}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'academic_year_id'])
                    </div>
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.employeeJobSideBar')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select class="form-control" id="employee_job_id" name="employee_job_id" required>
                                <option selected disabled> {{__('admin.selectEmployeeJob')}} </option>
                                @foreach($employeeJobs as $employeeJob)
                                    <option value="{{$employeeJob->id}}">
                                        {{$employeeJob->name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'employee_job_id'])
                    </div>
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.nameAttribute')}} <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" id="name" name="name" class="form-control" placeholder="{{__('admin.nameAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'name'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.phoneAttribute')}} <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" id="phone" name="phone" class="form-control" placeholder="{{__('admin.phoneAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'phone'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.emailAttribute')}} </label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="email" id="email" name="email" class="form-control" placeholder="{{__('admin.emailAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'email'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.addressAttribute')}} </label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" id="address" name="address" class="form-control" placeholder="{{__('admin.addressAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'address'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.IdNumberAttribute')}} </label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="number" id="id_number" name="id_number" class="form-control" placeholder="{{__('admin.IdNumberAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'id_number'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.expiryIdNumAttribute')}} </label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" id="expiry_id_number" name="expiry_id_number" data-toggle="datepicker" autocomplete="off" class="form-control" placeholder="{{__('admin.expiryIdNumAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'expiry_id_number'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.nationalityAttribute')}} </label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" id="nationality" name="nationality" class="form-control" placeholder="{{__('admin.nationalityAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'nationality'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.passportAttribute')}} </label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" id="passport" name="passport" class="form-control" placeholder="{{__('admin.passportAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'passport'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.expiryPassAttribute')}} </label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" id="expiry_passport" name="expiry_passport" data-toggle="datepicker" autocomplete="off" class="form-control" placeholder="{{__('admin.expiryPassAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'expiry_passport'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.imageAttribute')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <div class="d-flex">
                                <input type="file" name="image" id="file-1" class="inputfile">
                                <label for="file-1" class="tx-white bg-info">
                                    <i class="icon ion-ios-upload-outline tx-24"></i>
                                    <span>Choose a file...</span>
                                </label>
                            </div><!-- ht-200 -->
                            @include('Admin.layouts.error', ['input' => 'image'])
                            <div id="employee_image"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                    <button type="submit" class="btn btn-info btn-sm">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                </div>
            </form>
        </div>
    </div>
    @endif

@endsection

@section('script')
    <script src="/admin/lib/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/admin/lib/datatables.net-dt/js/dataTables.dataTables.min.js"></script>
    <script src="/admin/lib/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="/admin/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js"></script>
    <script src="/admin/js/datepicker.js"></script>
    <script>
        function openModalEditEvent(employee) {

            $('#academic_year_id').val(employee.academic_year_id);
            $('#employee_job_id').val(employee.employee_job_id);
            $('#name').val(employee.name);
            $('#phone').val(employee.phone);
            $('#email').val(employee.email);
            $("#address").val(employee.address)
            $('#id_number').val(employee.id_number);
            $('#expiry_id_number').val(employee.expiry_id_number);
            $('#nationality').val(employee.nationality);
            $('#passport').val(employee.passport);
            $('#expiry_passport').val(employee.expiry_passport)
            $('#employee_image').prepend('<img src="'+employee.image+'" style="width: 70px" />')
            $('#edit_form').attr('action', '/school/admin/employees/' + employee.id);
            $('#editEmployee').modal('show');
        }

        $(function(){
            'use strict';

            $('#datatable1').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });

            // Select2
            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

            $('[data-toggle="datepicker"]').datepicker({
                format: 'yyyy-mm-dd',
                autoHide: true,
                zIndex: 2048,
            });

            $( '.inputfile' ).each( function()
            {
                var $input	 = $( this ),
                    $label	 = $input.next( 'label' ),
                    labelVal = $label.html();

                $input.on( 'change', function( e )
                {
                    var fileName = '';

                    if( this.files && this.files.length > 1 )
                        fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
                    else if( e.target.value )
                        fileName = e.target.value.split( '\\' ).pop();

                    if( fileName )
                        $label.find( 'span' ).html( fileName );
                    else
                        $label.html( labelVal );
                });

                // Firefox bug fix
                $input
                    .on( 'focus', function(){ $input.addClass( 'has-focus' ); })
                    .on( 'blur', function(){ $input.removeClass( 'has-focus' ); });
            });
        });
    </script>
@endsection
