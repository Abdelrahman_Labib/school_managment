@extends('Admin.layouts.app')
@section('title') {{__('admin.employeeAgreeSideBar')}} @endsection
@section('css') <link rel="stylesheet" href="/admin/css/datepicker.css"> @endsection
@section('content')

    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <span class="breadcrumb-item active">{{__('admin.employeeAgreeSideBar')}}</span>
        </nav>
    </div><!-- br-pageheader -->
    <div class="br-pagetitle">

    </div><!-- d-flex -->
    <!-- Button trigger modal -->
    <div style="padding:15px;float: right">
        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#addEmployeeAgreement">
            <i class="fa fa-plus"></i>
            {{__('admin.addEmployeeAgreement')}}
        </button>
    </div>
    <div class="br-section-wrapper">
        @include('Admin.layouts.message')
        <div class="bd bd-gray-300 rounded table-responsive">
            <table class="table table-striped mg-b-0">
                <thead>
                <tr>
                    <th>{{__('admin.employeesSideBar')}}</th>
                    <th>{{__('admin.paymentAttribute')}}</th>
                    <th>{{__('admin.startDateAttribute')}}</th>
                    <th>{{__('admin.endDateAttribute')}}</th>
                    <th>{{__('admin.OperationAttribute')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($employeeAgreements as $employeeAgreement)
                    <tr>
                        <td>{{$employeeAgreement->employee->name}}</td>
                        <td>{{$employeeAgreement->payment}}</td>
                        <td>{{$employeeAgreement->start_date}}</td>
                        <td>{{$employeeAgreement->end_date}}</td>
                        <td>
                            <button type="button" class="btn btn-warning btn-sm" onclick="openModalEditEvent({{$employeeAgreement}})">
                                <i class="fa fa-edit"></i>
                                {{__('admin.editEmployeeSalary')}}
                            </button>

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div><!-- bd -->
        {{$employeeAgreements->links('Admin.layouts.pagination')}}
    </div>

    <!-- Add Modal -->
    <div class="modal fade" id="addEmployeeAgreement" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form class="modal-content" method="post" action="{{route('admin.employees.agreements.store', $scope)}}">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('admin.addEmployeeAgreement')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <label class="col-sm-4 form-control-label">{{__('admin.employeesSideBar')}} <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select id="addSelect" name="employee_id" style="width: 100%" required>
                                <option selected disabled>{{__('admin.selectEmployee')}}</option>
                                @foreach($employees as $employee)
                                    <option value="{{$employee->id}}">{{$employee->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'employee_id'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.salaryAgreedAttribute')}} <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="number" name="payment" class="form-control" placeholder="{{__('admin.salaryAgreedAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'payment'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.startDateAttribute')}} <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" name="start_date" data-toggle="datepicker" value="{{request()->from ?: now()->format('yy-m-d')}}" autocomplete="off" class="form-control" placeholder="{{__('admin.startDateAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'date'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.endDateAttribute')}} <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" name="end_date" data-toggle="datepicker" autocomplete="off" class="form-control" placeholder="{{__('admin.endDateAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'end_date'])
                    </div><!-- row -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                    <button type="submit" class="btn btn-info btn-sm">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                </div>
            </form>
        </div>
    </div>

    <!-- Edit Modal -->
    <div class="modal fade" id="editEmployeeAgreement" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form class="modal-content" id="edit_form" method="post">
                @csrf
                @method('put')
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('admin.editEmployeeAgreement')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <label class="col-sm-4 form-control-label">{{__('admin.employeesSideBar')}} </label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select id="editSelect" name="employee_id" style="width: 100%">
                                @foreach($employees as $employee)
                                    <option value="{{$employee->id}}">{{$employee->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'employee_id'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.salaryAgreedAttribute')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="number" id="payment" name="payment" class="form-control" placeholder="{{__('admin.salaryAgreedAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'payment'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.startDateAttribute')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" id="start_date" name="start_date" data-toggle="datepicker" autocomplete="off" class="form-control" placeholder="{{__('admin.startDateAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'start_date'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 fortextm-control-label">{{__('admin.endDateAttribute')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" id="end_date" name="end_date" data-toggle="datepicker" autocomplete="off" class="form-control" placeholder="{{__('admin.endDateAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'end_date'])
                    </div><!-- row -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                    <button type="submit" class="btn btn-info btn-sm">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('script')
    <script src="/admin/js/datepicker.js"></script>

    <script>
        $(function () {
            $("#addSelect").select2({
                dropdownParent: $("#addEmployeeAgreement")
            });

            $("#editSelect").select2({
                dropdownParent: $("#editEmployeeAgreement")
            });

            $('[data-toggle="datepicker"]').datepicker({
                format: 'yyyy-mm-dd',
                autoHide: true,
                zIndex: 2048,
            });
        });

        function openModalEditEvent(employeeAgreement) {

            $('#editSelect').val(employeeAgreement.employee_id);
            $('#payment').val(employeeAgreement.payment);
            $('#start_date').val(employeeAgreement.start_date);
            $('#end_date').val(employeeAgreement.end_date);

            $('#edit_form').attr('action', '/school/admin/employees/agreements/' + employeeAgreement.id);
            $('#editEmployeeAgreement').modal('show');
        }
    </script>
@endsection
