@extends('Admin.layouts.app')
@section('title') {{__('admin.employeeIncenSideBar')}} @endsection
@section('css') <link rel="stylesheet" href="/admin/css/datepicker.css"> @endsection
@section('content')

    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <span class="breadcrumb-item active">{{__('admin.employeeIncenSideBar')}}</span>
        </nav>
    </div><!-- br-pageheader -->
    <div class="br-pagetitle">

    </div><!-- d-flex -->
    <!-- Button trigger modal -->
    <div style="padding:15px;float: right">
        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#addEmployeeIncentive">
            <i class="fa fa-plus"></i>
            {{__('admin.addEmployeeIncentive')}}
        </button>
    </div>
    <div class="br-section-wrapper">
        @include('Admin.layouts.message')
        <div class="bd bd-gray-300 rounded table-responsive">
            <table class="table table-striped mg-b-0">
                <thead>
                <tr>
                    <th>{{__('admin.categoryIncenSideBar')}}</th>
                    <th>{{__('admin.employeesSideBar')}}</th>
                    <th>{{__('admin.paymentAttribute')}}</th>
                    <th>{{__('admin.dateAttribute')}}</th>
                    <th>{{__('admin.notesAttribute')}}</th>
                    <th>{{__('admin.OperationAttribute')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($employeeIncentives as $employeeIncentive)
                    <tr>
                        <td>{{$employeeIncentive->category->name}}</td>
                        <td>{{$employeeIncentive->employee->name}}</td>
                        <td>{{$employeeIncentive->cost}}</td>
                        <td>{{$employeeIncentive->date}}</td>
                        <td>{{$employeeIncentive->notes}}</td>
                        <td>
                            <button type="button" class="btn btn-warning btn-sm" onclick="openModalEditEvent({{$employeeIncentive}})" title="{{__('admin.editEmployeeIncentive')}}">
                                <i class="fa fa-edit"></i>
                            </button>
                            <a target="_blank" class="btn btn-dark btn-sm" href="{{route('admin.employees.incentives.show', [$scope, $employeeIncentive->id])}}" title="{{__('admin.receiptExchangeAttr')}}">
                                <i class="fa fa-print"></i>
                            </a>

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div><!-- bd -->
        {{$employeeIncentives->links('Admin.layouts.pagination')}}
    </div>

    <!-- Add Modal -->
    <div class="modal fade" id="addEmployeeIncentive" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form class="modal-content" method="post" action="{{route('admin.employees.incentives.store', $scope)}}">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('admin.addEmployeeIncentive')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <label class="col-sm-4 form-control-label">{{__('admin.categoryIncenSideBar')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="category_incentive_id" class="form-control">
                                <option selected disabled>{{__('admin.selectCategory')}}</option>
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'category_incentive_id'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.employeesSideBar')}} <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select id="addSelect" name="employee_id" style="width: 100%">
                                <option selected disabled>{{__('admin.selectEmployee')}}</option>
                                @foreach($employees as $employee)
                                    <option value="{{$employee->id}}">{{$employee->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'employee_id'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.paymentAttribute')}} <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="number" name="cost" class="form-control" placeholder="{{__('admin.paymentAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'cost'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.dateAttribute')}} <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" name="date" data-toggle="datepicker" value="{{request()->from ?: now()->format('yy-m-d')}}" autocomplete="off" class="form-control" placeholder="{{__('admin.dateAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'date'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.notesAttribute')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <textarea rows="3" name="notes" class="form-control" placeholder="{{__('admin.notesAttribute')}}" ></textarea>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'notes'])
                    </div><!-- row -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                    <button type="submit" class="btn btn-info btn-sm">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                </div>
            </form>
        </div>
    </div>

    <!-- Edit Modal -->
    <div class="modal fade" id="editEmployeeIncentive" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form class="modal-content" id="edit_form" method="post">
                @csrf
                @method('put')
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('admin.editEmployeeIncentive')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <label class="col-sm-4 form-control-label">{{__('admin.categoryIncenSideBar')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select id="category_incentive_id" name="category_incentive_id" class="form-control">
                                <option selected disabled>{{__('admin.selectCategory')}}</option>
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'category_incentive_id'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.employeesSideBar')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select id="editSelect" name="employee_id" style="width: 100%">
                                @foreach($employees as $employee)
                                    <option value="{{$employee->id}}">{{$employee->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'employee_id'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.paymentAttribute')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="number" id="cost" name="cost" class="form-control" placeholder="{{__('admin.paymentAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'cost'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.dateAttribute')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" id="date" name="date" data-toggle="datepicker" autocomplete="off" class="form-control" placeholder="{{__('admin.dateAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'date'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.notesAttribute')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <textarea rows="3" id="notes" name="notes" class="form-control" placeholder="{{__('admin.notesAttribute')}}" ></textarea>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'notes'])
                    </div><!-- row -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                    <button type="submit" class="btn btn-info btn-sm">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('script')
    <script src="/admin/js/datepicker.js"></script>

    <script>
        $(function () {
            $("#addSelect").select2({
                dropdownParent: $("#addEmployeeIncentive")
            });

            $("#editSelect").select2({
                dropdownParent: $("#editEmployeeIncentive")
            });

            $('[data-toggle="datepicker"]').datepicker({
                format: 'yyyy-mm-dd',
                autoHide: true,
                zIndex: 2048,
            });
        });

        function openModalEditEvent(employeeIncentive) {

            $('#category_incentive_id').val(employeeIncentive.category_incentive_id);
            $('#editSelect').val(employeeIncentive.employee_id);
            $('#cost').val(employeeIncentive.cost);
            $('#date').val(employeeIncentive.date);
            $('#notes').val(employeeIncentive.notes);

            $('#edit_form').attr('action', '/school/admin/employees/incentives/' + employeeIncentive.id);
            $('#editEmployeeIncentive').modal('show');
        }
    </script>
@endsection
