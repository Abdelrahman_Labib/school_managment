@extends('Admin.layouts.app')
@section('title') {{__('admin.dashboardSideBar')}} @endsection
@section('content')
    @if(auth()->guard('admin')->user()->hasPermissionTo('show dashboard'))
    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <span class="breadcrumb-item active">{{__('admin.dashboardSideBar')}}</span>
        </nav>
    </div><!-- br-pageheader -->
    <div class="br-pagetitle">

    </div><!-- d-flex -->

    <div class="row row-sm">
        <div class="col-sm-6 col-xl-3">
            <div class="bg-info rounded overflow-hidden">
                <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                    <i class="fa fa-users tx-60 lh-0 tx-white op-7"></i>
                    <div class="mg-l-20">
                        <p class="tx-10 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white-8 mg-b-10">Total Students</p>
                        <p class="tx-24 tx-white tx-lato tx-bold mg-b-0 lh-1">{{$students}}</p>
{{--                        <span class="tx-11 tx-roboto tx-white-8">24% higher yesterday</span>--}}
                    </div>
                </div>
                <div id="ch1" class="ht-50 tr-y-1"></div>
            </div>
        </div><!-- col-3 -->
        <div class="col-sm-6 col-xl-3 mg-t-20 mg-sm-t-0">
            <div class="bg-purple rounded overflow-hidden">
                <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                    <i class="fa fa-money-bill-wave tx-60 lh-0 tx-white op-7"></i>
                    <div class="mg-l-20">
                        <p class="tx-10 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white-8 mg-b-10">Payment Received</p>
                        <p class="tx-24 tx-white tx-lato tx-bold mg-b-0 lh-1">{{$payments_received}} OMR</p>
{{--                        <span class="tx-11 tx-roboto tx-white-8">$390,212 before tax</span>--}}
                    </div>
                </div>
                <div id="ch3" class="ht-50 tr-y-1"></div>
            </div>
        </div><!-- col-3 -->
        <div class="col-sm-6 col-xl-3 mg-t-20 mg-xl-t-0">
            <div class="bg-teal rounded overflow-hidden">
                <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                    <i class="fa fa-money-check-alt tx-60 lh-0 tx-white op-7"></i>
                    <div class="mg-l-20">
                        <p class="tx-10 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white-8 mg-b-10">Payment Remain</p>
                        <p class="tx-24 tx-white tx-lato tx-bold mg-b-0 lh-1">{{$payments_remain}} OMR</p>
{{--                        <span class="tx-11 tx-roboto tx-white-8">23% average duration</span>--}}
                    </div>
                </div>
                <div id="ch2" class="ht-50 tr-y-1"></div>
            </div>
        </div><!-- col-3 -->
        <div class="col-sm-6 col-xl-3 mg-t-20 mg-xl-t-0">
            <div class="bg-primary rounded overflow-hidden">
                <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                    <i class="fa fa-wallet tx-60 lh-0 tx-white op-7"></i>
                    <div class="mg-l-20">
                        <p class="tx-10 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white-8 mg-b-10">Total Expenses</p>
                        <p class="tx-24 tx-white tx-lato tx-bold mg-b-0 lh-1">{{$expenses}}</p>
{{--                        <span class="tx-11 tx-roboto tx-white-8">65.45% on average time</span>--}}
                    </div>
                </div>
                <div id="ch4" class="ht-50 tr-y-1"></div>
            </div>
        </div><!-- col-3 -->
    </div><!-- row -->
    @endif
@endsection
