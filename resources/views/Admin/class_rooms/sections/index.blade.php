@extends('Admin.layouts.app')
@section('title') {{__('admin.sectionSideBar')}} @endsection
@section('content')

    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{route('admin.class_rooms.index', $scope)}}">{{__('admin.classRoomSideBar')}}</a>
            <span class="breadcrumb-item active">{{__('admin.sectionSideBar')}}</span>
        </nav>
    </div><!-- br-pageheader -->
    <div class="br-pagetitle">

    </div><!-- d-flex -->
    <!-- Button trigger modal -->
    <div style="padding:15px;float: right">
        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#addSection">
            <i class="fa fa-plus"></i>
            {{__('admin.addSection')}}
        </button>
    </div>
    <div class="br-section-wrapper">
        @include('Admin.layouts.message')
        <div class="bd bd-gray-300 rounded table-responsive">
            <table class="table table-striped mg-b-0">
                <thead>
                <tr>
                    <th>{{__('admin.nameAttribute')}}</th>
                    <th>{{__('admin.OperationAttribute')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($sections as $section)
                    <tr>
                        <td>{{$section->name}}</td>
                        <td>
                            <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#editSection{{$section->id}}">
                                <i class="fa fa-edit"></i>
                                {{__('admin.editSection')}}
                            </button>

                            <!-- Modal -->
                            <div class="modal fade" id="editSection{{$section->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <form class="modal-content" method="post" action="{{route('admin.sections.update', [$scope, $section->id])}}">
                                        @csrf
                                        @method('put')
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">{{__('admin.editSection')}}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <label class="col-sm-4 form-control-label">{{__('admin.nameAttribute')}}</label>
                                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                                    <input type="text" name="name" class="form-control" placeholder="{{__('admin.nameAttribute')}}" value="{{$section->name}}" required>
                                                </div>
                                                @include('Admin.layouts.error', ['input' => 'name'])
                                            </div><!-- row -->
                                            <input type="hidden" name="class_room_id" value="{{$id}}">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                                            <button type="submit" class="btn btn-info btn-sm">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div><!-- bd -->
        {{$sections->links('Admin.layouts.pagination')}}
    </div>

    <!-- Modal -->
    <div class="modal fade" id="addSection" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form class="modal-content" method="post" action="{{route('admin.sections.store', $scope)}}">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('admin.addClassRoom')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <label class="col-sm-4 form-control-label">{{__('admin.nameAttribute')}} <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" name="name" class="form-control" placeholder="{{__('admin.nameAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'name'])
                    </div><!-- row -->
                    <input type="hidden" name="class_room_id" value="{{$id}}">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                    <button type="submit" class="btn btn-info btn-sm">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                </div>
            </form>
        </div>
    </div>
@endsection
