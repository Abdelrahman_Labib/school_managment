@extends('Admin.layouts.app')
@section('title') {{__('admin.classRoomSideBar')}} @endsection
@section('content')

    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <span class="breadcrumb-item active">{{__('admin.classRoomSideBar')}}</span>
        </nav>
    </div><!-- br-pageheader -->
    <div class="br-pagetitle">

    </div><!-- d-flex -->
    <!-- Button trigger modal -->
    @if(auth()->guard('admin')->user()->hasPermissionTo('create classrooms'))
    <div style="padding:15px;float: right">
        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#addClassRoom">
            <i class="fa fa-plus"></i>
            {{__('admin.addClassRoom')}}
        </button>
    </div>
    @endif
    <div class="br-section-wrapper">
        @include('Admin.layouts.message')
        <div class="bd bd-gray-300 rounded table-responsive">
            <table class="table table-striped mg-b-0">
                <thead>
                <tr>
                    <th>{{__('admin.academicYearSideBar')}}</th>
                    <th>{{__('admin.schoolGradesSideBar')}}</th>
                    <th>{{__('admin.nameAttribute')}}</th>
                    <th>{{__('admin.priceAttribute')}}</th>
                    <th>{{__('admin.countStudents')}}</th>
                    @if(auth()->guard('admin')->user()->hasPermissionTo('edit classrooms') || auth()->guard('admin')->user()->hasPermissionTo('show sections'))
                    <th>{{__('admin.OperationAttribute')}}</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @foreach($class_rooms as $class_room)
                    <tr>
                        <td>{{$class_room->school_grade->academic_year->year}}</td>
                        <td>{{$class_room->school_grade->name}}</td>
                        <td>{{$class_room->name}}</td>
                        <td>{{$class_room->price}}</td>
                        <td>
                            <a href="{{route('admin.students.index', [$scope, 'class_room' => $class_room->id])}}">
                                {{$class_room->student_class_rooms->groupBy('student_id')->count()}}
                            </a>
                        </td>
                        @if(auth()->guard('admin')->user()->hasPermissionTo('edit classrooms') || auth()->guard('admin')->user()->hasPermissionTo('show sections'))
                        <td>
                            @if(auth()->guard('admin')->user()->hasPermissionTo('show sections'))
                            <a href="{{route('admin.sections.show', [$scope, $class_room->id])}}" class="btn btn-info btn-sm pull-right">
                                <i class="fa fa-eye"></i>
                                {{__('admin.showSection')}}
                            </a>
                            @endif

                            @if(auth()->guard('admin')->user()->hasPermissionTo('edit classrooms'))
                            <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#editClassRoom{{$class_room->id}}">
                                <i class="fa fa-edit"></i>
                                {{__('admin.editClassRoom')}}
                            </button>

                            <!-- Modal -->
                            <div class="modal fade" id="editClassRoom{{$class_room->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <form class="modal-content" method="post" action="{{route('admin.class_rooms.update', [$scope, $class_room->id])}}">
                                        @csrf
                                        @method('put')
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">{{__('admin.editClassRoom')}}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <label class="col-sm-4 form-control-label">{{__('admin.schoolGradesSideBar')}}</label>
                                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                                    <select class="form-control" name="school_grade_id" required>
                                                        <option selected disabled> {{__('admin.selectSchoolGrade')}} </option>
                                                        @foreach($school_grades as $school_grade)
                                                            <option value="{{$school_grade->id}}" @if($school_grade->id == $class_room->school_grade_id) selected @endif>
                                                                {{$school_grade->name}}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                @include('Admin.layouts.error', ['input' => 'school_grade_id'])
                                            </div>
                                            <div class="row mg-t-20">
                                                <label class="col-sm-4 form-control-label">{{__('admin.nameAttribute')}}</label>
                                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                                    <input type="text" name="name" class="form-control" placeholder="{{__('admin.nameAttribute')}}" value="{{$class_room->name}}" required>
                                                </div>
                                                @include('Admin.layouts.error', ['input' => 'name'])
                                            </div><!-- row -->
                                            <div class="row mg-t-20">
                                                <label class="col-sm-4 form-control-label">{{__('admin.priceAttribute')}}</label>
                                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                                    <input type="number" name="price" class="form-control" placeholder="{{__('admin.priceAttribute')}}" value="{{$class_room->price}}" required>
                                                </div>
                                                @include('Admin.layouts.error', ['input' => 'price'])
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                                            <button type="submit" class="btn btn-info btn-sm">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            @endif
                        </td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div><!-- bd -->
        {{$class_rooms->links('Admin.layouts.pagination')}}
    </div>

    @if(auth()->guard('admin')->user()->hasPermissionTo('create classrooms'))
    <!-- Modal -->
    <div class="modal fade" id="addClassRoom" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form class="modal-content" method="post" action="{{route('admin.class_rooms.store', $scope)}}">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('admin.addClassRoom')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <label class="col-sm-4 form-control-label">{{__('admin.schoolGradesSideBar')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select class="form-control" name="school_grade_id" required>
                                <option selected disabled> {{__('admin.selectSchoolGrade')}} </option>
                                @foreach($school_grades as $school_grade)
                                    <option value="{{$school_grade->id}}">{{$school_grade->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'school_grade_id'])
                    </div>
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.nameAttribute')}} <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" name="name" class="form-control" placeholder="{{__('admin.nameAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'name'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.priceAttribute')}} <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="number" name="price" class="form-control" placeholder="{{__('admin.priceAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'price'])
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                    <button type="submit" class="btn btn-info">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                </div>
            </form>
        </div>
    </div>
    @endif
@endsection
