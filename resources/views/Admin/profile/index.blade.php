<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta property="og:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>
        Profile
    </title>

    <!-- vendor css -->
    <link href="/admin/lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="/admin/lib/ionicons/css/ionicons.min.css" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="/admin/css/bracket.css">
</head>

<body class="collapsed-menu">


<!-- ########## START: HEAD PANEL ########## -->
<div class="br-header">
    <div class="br-header-left">

    </div><!-- br-header-left -->
    <div class="br-header-right">
        <nav class="nav">
            <div class="dropdown">
                <a href="" class="nav-link nav-link-profile" data-toggle="dropdown">
                    <span class="logged-name hidden-md-down">{{Auth::guard('admin')->user()->name}}</span>
                    <img src="https://via.placeholder.com/500" class="wd-32 rounded-circle" alt="">
                    <span class="square-10 bg-success"></span>
                </a>
                <div class="dropdown-menu dropdown-menu-header wd-250">
                    <div class="tx-center">
                        <a href=""><img src="{{Auth::guard('admin')->user()->image != null ? Auth::guard('admin')->user()->image : 'https://via.placeholder.com/500'}}" class="wd-80 rounded-circle" alt=""></a>
                        <h6 class="logged-fullname">{{Auth::guard('admin')->user()->name}}</h6>
                        <p>{{Auth::guard('admin')->user()->email}}</p>
                    </div>
                    <hr>
                    <ul class="list-unstyled user-profile-nav">
                        {{--                        <li><a href=""><i class="icon ion-ios-person"></i> Edit Profile</a></li>--}}
                        {{--                        <li><a href=""><i class="icon ion-ios-gear"></i> Settings</a></li>--}}
                        {{--                        <li><a href=""><i class="icon ion-ios-download"></i> Downloads</a></li>--}}
                        {{--                        <li><a href=""><i class="icon ion-ios-star"></i> Favorites</a></li>--}}
                        {{--                        <li><a href=""><i class="icon ion-ios-folder"></i> Collections</a></li>--}}
                        <li><a href="{{route('admin.logout', $scope)}}"><i class="icon ion-power"></i> Sign Out</a></li>
                    </ul>
                </div><!-- dropdown-menu -->
            </div><!-- dropdown -->
        </nav>
    </div><!-- br-header-right -->
</div><!-- br-header -->
<!-- ########## END: HEAD PANEL ########## -->


<!-- ########## START: MAIN PANEL ########## -->
<div class="br-mainpanel br-profile-page">
    @include('Admin.layouts.message')
    <div class="card shadow-base bd-0 rounded-0 widget-4">
        <div class="card-header ht-75">

        </div><!-- card-header -->
        <div class="card-body">
            <div class="card-profile-img">
                <img src="{{$student->image}}" alt="">
            </div><!-- card-profile-img -->
            <h4 class="tx-normal tx-roboto tx-white">{{$student->name}}</h4>
            <p class="mg-b-25">{{$student->address}}</p>

        </div><!-- card-body -->
    </div><!-- card -->

    <div class="ht-70 bg-gray-100 pd-x-20 d-flex align-items-center justify-content-center shadow-base">
        <ul class="nav nav-outline active-info align-items-center flex-row" role="tablist">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#information" role="tab">{{__('admin.informationSideBar')}}</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#agreements" role="tab">{{__('admin.agreementsSideBar')}}</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#attachment" role="tab">{{__('admin.attachmentAttribute')}}</a></li>
        </ul>
    </div>

    <div class="tab-content br-profile-body">
        <div class="tab-pane fade active show" id="information">
            <div class="row">
                <div class="col-lg-8">
                    <div class="media-list bg-white rounded shadow-base">
                        <div class="media pd-20 pd-xs-30">
                            <div class="media-body mg-l-20">
                                <table class="table table-striped mg-b-0">
                                    <thead>
                                    <tr>
                                        <th>{{__('admin.academicYearSideBar')}}</th>
                                        <th>{{__('admin.classRoomSingleWord')}}</th>
                                        <th>{{__('admin.sectionSingleWord')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($student->classrooms_sections as $classroom)
                                        <tr>
                                            <td>{{$classroom->class_room->academic_year->name}}</td>
                                            <td>{{$classroom->class_room->name}}</td>
                                            <td>{{$classroom->section->name}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div><!-- media-body -->
                        </div><!-- media -->
                    </div><!-- card -->

                </div><!-- col-lg-8 -->
                <div class="col-lg-4 mg-t-30 mg-lg-t-0">
                    <div class="card pd-20 pd-xs-30 shadow-base bd-0">
                        <h6 class="tx-gray-800 tx-uppercase tx-semibold tx-13 mg-b-25">{{__('admin.contactInformation')}}</h6>

                        <label class="tx-10 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">{{__('admin.parentNameAttribute')}}</label>
                        <p class="tx-info mg-b-25">{{$student->parent_name}}</p>

                        <label class="tx-10 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">{{__('admin.parentPhoneAttribute')}}</label>
                        <p class="tx-inverse mg-b-25">{{$student->parent_phone}}</p>

                        <label class="tx-10 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">{{__('admin.schoolNumberAttribute')}}</label>
                        <p class="tx-inverse mg-b-25">{{$student->school_number_id}} </p>

                        <label class="tx-10 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">{{__('admin.addressAttribute')}}</label>
                        <p class="tx-inverse mg-b-50">{{$student->address}} </p>

                    </div><!-- card -->

                </div><!-- col-lg-4 -->
            </div><!-- row -->
        </div><!-- tab-pane -->
        <div class="tab-pane fade" id="agreements">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card pd-20 pd-xs-30 shadow-base bd-0 mg-t-30">
                        <div class="row row-xs">
                            <h6 class="tx-gray-800 tx-uppercase tx-semibold tx-14 mg-b-30">{{__('admin.agreementsSideBar')}}</h6>
                        </div><!-- row -->

                        <div class="row row-xs">
                            <div class="bd bd-gray-300 rounded table-responsive">
                                <table class="table table-striped mg-b-0">
                                    <thead>
                                    <tr>
                                        <th>{{__('admin.startDateAttribute')}}</th>
                                        <th>{{__('admin.endDateAttribute')}}</th>
                                        <th>{{__('admin.paymentAttribute')}}</th>
                                        <th>{{__('admin.discountAttribute')}}</th>
                                        <th>{{__('admin.afrDiscountAttribute')}}</th>
                                        <th>{{__('admin.paidAttribute')}}</th>
                                        <th>{{__('admin.remainingAttribute')}}</th>
                                        <th>{{__('admin.OperationAttribute')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($student->agreements as $agreement)
                                        <tr>
                                            <td>{{$agreement->start_date}}</td>
                                            <td>{{$agreement->end_date}}</td>
                                            <td>{{$agreement->payment}}</td>
                                            <td>{{$agreement->discount}}</td>
                                            <td>{{$agreement->after_discount}}</td>
                                            <td>{{$student->studentPaymentAgreement($agreement->id)->sum('paid')}}</td>
                                            <td>
                                                {{$student->studentPaymentAgreement($agreement->id)->last()['remain'] ?: $agreement->after_discount}}
                                            </td>
                                            <td>
                                                <a target="_blank" href="{{route('admin.agreements.show',[$scope, $agreement->id])}}" class="btn btn-info btn-sm">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                                <button type="button" class="btn btn-dark btn-sm" data-toggle="modal" data-target="#addPayment{{$agreement->id}}" title="Show Payment">
                                                    <i class="fa fa-money-check"></i>
                                                </button>

                                                <div class="modal fade" id="addPayment{{$agreement->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="media-list bg-white rounded shadow-base">
                                                            <div class="media pd-20 pd-xs-30">
                                                                <div class="media-body mg-l-20">
                                                                    <table class="table table-striped mg-b-0">
                                                                        <thead>
                                                                        <tr>
                                                                            <th>#</th>
                                                                            <th>{{__('admin.paidAttribute')}}</th>
                                                                            <th>{{__('admin.remainingAttribute')}}</th>
                                                                            <th>{{__('admin.paidDateAttribute')}}</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        @foreach($student->student_payment as $payment)
                                                                            <tr>
                                                                                <td>{{$payment->id}}</td>
                                                                                <td>{{$payment->paid}}</td>
                                                                                <td>{{$payment->remain}}</td>
                                                                                <td>{{$payment->paid_at}}</td>
                                                                            </tr>
                                                                        @endforeach
                                                                        </tbody>
                                                                    </table>
                                                                </div><!-- media-body -->
                                                            </div><!-- media -->
                                                        </div><!-- card -->

                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div><!-- bd -->

                        </div><!-- row -->


                    </div><!-- card -->
                </div><!-- col-lg-12 -->
            </div><!-- row -->
        </div><!-- tab-pane -->
        <div class="tab-pane fade" id="attachment">
            <div class="row">
                <div class="offset-md-2 col-lg-8">
                    <div class="card pd-20 pd-xs-30 shadow-base bd-0 mg-t-30">
                        <div class="row row-xs">
                            <div class="col-6 col-sm-4 col-md-6">
                                <h6 class="tx-gray-800 tx-uppercase tx-semibold tx-14 mg-b-30">{{__('admin.attachmentAttribute')}}</h6>
                            </div>
                        </div><!-- row -->

                        <div class="row row-xs">
                            @foreach($student->attachments as $attachment)
                                <div class="col-6 col-sm-4 col-md-6 row">
                                    <div class="col-md-6">
                                        <p class="tx-inverse mg-b-25">{{$attachment->name}} </p>
                                    </div>
                                    <div class="col-md-6">
                                        <a href="{{$attachment->attachment}}">Preview file</a>
                                    </div>
                                </div>
                            @endforeach
                        </div><!-- row -->


                    </div><!-- card -->
                </div><!-- col-lg-12 -->
            </div><!-- row -->
        </div><!-- tab-pane -->
    </div><!-- br-pagebody -->

</div><!-- br-mainpanel -->
<!-- ########## END: MAIN PANEL ########## -->

<script src="/admin/lib/jquery/jquery.min.js"></script>
<script src="/admin/lib/jquery-ui/ui/widgets/datepicker.js"></script>
<script src="/admin/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="/admin/lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
<script src="/admin/lib/moment/min/moment.min.js"></script>
<script src="/admin/lib/peity/jquery.peity.min.js"></script>

<script src="/admin/js/bracket.js"></script>
</body>
</html>
