<!DOCTYPE html>
<html lang="en" @if(app()->getLocale() == 'ar') dir="rtl" class="rtl" @endif>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/bracketplus">
    <meta property="og:title" content="Bracket Plus">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>
        @yield('title')
    </title>

    <!-- vendor css -->
    <link href="/admin/lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="/admin/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="/admin/lib/rickshaw/rickshaw.min.css" rel="stylesheet">
    <link href="/admin/lib/select2/css/select2.min.css" rel="stylesheet">

    @yield('css')

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="/admin/css/bracket.css">
    @if(app()->getLocale() == 'ar')
    <style>
        @import url(https://fonts.googleapis.com/earlyaccess/droidarabickufi.css);
        body {
            font-family: 'Droid Arabic Kufi', serif;
        }
    </style>
    @endif
</head>

<body>

<!-- ########## START: LEFT PANEL ########## -->
<div class="br-logo"><a href=""><span>[</span> <i>Admin</i><span>]</span></a></div>
<div class="br-sideleft sideleft-scrollbar">
    <label class="sidebar-label pd-x-10 mg-t-20 op-3">Navigation</label>
    <ul class="br-sideleft-menu">
        @if(auth()->guard('admin')->user()->hasPermissionTo('show dashboard'))
        <li class="br-menu-item">
            <a href="{{route('admin.dashboard', $scope)}}" class="br-menu-link @if(Request::is('school/admin/dashboard')) active @endif">
                <i class="menu-item-icon icon ion-stats-bars tx-24"></i>
                <span class="menu-item-label">{{__('admin.dashboardSideBar')}}</span>
            </a><!-- br-menu-link -->
        </li><!-- br-menu-item -->
        @endif

        @if(auth()->guard('admin')->user()->hasPermissionTo('show school'))
        <li class="br-menu-item">
            <a href="{{route('admin.school_info.index', $scope)}}" class="br-menu-link @if(Request::is('school/admin/school_info*')) active @endif">
                <i class="menu-item-icon icon ion-ios-folder-outline tx-24"></i>
                <span class="menu-item-label">{{__('admin.schoolSideBar')}}</span>
            </a><!-- br-menu-link -->
        </li><!-- br-menu-item -->
        @endif

        @if(auth()->guard('admin')->user()->hasPermissionTo('show academic year'))
        <li class="br-menu-item">
            <a href="{{route('admin.academic_years.index', $scope)}}" class="br-menu-link @if(Request::is('school/admin/academic_year*')) active @endif">
                <i class="menu-item-icon icon ion-calendar tx-24"></i>
                <span class="menu-item-label">{{__('admin.academicYearSideBar')}}</span>
            </a><!-- br-menu-link -->
        </li><!-- br-menu-item -->
        @endif

        @if(auth()->guard('admin')->user()->hasPermissionTo('show school grades'))
        <li class="br-menu-item">
            <a href="{{route('admin.school_grades.index', $scope)}}" class="br-menu-link @if(Request::is('school/admin/school_grades*')) active @endif">
                <i class="menu-item-icon icon ion-document tx-24"></i>
                <span class="menu-item-label">{{__('admin.schoolGradesSideBar')}}</span>
            </a><!-- br-menu-link -->
        </li><!-- br-menu-item -->
        @endif

        @if(auth()->guard('admin')->user()->hasPermissionTo('show classrooms'))
        <li class="br-menu-item">
            <a href="{{route('admin.class_rooms.index', $scope)}}" class="br-menu-link @if(Request::is('school/admin/class_rooms*') || Request::is('school/admin/sections*')) active @endif">
                <i class="menu-item-icon icon ion-ios-home-outline tx-24"></i>
                <span class="menu-item-label">{{__('admin.classRoomSideBar')}}</span>
            </a><!-- br-menu-link -->
        </li><!-- br-menu-item -->
        @endif

        @if(auth()->guard('admin')->user()->hasPermissionTo('show students'))
        <li class="br-menu-item">
            <a href="{{route('admin.students.index', $scope)}}" class="br-menu-link @if(Request::is('school/admin/students*')) active @endif">
                <i class="menu-item-icon icon ion-person-stalker tx-24"></i>
                <span class="menu-item-label">{{__('admin.studentSideBar')}}</span>
            </a><!-- br-menu-link -->
        </li><!-- br-menu-item -->
        @endif

        @if(auth()->guard('admin')->user()->hasPermissionTo('show agreements'))
            <li class="br-menu-item">
                <a href="{{route('admin.agreements.index', $scope)}}" class="br-menu-link @if(Request::is('school/admin/agreements*')) active @endif">
                    <i class="menu-item-icon icon ion-clipboard tx-24"></i>
                    <span class="menu-item-label">{{__('admin.agreementsSideBar')}}</span>
                </a><!-- br-menu-link -->
            </li><!-- br-menu-item -->
        @endif

        @if(auth()->guard('admin')->user()->hasPermissionTo('show category allowances') ||
        auth()->guard('admin')->user()->hasPermissionTo('show category incentives') ||
        auth()->guard('admin')->user()->hasPermissionTo('show category discounts'))
            <li class="br-menu-item">
                <a href="#" class="br-menu-link with-sub @if(Request::is('school/admin/categories*')) active @endif">
                    <i class="menu-item-icon icon ion-navicon-round tx-24"></i>
                    <span class="menu-item-label">{{__('admin.categoryEmploySideBar')}}</span>
                </a><!-- br-menu-link -->
                <ul class="br-menu-sub">
                    @if(auth()->guard('admin')->user()->hasPermissionTo('show category allowances'))
                        <li class="sub-item">
                            <a href="{{route('admin.categories.allowances.index', $scope)}}" class="sub-link  @if(Request::is('school/admin/categories/allowances*')) active @endif">
                                {{__('admin.categoryAllowSideBar')}}
                            </a>
                        </li>
                    @endif
                    @if(auth()->guard('admin')->user()->hasPermissionTo('show category incentives'))
                        <li class="sub-item">
                            <a href="{{route('admin.categories.incentives.index', $scope)}}" class="sub-link  @if(Request::is('school/admin/categories/incentives*')) active @endif">
                                {{__('admin.categoryIncenSideBar')}}
                            </a>
                        </li>
                    @endif
                    @if(auth()->guard('admin')->user()->hasPermissionTo('show category discounts'))
                        <li class="sub-item">
                            <a href="{{route('admin.categories.discounts.index', $scope)}}" class="sub-link  @if(Request::is('school/admin/categories/discounts*')) active @endif">
                                {{__('admin.categoryDiscoSideBar')}}
                            </a>
                        </li>
                    @endif
                </ul>
            </li><!-- br-menu-item -->
        @endif

        @if(auth()->guard('admin')->user()->hasPermissionTo('show employee sections') ||
            auth()->guard('admin')->user()->hasPermissionTo('show employee jobs') ||
            auth()->guard('admin')->user()->hasPermissionTo('show employees') ||
            auth()->guard('admin')->user()->hasPermissionTo('show agreements') ||
            auth()->guard('admin')->user()->hasPermissionTo('show salaries') ||
            auth()->guard('admin')->user()->hasPermissionTo('show allowances') ||
            auth()->guard('admin')->user()->hasPermissionTo('show incentives') ||
            auth()->guard('admin')->user()->hasPermissionTo('show discounts'))
            <li class="br-menu-item">
                <a href="#" class="br-menu-link with-sub @if(Request::is('school/admin/employees*')) active @endif">
                    <i class="menu-item-icon icon ion-person tx-24"></i>
                    <span class="menu-item-label">{{__('admin.employeesSideBar')}}</span>
                </a><!-- br-menu-link -->
                <ul class="br-menu-sub">
                    @if(auth()->guard('admin')->user()->hasPermissionTo('show employee sections'))
                        <li class="sub-item">
                            <a href="{{route('admin.employees.sections.index', $scope)}}" class="sub-link  @if(Request::is('school/admin/employees/sections*')) active @endif">
                                {{__('admin.employeeSectSideBar')}}
                            </a>
                        </li>
                    @endif
                    @if(auth()->guard('admin')->user()->hasPermissionTo('show employee jobs'))
                        <li class="sub-item">
                            <a href="{{route('admin.employees.jobs.index', $scope)}}" class="sub-link  @if(Request::is('school/admin/employees/jobs*')) active @endif">
                                {{__('admin.employeeJobSideBar')}}
                            </a>
                        </li>
                    @endif
                    @if(auth()->guard('admin')->user()->hasPermissionTo('show employees'))
                        <li class="sub-item">
                            <a href="{{route('admin.employees.index', $scope)}}" class="sub-link  @if(Request::is('school/admin/employees')) active @endif">
                                {{__('admin.employeesSideBar')}}
                            </a>
                        </li>
                    @endif
                    @if(auth()->guard('admin')->user()->hasPermissionTo('show employee agreements'))
                        <li class="sub-item">
                            <a href="{{route('admin.employees.agreements.index', $scope)}}" class="sub-link  @if(Request::is('school/admin/employees/agreements')) active @endif">
                                {{__('admin.employeeAgreeSideBar')}}
                            </a>
                        </li>
                    @endif
                    @if(auth()->guard('admin')->user()->hasPermissionTo('show employee salaries'))
                        <li class="sub-item">
                            <a href="{{route('admin.employees.salaries.index', $scope)}}" class="sub-link  @if(Request::is('school/admin/employees/salaries')) active @endif">
                                {{__('admin.employeeSalarySideBar')}}
                            </a>
                        </li>
                    @endif
                    @if(auth()->guard('admin')->user()->hasPermissionTo('show employee allowances'))
                        <li class="sub-item">
                            <a href="{{route('admin.employees.allowances.index', $scope)}}" class="sub-link  @if(Request::is('school/admin/employees/allowances')) active @endif">
                                {{__('admin.employeeAllowSideBar')}}
                            </a>
                        </li>
                    @endif
                    @if(auth()->guard('admin')->user()->hasPermissionTo('show employee incentives'))
                        <li class="sub-item">
                            <a href="{{route('admin.employees.incentives.index', $scope)}}" class="sub-link  @if(Request::is('school/admin/employees/incentives')) active @endif">
                                {{__('admin.employeeIncenSideBar')}}
                            </a>
                        </li>
                    @endif
                    @if(auth()->guard('admin')->user()->hasPermissionTo('show employee discounts'))
                        <li class="sub-item">
                            <a href="{{route('admin.employees.discounts.index', $scope)}}" class="sub-link  @if(Request::is('school/admin/employees/discounts')) active @endif">
                                {{__('admin.employeeDiscoSideBar')}}
                            </a>
                        </li>
                    @endif
                </ul>
            </li><!-- br-menu-item -->
        @endif

        @if(auth()->guard('admin')->user()->hasPermissionTo('show expense category') || auth()->guard('admin')->user()->hasPermissionTo('show expenses'))
        <li class="br-menu-item">
            <a href="#" class="br-menu-link with-sub @if(Request::is('school/admin/category_expenses*') || Request::is('school/admin/expenses*')) active @endif">
                <i class="menu-item-icon icon ion-ios-filing-outline tx-24"></i>
                <span class="menu-item-label">{{__('admin.expensesSideBar')}}</span>
            </a><!-- br-menu-link -->
            <ul class="br-menu-sub">
                @if(auth()->guard('admin')->user()->hasPermissionTo('show expense category'))
                <li class="sub-item">
                    <a href="{{route('admin.category_expenses.index', $scope)}}" class="sub-link  @if(Request::is('school/admin/categories*')) active @endif">
                        {{__('admin.expenseCategory')}}
                    </a>
                </li>
                @endif
                @if(auth()->guard('admin')->user()->hasPermissionTo('show expenses'))
                <li class="sub-item">
                    <a href="{{route('admin.expenses.index', $scope)}}" class="sub-link  @if(Request::is('school/admin/expenses*')) active @endif">
                        {{__('admin.expensesSideBar')}}
                    </a>
                </li>
                @endif
            </ul>
        </li><!-- br-menu-item -->
        @endif

        @if(auth()->guard('admin')->user()->hasPermissionTo('show report payments') || auth()->guard('admin')->user()->hasPermissionTo('show report expenses'))
        <li class="br-menu-item">
            <a href="#" class="br-menu-link with-sub @if(Request::is('school/admin/reports*')) active @endif">
                <i class="menu-item-icon icon ion-document-text tx-24"></i>
                <span class="menu-item-label">{{__('admin.reportsSideBar')}}</span>
            </a><!-- br-menu-link -->
            <ul class="br-menu-sub">
                @if(auth()->guard('admin')->user()->hasPermissionTo('show report payments'))
                <li class="sub-item">
                    <a href="{{route('admin.reports.payments', $scope)}}" class="sub-link  @if(Request::is('school/admin/reports/payments*')) active @endif">
                        {{__('admin.paymentsSideBar')}}
                    </a>
                </li>
                @endif
                @if(auth()->guard('admin')->user()->hasPermissionTo('show report expenses'))
                <li class="sub-item">
                    <a href="{{route('admin.reports.expenses', $scope)}}" class="sub-link  @if(Request::is('school/admin/reports/expenses*')) active @endif">
                        {{__('admin.expensesSideBar')}}
                    </a>
                </li>
                @endif
                @if(auth()->guard('admin')->user()->hasPermissionTo('show report agreements'))
                    <li class="sub-item">
                        <a href="{{route('admin.reports.agreements', $scope)}}" class="sub-link  @if(Request::is('school/admin/reports/agreements*')) active @endif">
                            {{__('admin.agreementsSideBar')}}
                        </a>
                    </li>
                @endif
            </ul>
        </li><!-- br-menu-item -->
        @endif

        @if(auth()->guard('admin')->user()->hasPermissionTo('show admins'))
            <li class="br-menu-item">
                <a href="{{route('admin.admins.index', $scope)}}" class="br-menu-link @if(Request::is('school/admin/admins*')) active @endif">
                    <i class="menu-item-icon icon ion-locked tx-24"></i>
                    <span class="menu-item-label">{{__('admin.adminSideBar')}}</span>
                </a><!-- br-menu-link -->
            </li><!-- br-menu-item -->
        @endif
    </ul><!-- br-sideleft-menu -->

    <br>
</div><!-- br-sideleft -->
<!-- ########## END: LEFT PANEL ########## -->

<!-- ########## START: HEAD PANEL ########## -->
<div class="br-header">
    <div class="br-header-left">
        <div class="navicon-left hidden-md-down"><a id="btnLeftMenu" href=""><i class="icon ion-navicon-round"></i></a></div>
        <div class="navicon-left hidden-lg-up"><a id="btnLeftMenuMobile" href=""><i class="icon ion-navicon-round"></i></a></div>
    </div><!-- br-header-left -->
    <div class="br-header-right">

        @if(app()->getLocale() == 'ar')
        <a href="{{ route('admin.locale.setting', [$scope, 'en']) }}">
            <i class="fa fa-language"></i>
            English
        </a>
        @else
        <a href="{{ route('admin.locale.setting', [$scope, 'ar']) }}">
            <i class="fa fa-language"></i>
            العربية
        </a>
        @endif

        <nav class="nav">
            <div class="dropdown">
                <a href="" class="nav-link nav-link-profile" data-toggle="dropdown">
                    <span class="logged-name hidden-md-down">{{Auth::guard('admin')->user()->name}}</span>
                    <img src="https://via.placeholder.com/500" class="wd-32 rounded-circle" alt="">
                    <span class="square-10 bg-success"></span>
                </a>
                <div class="dropdown-menu dropdown-menu-header wd-250">
                    <div class="tx-center">
                        <a href=""><img src="{{Auth::guard('admin')->user()->image != null ? Auth::guard('admin')->user()->image : 'https://via.placeholder.com/500'}}" class="wd-80 rounded-circle" alt=""></a>
                        <h6 class="logged-fullname">{{Auth::guard('admin')->user()->name}}</h6>
                        <p>{{Auth::guard('admin')->user()->email}}</p>
                    </div>
                    <hr>
                    <ul class="list-unstyled user-profile-nav">
{{--                        <li><a href=""><i class="icon ion-ios-person"></i> Edit Profile</a></li>--}}
{{--                        <li><a href=""><i class="icon ion-ios-gear"></i> Settings</a></li>--}}
{{--                        <li><a href=""><i class="icon ion-ios-download"></i> Downloads</a></li>--}}
{{--                        <li><a href=""><i class="icon ion-ios-star"></i> Favorites</a></li>--}}
{{--                        <li><a href=""><i class="icon ion-ios-folder"></i> Collections</a></li>--}}
                        <li><a href="{{route('admin.logout', $scope)}}"><i class="icon ion-power"></i> Sign Out</a></li>
                    </ul>
                </div><!-- dropdown-menu -->
            </div><!-- dropdown -->
        </nav>
    </div><!-- br-header-right -->
</div><!-- br-header -->
<!-- ########## END: HEAD PANEL ########## -->

<!-- ########## START: MAIN PANEL ########## -->
<div class="br-mainpanel">

    <div class="br-pagebody">
        @yield('content')
    </div><!-- br-pagebody -->
</div><!-- br-mainpanel -->
<!-- ########## END: MAIN PANEL ########## -->

<script src="/admin/lib/jquery/jquery.min.js"></script>
<script src="/admin/lib/jquery-ui/ui/widgets/datepicker.js"></script>
<script src="/admin/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="/admin/lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
<script src="/admin/lib/moment/min/moment.min.js"></script>
<script src="/admin/lib/rickshaw/vendor/d3.min.js"></script>
<script src="/admin/lib/rickshaw/vendor/d3.layout.min.js"></script>
<script src="/admin/lib/rickshaw/rickshaw.min.js"></script>
<script src="/admin/lib/jquery.flot/jquery.flot.js"></script>
<script src="/admin/lib/select2/js/select2.full.min.js"></script>

<script src="/admin/js/bracket.js"></script>
<script src="/admin/js/ResizeSensor.js"></script>
<script src="/admin/js/dashboard.js"></script>

@yield('script')
</body>
</html>
