<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title> RECEIPT - ســـــنــد قــبـــــــض </title>
    <link rel="stylesheet" href="/admin/css/receipt.css" media="all" />
    <style type="text/css">

        @import url({{asset('/admin/css/font-droid/droidarabickufi.css')}});
        @media print
        {
            .no-print, .no-print *
            {
                display: none !important;
            }
        }
    </style>
</head>





<body dir="rtl">

<header class="clearfix">
    <img src="{{$school->header_invoice_image}}" width="793" height="125" alt="" style="border-radius:10px;"/>
</header>



<! end-------------------------- -->

<div id="titel" >
    <h2>ســـــنــد صــــــرف - RECEIPT EXCHANGE </h2>
</div>


<! end titel-------------------------- -->

<div id="no"> NO. {{$receipt->id}} </div>


<main>
    <div>
        <div id="right">
            التاريخ : <input type="text" style="width: 200px" value="{{$receipt->date}}" />
        </div>

        <div id="left">
            <table width="200" border="1">
                <tbody>
                <tr>
                    <!-- <th scope="col">000</th>
 <th scope="col">100</th> -->
                    <th scope="col">{{$receipt->cost}}</th>
                </tr>
                </tbody>
            </table>
        </div>

    </div>
    <div class="clearfix"> </div>

    <div id="content" >
        <p>
            صرفنا إلي الفاضل/الفاضلة :
            <input type="text" style="width: 350px" value="{{$receipt->name}}" />
            :Exchange to Mr/Ms
            <br>

            مبلغ وقدره ريال عماني :
            <input type="text" style="width: 420px" value="{{$receipt->cost}}" />
            The Sum of Rials Omani
            <br>
            نقداً / إيداع / شيك :
            <input type="text" style="width: 170px" value="{{$receipt->type ?: 'نقداً'}}" />
            By Cash/Bank.
            على بنك :
            <input type="text" style="width: 225px" value="{{$receipt->payment_type ?: ''}}" />
            Bank

            <br>
            وذلك عن :
            <input type="text" style="width: 610px"  value="{{$receipt->notes}}" />
            Being
        </p>

    </div>


    <p align="center">  التوقيع : ............................................ Signature
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

        <img src="{{$school->stamp}}" style="width: 100px; height:104px; position: absolute; bottom: 40px; left: 210px;" >
        الختم  : .................................................... Receiver's Signature
    </p>





</main>
<! end main  -------------------------- -->

<script>
    // $(document).ready(function(e){
    //     $('#print').on('click',function(e){
    //         window.print();
    //     });
    //     $("#pdf").on('click',function(e){
    //         var invoice = $('#invoice').val();
    //         window.location.href='https://dev.mazoonapps.com/resystem/sales/receipt_download/'+'/'+invoice;
    //     });
    //     $('#email').on('click',function(e){
    //         var invoice = $('#invoice').val();
    //         $.ajax({
    //             url:'https://dev.mazoonapps.com/resystem/sales/receipt_email/',
    //             type:'POST',
    //             data:{invoice:invoice},
    //             success:function(msg)
    //             {
    //                 if(msg==1)
    //                 {
    //                     alert('email sent');
    //                 }
    //                 else
    //                 {
    //                     alert('Error Sending email, Try again');
    //                 }
    //             }
    //         });
    //     })
    // });
</script>



</body>
</html>
