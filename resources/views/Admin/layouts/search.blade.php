<div class="br-section-wrapper" style="padding: 20px; margin-top: 10px">
    <form class="row" method="get" action="{{$route}}">
        @csrf
        <div class="col-md-4">
            <label>{{__('admin.fromAttribute')}}</label>
            <input type="text" name="from" data-toggle="datepicker" class="form-control" value="{{request()->from ?: now()->format('yy-m-d')}}" autocomplete="off" placeholder="{{__('admin.fromAttribute')}}">
        </div>
        <div class="col-md-4">
            <label>{{__('admin.toAttribute')}}</label>
            <input type="text" name="to" data-toggle="datepicker" class="form-control" value="{{request()->to}}" autocomplete="off" placeholder="{{__('admin.toAttribute')}}">
        </div>
        <div class="col-md-4">
            <label style="padding-top: 15px"></label>
            <button type="submit" class="btn btn-primary btn-block"> {{__('admin.searchButton')}} </button>
        </div>

    </form>
</div>
