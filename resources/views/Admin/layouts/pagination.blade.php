@if ($paginator->hasPages())
    <div class="ht-80 d-flex align-items-center justify-content-center mg-t-20">
        <ul class="pagination pagination-circle mg-b-0">
            @if ($paginator->onFirstPage())
                <li class="page-item disabled">
                    <a class="page-link" href="#" aria-label="Previous"><i class="fa fa-angle-left"></i></a>
                </li>
            @else
                <li class="page-item">
                    <a class="page-link" href="{{ $paginator->previousPageUrl() }}" aria-label="Previous"><i class="fa fa-angle-left"></i></a>
                </li>
            @endif

            @foreach ($elements as $element)
                @if (is_string($element))
                    <li class="page-item disabled"><a class="page-link" href="#">{{ $element }}</a></li>
                @endif

                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="page-item active"><a class="page-link" href="#">{{ $page }}</a></li>
                        @else
                            <li class="page-item"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            @if ($paginator->hasMorePages())
                <li class="page-item">
                    <a class="page-link" href="{{ $paginator->nextPageUrl() }}" aria-label="Next"><i class="fa fa-angle-right"></i></a>
                </li>
            @else
                <li class="page-item disabled">
                    <a class="page-link" href="#" aria-label="Next"><i class="fa fa-angle-right"></i></a>
                </li>
            @endif
        </ul>
    </div>
@endif

{{--@if ($paginator->hasPages())--}}
{{--    <ul class="pager">--}}

{{--        @if ($paginator->onFirstPage())--}}
{{--            <li class="disabled"><span>← Previous</span></li>--}}
{{--        @else--}}
{{--            <li><a href="{{ $paginator->previousPageUrl() }}" rel="prev">← Previous</a></li>--}}
{{--        @endif--}}



{{--        @foreach ($elements as $element)--}}

{{--            @if (is_string($element))--}}
{{--                <li class="disabled"><span>{{ $element }}</span></li>--}}
{{--            @endif--}}



{{--            @if (is_array($element))--}}
{{--                @foreach ($element as $page => $url)--}}
{{--                    @if ($page == $paginator->currentPage())--}}
{{--                        <li class="active my-active"><span>{{ $page }}</span></li>--}}
{{--                    @else--}}
{{--                        <li><a href="{{ $url }}">{{ $page }}</a></li>--}}
{{--                    @endif--}}
{{--                @endforeach--}}
{{--            @endif--}}
{{--        @endforeach--}}



{{--        @if ($paginator->hasMorePages())--}}
{{--            <li><a href="{{ $paginator->nextPageUrl() }}" rel="next">Next →</a></li>--}}
{{--        @else--}}
{{--            <li class="disabled"><span>Next →</span></li>--}}
{{--        @endif--}}
{{--    </ul>--}}
{{--@endif--}}
