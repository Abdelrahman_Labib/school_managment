@extends('Admin.layouts.app')
@section('title') {{__('admin.editSchoolInfo')}} @endsection
@section('content')

    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{route('admin.school_info.index', $scope)}}">{{__('admin.schoolSideBar')}}</a>
            <span class="breadcrumb-item active">{{__('admin.editSchoolInfo')}}</span>
        </nav>
    </div><!-- br-pageheader -->
    <div class="br-pagetitle">

    </div><!-- d-flex -->

    <div class="br-section-wrapper">
        @include('Admin.layouts.message')
        <form method="post" action="{{route('admin.school_info.update', [$scope, $school->id])}}" enctype="multipart/form-data">
            @csrf
            @method('put')
            <div class="row mg-b-25">
                <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label">{{__('admin.nameAttribute')}}:</label>
                        <input class="form-control" type="text" name="name" value="{{$school->name}}" placeholder="{{__('admin.nameAttribute')}}" required>
                        @include('Admin.layouts.error', ['input' => 'name'])
                    </div>
                </div><!-- col-4 -->
                <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label">{{__('admin.emailAttribute')}}:</label>
                        <input class="form-control" type="email" name="email" value="{{$school->email}}" placeholder="{{__('admin.emailAttribute')}}" required>
                        @include('Admin.layouts.error', ['input' => 'email'])
                    </div>
                </div><!-- col-4 -->
                <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label">{{__('admin.phoneAttribute')}}:</label>
                        <input class="form-control" type="text" name="phone" value="{{$school->phone}}" placeholder="{{__('admin.phoneAttribute')}}" required>
                        @include('Admin.layouts.error', ['input' => 'phone'])
                    </div>
                </div><!-- col-4 -->
                <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label">{{__('admin.addressAttribute')}}:</label>
                        <input class="form-control" type="text" name="address" value="{{$school->address}}" placeholder="{{__('admin.addressAttribute')}}" required>
                        @include('Admin.layouts.error', ['input' => 'address'])
                    </div>
                </div><!-- col-4 -->
                <div class="col-lg-4">
                    <div class="row">
                        <div class="form-group col-lg-6">
                            <label class="form-control-label">{{__('admin.logoAttribute')}}:</label>
                            <div class="d-flex">
                                <input type="file" name="logo" id="file-1" class="inputfile">
                                <label for="file-1" class="tx-white bg-info">
                                    <i class="icon ion-ios-upload-outline tx-24"></i>
                                    <span>Choose a file...</span>
                                </label>
                            </div><!-- ht-200 -->
                            @include('Admin.layouts.error', ['input' => 'logo'])
                        </div>
                        <div class="col-lg-6">
                            <img src="{{$school->logo}}" style="width: 100px">
                        </div>
                    </div>
                </div><!-- col-4 -->

                <div class="col-lg-4">
                    <div class="row">
                        <div class="form-group col-lg-6">
                            <label class="form-control-label">{{__('admin.headInvoiceAttribute')}}:</label>
                            <div class="d-flex">
                                <input type="file" name="header_invoice_image" id="file-2" class="inputfile">
                                <label for="file-2" class="tx-white bg-info">
                                    <i class="icon ion-ios-upload-outline tx-24"></i>
                                    <span>Choose a file...</span>
                                </label>
                            </div><!-- ht-200 -->
                            @include('Admin.layouts.error', ['input' => 'header_invoice_image'])
                        </div>
                        <div class="col-lg-6">
                            <img src="{{$school->header_invoice_image}}" style="width: 100px">
                        </div>
                    </div>
                </div><!-- col-4 -->

                <div class="col-lg-4">
                    <div class="row">
                        <div class="form-group col-lg-6">
                            <label class="form-control-label">{{__('admin.stampAttribute')}}:</label>
                            <div class="d-flex">
                                <input type="file" name="stamp" id="file-3" class="inputfile">
                                <label for="file-3" class="tx-white bg-info">
                                    <i class="icon ion-ios-upload-outline tx-24"></i>
                                    <span>Choose a file...</span>
                                </label>
                            </div><!-- ht-200 -->
                            @include('Admin.layouts.error', ['input' => 'stamp'])
                        </div>
                        <div class="col-lg-6">
                            <img src="{{$school->stamp}}" style="width: 100px">
                        </div>
                    </div>
                </div><!-- col-4 -->
            </div><!-- row -->

            <div class="modal-footer" style="margin-top: 15px">
                <button type="submit" class="btn btn-info">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
            </div>
        </form>
    </div>

@endsection

@section('script')
    <script>
        $(function(){

            'use strict';

            $( '.inputfile' ).each( function()
            {
                var $input	 = $( this ),
                    $label	 = $input.next( 'label' ),
                    labelVal = $label.html();

                $input.on( 'change', function( e )
                {
                    var fileName = '';

                    if( this.files && this.files.length > 1 )
                        fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
                    else if( e.target.value )
                        fileName = e.target.value.split( '\\' ).pop();

                    if( fileName )
                        $label.find( 'span' ).html( fileName );
                    else
                        $label.html( labelVal );
                });

                // Firefox bug fix
                $input
                    .on( 'focus', function(){ $input.addClass( 'has-focus' ); })
                    .on( 'blur', function(){ $input.removeClass( 'has-focus' ); });
            });

        });
    </script>
@endsection
