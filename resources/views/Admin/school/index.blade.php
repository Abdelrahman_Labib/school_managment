@extends('Admin.layouts.app')
@section('title') {{__('admin.schoolSideBar')}} @endsection
@section('content')

    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <span class="breadcrumb-item active">{{__('admin.schoolSideBar')}}</span>
        </nav>
    </div><!-- br-pageheader -->
    <div class="br-pagetitle">

    </div><!-- d-flex -->

    <!-- Button trigger modal -->
    @if(auth()->guard('admin')->user()->hasPermissionTo('create school'))
        <div style="padding:15px;float: right">
            @if(!$school)
            <a href="{{route('admin.school_info.create', $scope)}}" class="btn btn-primary btn-sm pull-right">
                <i class="fa fa-plus"></i>
                {{__('admin.addSchoolInfo')}}
            </a>
            @endif
        </div>
    @endif
    <div class="br-section-wrapper">
        @include('Admin.layouts.message')
        <div class="bd bd-gray-300 rounded table-responsive">
            <table class="table table-striped mg-b-0">
                <thead>
                <tr>
                    <th>{{__('admin.nameAttribute')}}</th>
                    <th>{{__('admin.emailAttribute')}}</th>
                    <th>{{__('admin.phoneAttribute')}}</th>
                    <th>{{__('admin.addressAttribute')}}</th>
                    <th>{{__('admin.logoAttribute')}}</th>
                    @if(auth()->guard('admin')->user()->hasPermissionTo('edit school'))
                    <th>{{__('admin.OperationAttribute')}}</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @if($school)
                <tr>
                    <td>{{$school->name}}</td>
                    <td>{{$school->email}}</td>
                    <td>{{$school->phone}}</td>
                    <td>{{$school->address}}</td>
                    <td><img src="{{$school->logo}}" style="width: 100px"></td>
                    @if(auth()->guard('admin')->user()->hasPermissionTo('edit school'))
                    <td>
                        <a href="{{route('admin.school_info.edit', [$scope, $school->id])}}" class="btn btn-warning btn-sm pull-right">
                            <i class="fa fa-edit"></i>
                            {{__('admin.editSchoolInfo')}}
                        </a>
                    </td>
                    @endif
                </tr>
                @endif
                </tbody>
            </table>
        </div><!-- bd -->
    </div>

@endsection
