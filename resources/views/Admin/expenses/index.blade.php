@extends('Admin.layouts.app')
@section('title') {{__('admin.sectionSideBar')}} @endsection
@section('css')
    <link href="/admin/lib/datatables.net-dt/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="/admin/lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/admin/css/datepicker.css">
@endsection
@section('content')

    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{route('admin.category_expenses.index', $scope)}}">{{__('admin.categoriesSideBar')}}</a>
            <span class="breadcrumb-item active">{{__('admin.expensesSideBar')}}</span>
        </nav>
    </div><!-- br-pageheader -->
    <div class="br-pagetitle">

    </div><!-- d-flex -->
    <!-- Button trigger modal -->
    @if(auth()->guard('admin')->user()->hasPermissionTo('create expenses'))
    <div style="padding:15px;float: right">
        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#addExpense">
            <i class="fa fa-plus"></i>
            {{__('admin.addExpense')}}
        </button>
    </div>
    @endif
    <div class="br-section-wrapper">
    @include('Admin.layouts.message')
        <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
                <thead>
                <tr>
                    <th class="wd-5p">#</th>
                    <th class="wd-5p">{{__('admin.categoriesSideBar')}}</th>
                    <th class="wd-5p">{{__('admin.nameAttribute')}}</th>
                    <th class="wd-5p">{{__('admin.dateAttribute')}}</th>
                    <th class="wd-5p">{{__('admin.expenseAttribute')}}</th>
                    <th class="wd-10p">{{__('admin.notesAttribute')}}</th>
                    @if(auth()->guard('admin')->user()->hasPermissionTo('edit expenses'))
                    <th class="wd-10p">{{__('admin.OperationAttribute')}}</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @foreach($expenses as $expense)
                    <tr>
                        <td>{{$expense->id}}</td>
                        <td>{{$expense->category->name}}</td>
                        <td>{{$expense->name}}</td>
                        <td>{{$expense->date}}</td>
                        <td>{{$expense->expense}} </td>
                        <td>{{$expense->notes}}</td>
                        @if(auth()->guard('admin')->user()->hasPermissionTo('edit expenses'))
                        <td>
                            <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#editExpense{{$expense->id}}" title="{{__('admin.editExpense')}}">
                                <i class="fa fa-edit"></i>
                            </button>
                            <a target="_blank" class="btn btn-dark btn-sm" href="{{route('admin.expenses.show', [$scope, $expense->id])}}" title="{{__('admin.receiptExchangeAttr')}}">
                                <i class="fa fa-print"></i>
                            </a>

                            <!-- Modal -->
                            <div class="modal fade" id="editExpense{{$expense->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <form class="modal-content" method="post" action="{{route('admin.expenses.update', [$scope, $expense->id])}}">
                                        @csrf
                                        @method('put')
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">{{__('admin.editExpense')}}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <label class="col-sm-4 form-control-label">{{__('admin.categoriesSideBar')}}</label>
                                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                                    <select class="form-control" name="category_id" required>
                                                        <option selected disabled> {{__('admin.selectCategory')}} </option>
                                                        @foreach($categories as $category)
                                                            <option value="{{$category->id}}" @if($category->id == $expense->category_id) selected @endif>
                                                                {{$category->name}}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                @include('Admin.layouts.error', ['input' => 'category_id'])
                                            </div>
                                            <div class="row mg-t-20">
                                                <label class="col-sm-4 form-control-label">{{__('admin.nameAttribute')}}</label>
                                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                                    <input type="text" name="name" class="form-control" placeholder="{{__('admin.nameAttribute')}}" value="{{$expense->name}}" required>
                                                </div>
                                                @include('Admin.layouts.error', ['input' => 'name'])
                                            </div><!-- row -->
                                            <div class="row mg-t-20">
                                                <label class="col-sm-4 form-control-label">{{__('admin.expenseAttribute')}}</label>
                                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                                    <input type="number" name="expense" class="form-control" placeholder="{{__('admin.expenseAttribute')}}" value="{{$expense->expense}}" required>
                                                </div>
                                                @include('Admin.layouts.error', ['input' => 'expense'])
                                            </div><!-- row -->

                                            <div class="row mg-t-20">
                                                <label class="col-sm-4 form-control-label">{{__('admin.dateAttribute')}}</label>
                                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                                    <input type="text" name="date" data-toggle="datepicker" class="form-control" autocomplete="off" placeholder="{{__('admin.priceAttribute')}}" value="{{$expense->date}}" required>
                                                </div>
                                                @include('Admin.layouts.error', ['input' => 'date'])
                                            </div><!-- row -->

                                            <div class="row mg-t-20">
                                                <label class="col-sm-4 form-control-label">{{__('admin.notesAttribute')}}</label>
                                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                                    <textarea rows="3" name="notes" class="form-control" placeholder="{{__('admin.notesAttribute')}}" required>{{$expense->notes}}</textarea>
                                                </div>
                                                @include('Admin.layouts.error', ['input' => 'notes'])
                                            </div><!-- row -->
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                                            <button type="submit" class="btn btn-info btn-sm">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div><!-- table-wrapper -->
    </div>

    @if(auth()->guard('admin')->user()->hasPermissionTo('create expenses'))
    <!-- Modal -->
    <div class="modal fade" id="addExpense" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form class="modal-content" method="post" action="{{route('admin.expenses.store', $scope)}}">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('admin.addExpense')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <label class="col-sm-4 form-control-label">{{__('admin.categoriesSideBar')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select class="form-control" name="category_id" required>
                                <option selected disabled> {{__('admin.selectCategory')}} </option>
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">
                                        {{$category->name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'category_id'])
                    </div>
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.nameAttribute')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" name="name" class="form-control" placeholder="{{__('admin.nameAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'name'])
                    </div><!-- row -->
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.expenseAttribute')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="number" name="expense" class="form-control" placeholder="{{__('admin.expenseAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'expense'])
                    </div><!-- row -->

                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.dateAttribute')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" data-toggle="datepicker" name="date" autocomplete="off" class="form-control" placeholder="{{__('admin.dateAttribute')}}" value="{{now()->format('yy-m-d')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'date'])
                    </div><!-- row -->

                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">{{__('admin.notesAttribute')}}</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <textarea rows="3" name="notes" class="form-control" placeholder="{{__('admin.notesAttribute')}}" required></textarea>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'notes'])
                    </div><!-- row -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                    <button type="submit" class="btn btn-info btn-sm">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                </div>
            </form>
        </div>
    </div>
    @endif

@endsection

@section('script')
    <script src="/admin/lib/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/admin/lib/datatables.net-dt/js/dataTables.dataTables.min.js"></script>
    <script src="/admin/lib/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="/admin/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js"></script>
    <script src="/admin/js/datepicker.js"></script>
    <script>
        $(function(){
            'use strict';

            $('#datatable1').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });

            // Select2
            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

            $('[data-toggle="datepicker"]').datepicker({
                format: 'yyyy-mm-dd',
                autoHide: true,
                zIndex: 2048,
            });
        });
    </script>
@endsection
